;;
;  MATH_dp_maxval.s
;
    .global MATH_dp_maxval
MATH_dp_maxval:
; 1
    LDDW      .D1     *A4, A31:A30                 ; 5 | load one 8-B input from A
||  ADD       .L2     A4, 8, B7                    ; 1 | pointer to input A
||  SHR       .S2     B4, 3, B2                    ; 1 | get number of input blocks (per eight)
; 2
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load one 8-B input from A
||  SUB       .S2     B2, 3, B2                    ; 1 | decrement number of input blocks
||  LDDW      .D2     *B7, B31:B30                 ; 5 | load one 8-B input from A
; 3
    LDDW      .D1     *++A4[2], A27:A26            ; 5 | load one 8-B input from A
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load one 8-B input from A
; 4
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load one 8-B input from A
||  LDDW      .D2     *++B7[2], B27:B26            ; 5 | load one 8-B input from A
; 5
    LDDW      .D2     *++B7[2], B25:B24            ; 5 | load one 8-B input from A
; 6
    LDDW      .D1     *++A4[2], A23:A22            ; 5 | load one 8-B input from A
; 7
    CMPGTDP   .S1     A29:A28, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B23:B22            ; 5 | load one 8-B input from A
; 8
    LDDW      .D1     *++A4[2], A21:A20            ; 5 | load one 8-B input from A
||  CMPGTDP   .S2     B29:B28, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 9
loop:
    [A0]DMV   .L1     A29, A28, A31:A30            ; 1 |
||  CMPGTDP   .S1     A25:A24, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  SUB       .L2     B2, 1, B2                    ; 1 | decrement number of input blocks
||  LDDW      .D2     *++B7[2], B21:B20            ; 5 | load one 8-B input from A
; 10
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B29, B28, B31:B30            ; 1 |
||  CMPGTDP   .S2     B25:B24, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 11
    [A0]DMV   .L1     A25, A24, A27:A26            ; 1 |
||  CMPGTDP   .S1     A23:A22, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load one 8-B input from A
; 12
    [B2]B     .S2     loop                         ; 6 | if B2<>0 go back to loop
; 13
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B25, B24, B27:B26            ; 1 |
||  CMPGTDP   .S2     B23:B22, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 14
    [A0]DMV   .L1     A23, A22, A31:A30            ; 1 |
||  CMPGTDP   .S1     A21:A20, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B25:B24            ; 5 | load one 8-B input from A
; 15
    LDDW      .D1     *++A4[2], A23:A22            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B23, B22, B31:B30            ; 1 |
||  CMPGTDP   .S2     B21:B20, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 16
    [A0]DMV   .L1     A21, A20, A27:A26            ; 1 |
||  CMPGTDP   .S1     A29:A28, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B23:B22            ; 5 | load one 8-B input from A
; 17
    LDDW      .D1     *++A4[2], A21:A20            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B21, B20, B27:B26            ; 1 |
||  CMPGTDP   .S2     B29:B28, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 18
    [A0]DMV   .L1     A29, A28, A31:A30            ; 1 |
||  CMPGTDP   .S1     A25:A24, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B21:B20            ; 5 | load one 8-B input from A
; 19
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B29, B28, B31:B30            ; 1 |
||  CMPGTDP   .S2     B25:B24, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 20
    [A0]DMV   .L1     A25, A24, A27:A26            ; 1 |
||  CMPGTDP   .S1     A23:A22, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load one 8-B input from A
; 21
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B25, B24, B27:B26            ; 1 |
||  CMPGTDP   .S2     B23:B22, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 22
    [A0]DMV   .L1     A23, A22, A31:A30            ; 1 |
||  CMPGTDP   .S1     A21:A20, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B25:B24            ; 5 | load one 8-B input from A
; 23
    LDDW      .D1     *++A4[2], A23:A22            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B23, B22, B31:B30            ; 1 |
||  CMPGTDP   .S2     B21:B20, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 24
    [A0]DMV   .L1     A21, A20, A27:A26            ; 1 |
||  CMPGTDP   .S1     A29:A28, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B23:B22            ; 5 | load one 8-B input from A
; 25
    LDDW      .D1     *++A4[2], A21:A20            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B21, B20, B27:B26            ; 1 |
||  CMPGTDP   .S2     B29:B28, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 26
    [A0]DMV   .L1     A29, A28, A31:A30            ; 1 |
||  CMPGTDP   .S1     A25:A24, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B21:B20            ; 5 | load one 8-B input from A
; 27
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B29, B28, B31:B30            ; 1 |
||  CMPGTDP   .S2     B25:B24, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 28
    [A0]DMV   .L1     A25, A24, A27:A26            ; 1 |
||  CMPGTDP   .S1     A23:A22, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load one 8-B input from A
; 29
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B25, B24, B27:B26            ; 1 |
||  CMPGTDP   .S2     B23:B22, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 30
    [A0]DMV   .L1     A23, A22, A31:A30            ; 1 |
||  CMPGTDP   .S1     A21:A20, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
; 31
    LDDW      .D1     *++A4[1], A23:A22            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B23, B22, B31:B30            ; 1 |
||  CMPGTDP   .S2     B21:B20, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 32
    [A0]DMV   .L1     A21, A20, A27:A26            ; 1 |
||  CMPGTDP   .S1     A29:A28, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
; 33
    [B0]DMV   .L2     B21, B20, B27:B26            ; 1 |
||  CMPGTDP   .S2     B29:B28, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 34
    [A0]DMV   .L1     A29, A28, A31:A30            ; 1 |
||  CMPGTDP   .S1     A25:A24, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
; 35
    [B0]DMV   .L2     B29, B28, B31:B30            ; 1 |
; 36
    [A0]DMV   .L1     A25, A24, A27:A26            ; 1 |
||  CMPGTDP   .S1     A23:A22, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  CMPGTDP   .S2     B31:B30, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 37
    DMV       .L2     B27, B26, B5:B4              ; 1 |
; 38
    [A0]DMV   .L1     A23, A22, A31:A30            ; 1 |
||  [B0]DMV   .L2     B31, B30, B5:B4              ; 1 |
; 39
    CMPGTDP   .S1     A31:A30, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  MV        .D1     B5, A7                       ; 1 |
||  B         .S2     B3                           ; 6 | return from function
; 40
    DMV       .L1     A27, A26, A5:A4              ; 1 |
||  MV        .D1     B4, A6                       ; 1 |
; 41
    [A0]DMV   .L1     A31, A30, A5:A4              ; 1 |
; 42
    CMPGTDP   .S1     A7:A6, A5:A4, A0             ; 2 | if src1 > src2, then dst = 1
; 43
    NOP
; 44
    [A0]DMV   .L1     A7, A6, A5:A4                ; 1 |
