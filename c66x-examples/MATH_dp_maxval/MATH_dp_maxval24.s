;;
;  MATH_dp_maxval24.s
;
    .global MATH_dp_maxval24
MATH_dp_maxval24:
; 1
    LDDW      .D1     *A4, A31:A30                 ; 5 | load one 8-B input from A
||  ADD       .L2     A4, 8, B7                    ; 1 | pointer to input A
; 2
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load one 8-B input from A
||  LDDW      .D2     *B7, B31:B30                 ; 5 | load one 8-B input from A
; 3
    LDDW      .D1     *++A4[2], A27:A26            ; 5 | load one 8-B input from A
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load one 8-B input from A
; 4
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load one 8-B input from A
||  LDDW      .D2     *++B7[2], B27:B26            ; 5 | load one 8-B input from A
; 5
    LDDW      .D2     *++B7[2], B25:B24            ; 5 | load one 8-B input from A
; 6
    LDDW      .D1     *++A4[2], A23:A22            ; 5 | load one 8-B input from A
; 7
    CMPGTDP   .S1     A29:A28, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B23:B22            ; 5 | load one 8-B input from A
; 8
    LDDW      .D1     *++A4[2], A21:A20            ; 5 | load one 8-B input from A
||  CMPGTDP   .S2     B29:B28, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 9
    [A0]DMV   .L1     A29, A28, A31:A30            ; 1 |
||  CMPGTDP   .S1     A25:A24, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B21:B20            ; 5 | load one 8-B input from A
; 10
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B29, B28, B31:B30            ; 1 |
||  CMPGTDP   .S2     B25:B24, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 11
    [A0]DMV   .L1     A25, A24, A27:A26            ; 1 |
||  CMPGTDP   .S1     A23:A22, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load one 8-B input from A
; 12
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B25, B24, B27:B26            ; 1 |
||  CMPGTDP   .S2     B23:B22, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 13
    [A0]DMV   .L1     A23, A22, A31:A30            ; 1 |
||  CMPGTDP   .S1     A21:A20, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B25:B24            ; 5 | load one 8-B input from A
; 14
    LDDW      .D1     *++A4[2], A23:A22            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B23, B22, B31:B30            ; 1 |
||  CMPGTDP   .S2     B21:B20, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 15
    [A0]DMV   .L1     A21, A20, A27:A26            ; 1 |
||  CMPGTDP   .S1     A29:A28, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B23:B22            ; 5 | load one 8-B input from A
; 16
    LDDW      .D1     *++A4[2], A21:A20            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B21, B20, B27:B26            ; 1 |
||  CMPGTDP   .S2     B29:B28, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 17
    [A0]DMV   .L1     A29, A28, A31:A30            ; 1 |
||  CMPGTDP   .S1     A25:A24, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B21:B20            ; 5 | load one 8-B input from A
; 18
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B29, B28, B31:B30            ; 1 |
||  CMPGTDP   .S2     B25:B24, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 19
    [A0]DMV   .L1     A25, A24, A27:A26            ; 1 |
||  CMPGTDP   .S1     A23:A22, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load one 8-B input from A
; 20
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B25, B24, B27:B26            ; 1 |
||  CMPGTDP   .S2     B23:B22, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 21
    [A0]DMV   .L1     A23, A22, A31:A30            ; 1 |
||  CMPGTDP   .S1     A21:A20, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
; 22
    LDDW      .D1     *++A4[1], A23:A22            ; 5 | load one 8-B input from A
||  [B0]DMV   .L2     B23, B22, B31:B30            ; 1 |
||  CMPGTDP   .S2     B21:B20, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 23
    [A0]DMV   .L1     A21, A20, A27:A26            ; 1 |
||  CMPGTDP   .S1     A29:A28, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
; 24
    [B0]DMV   .L2     B21, B20, B27:B26            ; 1 |
||  CMPGTDP   .S2     B29:B28, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 25
    [A0]DMV   .L1     A29, A28, A31:A30            ; 1 |
||  CMPGTDP   .S1     A25:A24, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
; 26
    [B0]DMV   .L2     B29, B28, B31:B30            ; 1 |
; 27
    [A0]DMV   .L1     A25, A24, A27:A26            ; 1 |
||  CMPGTDP   .S1     A23:A22, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  CMPGTDP   .S2     B31:B30, B27:B26, B0         ; 2 | if src1 > src2, then dst = 1
; 28
    DMV       .L2     B27, B26, B5:B4              ; 1 |
; 29
    [A0]DMV   .L1     A23, A22, A31:A30            ; 1 |
||  [B0]DMV   .L2     B31, B30, B5:B4              ; 1 |
; 30
    CMPGTDP   .S1     A31:A30, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  MV        .D1     B5, A7                       ; 1 |
||  B         .S2     B3                           ; 6 | return from function
; 31
    DMV       .L1     A27, A26, A5:A4              ; 1 |
||  MV        .D1     B4, A6                       ; 1 |
; 32
    [A0]DMV   .L1     A31, A30, A5:A4              ; 1 |
; 33
    CMPGTDP   .S1     A7:A6, A5:A4, A0             ; 2 | if src1 > src2, then dst = 1
; 34
    NOP
; 35
    [A0]DMV   .L1     A7, A6, A5:A4                ; 1 |
