;;
;  MATH_dp_maxval8.s
;
    .global MATH_dp_maxval8
MATH_dp_maxval8:
; 1
    LDDW      .D1     *A4, A31:A30                 ; 5 | load one 8-B input from A
||  ADD       .L2     A4, 8, B7                    ; 1 | pointer to input A
; 2
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load one 8-B input from A
||  LDDW      .D2     *B7, B31:B30                 ; 5 | load one 8-B input from A
; 3
    LDDW      .D1     *++A4[2], A27:A26            ; 5 | load one 8-B input from A
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load one 8-B input from A
; 4
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load one 8-B input from A
||  LDDW      .D2     *++B7[2], B27:B26            ; 5 | load one 8-B input from A
; 5
    LDDW      .D1     *++A4[1], A23:A22            ; 5 | load one 8-B input from A
; 6
    NOP
; 7
    CMPGTDP   .S1     A29:A28, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
; 8
    CMPGTDP   .S2     B29:B28, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 9
    [A0]DMV   .L1     A29, A28, A31:A30            ; 1 |
||  CMPGTDP   .S1     A25:A24, A27:A26, A1         ; 2 | if src1 > src2, then dst = 1
; 10
    [B0]DMV   .L2     B29, B28, B31:B30            ; 1 |
; 11
    [A1]DMV   .L1     A25, A24, A27:A26            ; 1 |
||  CMPGTDP   .S1     A23:A22, A31:A30, A0         ; 2 | if src1 > src2, then dst = 1
||  CMPGTDP   .S2     B27:B26, B31:B30, B0         ; 2 | if src1 > src2, then dst = 1
; 12
    DMV       .L2     B31, B30, B5:B4              ; 1 |
; 13
    [A0]DMV   .L1     A23, A22, A31:A30            ; 1 |
||  [B0]DMV   .L2     B27, B26, B5:B4              ; 1 |
; 14
    CMPGTDP   .S1     A31:A30, A27:A26, A0         ; 2 | if src1 > src2, then dst = 1
||  MV        .D1     B5, A7                       ; 1 |
||  B         .S2     B3                           ; 6 | return from function
; 15
    DMV       .L1     A27, A26, A5:A4              ; 1 |
||  MV        .D1     B4, A6                       ; 1 |
; 16
    [A0]DMV   .L1     A31, A30, A5:A4              ; 1 |
; 17
    CMPGTDP   .S1     A7:A6, A5:A4, A0             ; 2 | if src1 > src2, then dst = 1
; 18
    NOP
; 19
    [A0]DMV   .L1     A7, A6, A5:A4                ; 1 |
