/**
 ***********************************************************************
 * @file MATH_dp_maxval.h
 *
 * @brief Find maximum value element in double-precision float vector.
 * @author Tomas Fryza, Brno University of Technology, Czech Republic
 */


#ifndef MATH_DP_MAXVAL_H_INCLUDED
#define MATH_DP_MAXVAL_H_INCLUDED


/**
 ***********************************************************************
 * @ingroup MATH
 * @{
 * @defgroup MATH_dp_maxval MATH_dp_maxval
 * @brief Find maximum value element in double-precision float vector.
 * @{
 */


/**
 ***********************************************************************
 * @brief Find maximum value element in double-precision vector of length 8.
 *
 * @param a - Pointer to input vector of length 8
 * @return    Maximum value element in input vector
 *
 * @par Assumptions:
 *    Vector length is equal to 8.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 19<br>
 *    Architecture: TMS320C66x<br>
 */
extern double MATH_dp_maxval8(double *a);


/**
 ***********************************************************************
 * @brief Find maximum value element in double-precision vector of length 16.
 *
 * @param a - Pointer to input vector of length 16
 * @return    Maximum value element in input vector
 *
 * @par Assumptions:
 *    Vector length is equal to 16.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 27<br>
 *    Architecture: TMS320C66x<br>
*/
extern double MATH_dp_maxval16(double *a);


/**
 ***********************************************************************
 * @brief Find maximum value element in double-precision vector of length 24.
 *
 * @param a - Pointer to input vector of length 24
 * @return    Maximum value element in input vector
 *
 * @par Assumptions:
 *    Vector length is equal to 24.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 35<br>
 *    Architecture: TMS320C66x<br>
 */
extern double MATH_dp_maxval24(double *a);


/**
 ***********************************************************************
 * @brief Find maximum value element in double-precision vector of length N.
 *
 * @param a - Pointer to input vector of length N
 * @param N - Length of input vector
 * @return    Maximum value element in input vector
 *
 * @par Assumptions:
 *    Vector length must be a multiple of 8 and >= 32.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 9/8*N + 8<br>
 *    Architecture: TMS320C66x<br>
 */
extern double MATH_dp_maxval(double *a, int N);


/** @} defgroup MATH_dp_maxval */
#endif /* MATH_DP_MAXVAL_H_INCLUDED */
