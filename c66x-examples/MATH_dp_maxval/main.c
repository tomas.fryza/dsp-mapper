/***********************************************************************
 * Title: Find maximum value element in double-precision float vector
 * Author: Tomas Fryza, Brno University of Technology
 * Date: September 2017
 */


#include <stdio.h>
#include "MATH_dp_maxval.h"


/*
 *  Constants and macros
 */
#define     N   48


/*
 *  Function prototypes
 */
void init_mat(void);
double MATH_dp_maxval_cn(double*);


/*
 *  Global variables
 */
double a[N] = {0.0};
//N=48
/*double a[N] = {
    0.9026674, 0.1037426, 0.5256415, 0.9619902, 0.4255723, 0.4012991, 0.2031019, 0.8603935,
    0.1714196, 0.5859685, 0.2904353, 0.3767264, 0.7749230, 0.8899808, 0.8844871, 0.7444656,
    0.4649310, 0.5282603, 0.0751072, 0.5120813, 0.6450666, 0.2724613, 0.5655813, 0.4180544,
    0.5704180, 0.0621059, 0.4524802, 0.3394077, 0.7995255, 0.5973872, 0.2617430, 0.2603348,
    0.8390916, 0.9424355, 0.9027161, 0.5666718, 0.6652423, 0.0050029, 0.6895367, 0.4392365,
    0.3534879, 0.3048308, 0.9860462, 0.4533339, 0.9712771, 0.6702345, 0.9063893, 0.5377719};*/


/***********************************************************************
 * Main function
 */
int main(void)
{
    double max = 0.0;
    double max_cn = 0.0;

    puts("Start");

    /* Set the input data */
    init_mat();

    /* Find the maximum in ASM-code */
//    max = MATH_dp_maxval8(a);
//    max = MATH_dp_maxval16(a);
//    max = MATH_dp_maxval24(a);
    max = MATH_dp_maxval(a, N);

    /* Find the maximum in C-code */
    max_cn = MATH_dp_maxval_cn(a);

    puts("Stop");
    for (;;) {
    }
}


/***********************************************************************
 * Set the input data
 * param None
 * return None
 */
void init_mat()
{
    int i;
    for (i = 0; i < (N>>1)+1; i++) {
//        a[i] = i;
        a[i] = 2.0e90 * i;
    }
    for (i = (N>>1)-1; i < (N); i++) {
//        a[i] = N+i;
        a[i] = 2.0e49 * i;
    }
}


/***********************************************************************
 * Find the maximum in C-code. Non-optimized algorithm
 * param *pa - Pointer to input vector of length N
 * return      Maximum value element in input vector
 */
double MATH_dp_maxval_cn(double *pa)
{
    int i;
    double temp;

    temp = *pa;
    for (i = 1; i < N; i++) {
        if (*(pa+i) > temp) {
            temp = *(pa+i);
        }
    }
    return temp;
}
