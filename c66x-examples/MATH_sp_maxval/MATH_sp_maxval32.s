;;
;  MATH_sp_maxval32.s
;
    .global MATH_sp_maxval32
MATH_sp_maxval32:
; 1
    LDDW      .D1     *A4, A31:A30                 ; 5 | load two 4-B inputs from A
||  ADD       .L2     A4, 8, B7                    ; 1 | pointer to input A
; 2
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *B7, B31:B30                 ; 5 | load two 4-B inputs from A
; 3
    LDDW      .D1     *++A4[2], A27:A26            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load two 4-B inputs from A
; 4
    LDDW      .D2     *++B7[2], B27:B26            ; 5 | load two 4-B inputs from A
; 5
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load two 4-B inputs from A
; 6
    CMPGTSP   .S1     A30, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B25:B24            ; 5 | load two 4-B inputs from A
; 7
    [A0]MV    .L1     A30, A31                     ; 1 |
||  CMPGTSP   .S1     A28, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D1     *++A4[2], A23:A22            ; 5 | load two 4-B inputs from A
||  CMPGTSP   .S2     B30, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 8
    [A1]MV    .L1     A28, A29                     ; 1 |
||  CMPGTSP   .S1     A27, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B30, B31                     ; 1 |
||  CMPGTSP   .S2     B28, B29, B1                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B23:B22            ; 5 | load two 4-B inputs from A
; 9
    [A0]MV    .L1     A27, A31                     ; 1 |
||  CMPGTSP   .S1     A26, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D1     *++A4[2], A21:A20            ; 5 | load two 4-B inputs from A
||  [B1]MV    .L2     B28, B29                     ; 1 |
||  CMPGTSP   .S2     B27, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 10
    [A1]MV    .L1     A26, A29                     ; 1 |
||  CMPGTSP   .S1     A25, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B27, B31                     ; 1 |
||  CMPGTSP   .S2     B26, B29, B1                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B21:B20            ; 5 | load two 4-B inputs from A
; 11
    [A0]MV    .L1     A25, A31                     ; 1 |
||  CMPGTSP   .S1     A24, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D1     *++A4[2], A19:A18            ; 5 | load two 4-B inputs from A
||  [B1]MV    .L2     B26, B29                     ; 1 |
||  CMPGTSP   .S2     B25, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 12
    [A1]MV    .L1     A24, A29                     ; 1 |
||  CMPGTSP   .S1     A23, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B25, B31                     ; 1 |
||  CMPGTSP   .S2     B24, B29, B1                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B19:B18            ; 5 | load two 4-B inputs from A
; 13
    [A0]MV    .L1     A23, A31                     ; 1 |
||  CMPGTSP   .S1     A22, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D1     *++A4[2], A17:A16            ; 5 | load two 4-B inputs from A
||  [B1]MV    .L2     B24, B29                     ; 1 |
||  CMPGTSP   .S2     B23, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 14
    [A1]MV    .L1     A22, A29                     ; 1 |
||  CMPGTSP   .S1     A21, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B23, B31                     ; 1 |
||  CMPGTSP   .S2     B22, B29, B1                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D2     *++B7[2], B17:B16            ; 5 | load two 4-B inputs from A
; 15
    [A0]MV    .L1     A21, A31                     ; 1 |
||  CMPGTSP   .S1     A20, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  LDDW      .D1     *++A4[1], A27:A26            ; 5 | load two 4-B inputs from A
||  [B1]MV    .L2     B22, B29                     ; 1 |
||  CMPGTSP   .S2     B21, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 16
    [A1]MV    .L1     A20, A29                     ; 1 |
||  CMPGTSP   .S1     A19, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B21, B31                     ; 1 |
||  CMPGTSP   .S2     B20, B29, B1                 ; 1 | if src1 > src2, then dst = 1
; 17
    [A0]MV    .L1     A19, A31                     ; 1 |
||  CMPGTSP   .S1     A18, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  [B1]MV    .L2     B20, B29                     ; 1 |
||  CMPGTSP   .S2     B19, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 18
    [A1]MV    .L1     A18, A29                     ; 1 |
||  CMPGTSP   .S1     A17, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B19, B31                     ; 1 |
||  CMPGTSP   .S2     B18, B29, B1                 ; 1 | if src1 > src2, then dst = 1
; 19
    [A0]MV    .L1     A17, A31                     ; 1 |
||  CMPGTSP   .S1     A16, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  [B1]MV    .L2     B18, B29                     ; 1 |
||  CMPGTSP   .S2     B17, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 20
    [A1]MV    .L1     A16, A29                     ; 1 |
||  CMPGTSP   .S1     A26, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B17, B31                     ; 1 |
||  B         .S2     B3                           ; 6 | return from function
; 21
    [A0]MV    .L1     A26, A31                     ; 1 |
||  MV        .L2     B29, B4                      ; 1 |
||  CMPGTSP   .S2     B31, B29, B0                 ; 1 | if src1 > src2, then dst = 1
; 22
    MV        .L1     A29, A4                      ; 1 |
||  CMPGTSP   .S1     A31, A29, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B31, B4                      ; 1 |
; 23
    [A0]MV    .L1     A31, A4                      ; 1 |
; 24
    CMPGTSP   .S1     A4, B4, A0                   ; 1 | if src1 > src2, then dst = 1
; 25
    [!A0]MV   .L1     B4, A4                       ; 1 |
