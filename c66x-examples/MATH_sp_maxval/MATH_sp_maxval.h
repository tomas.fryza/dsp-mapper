/**
 ***********************************************************************
 * @file MATH_sp_maxval.h
 *
 * @brief Find maximum value element in single-precision float vector.
 * @author Tomas Fryza, Brno University of Technology, Czech Republic
 */


#ifndef MATH_SP_MAXVAL_H_INCLUDED
#define MATH_SP_MAXVAL_H_INCLUDED


/**
 ***********************************************************************
 * @ingroup MATH
 * @{
 * @defgroup MATH_sp_maxval MATH_sp_maxval
 * @brief Find maximum value element in single-precision float vector.
 * @{
 */


/**
 ***********************************************************************
 * @brief Find maximum value element in single-precision vector of length 16.
 *
 * @param a - Pointer to input vector of length 16
 * @return    Maximum value element in input vector
 *
 * @par Assumptions:
 *    Vector length is equal to 16.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 17<br>
 *    Architecture: TMS320C66x<br>
 */
extern float MATH_sp_maxval16(float *a);


/**
 ***********************************************************************
 * @brief Find maximum value element in single-precision vector of length 32.
 *
 * @param a - Pointer to input vector of length 32
 * @return    Maximum value element in input vector
 *
 * @par Assumptions:
 *    Vector length is equal to 32.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 25<br>
 *    Architecture: TMS320C66x<br>
 */
extern float MATH_sp_maxval32(float *a);


/**
 ***********************************************************************
 * @brief Find maximum value element in single-precision vector of length N.
 *
 * @param a - Pointer to input vector of length N
 * @param N - Length of input vector
 * @return    Maximum value element in input vector
 *
 * @par Assumptions:
 *    Vector length must be a multiple of 16 and >= 48.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 1/2*N + 12<br>
 *    Architecture: TMS320C66x<br>
 */
extern float MATH_sp_maxval(float *a, int N);


/** @} defgroup MATH_sp_maxval */
#endif /* MATH_SP_MAXVAL_H_INCLUDED */
