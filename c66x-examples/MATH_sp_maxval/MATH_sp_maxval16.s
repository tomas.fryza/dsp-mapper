;;
;  MATH_sp_maxval16.s
;
    .global MATH_sp_maxval16
MATH_sp_maxval16:
; 1
    LDDW      .D1     *A4, A31:A30                 ; 5 | load two 4-B inputs from A
||  ADD       .L2     A4, 8, B7                    ; 1 | pointer to input A
; 2
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *B7, B31:B30                 ; 5 | load two 4-B inputs from A
; 3
    LDDW      .D1     *++A4[2], A27:A26            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *++B7[2], B29:B28            ; 5 | load two 4-B inputs from A
; 4
    LDDW      .D1     *++A4[2], A25:A24            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *++B7[2], B27:B26            ; 5 | load two 4-B inputs from A
; 5
    LDDW      .D1     *++A4[1], A23:A22            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *++B7[2], B25:B24            ; 5 | load two 4-B inputs from A
; 6
    CMPGTSP   .S1     A30, A31, A0                 ; 1 | if src1 > src2, then dst = 1
; 7
    [A0]MV    .L1     A30, A31                     ; 1 |
||  CMPGTSP   .S1     A28, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  CMPGTSP   .S2     B30, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 8
    [A1]MV    .L1     A28, A29                     ; 1 |
||  CMPGTSP   .S1     A27, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B30, B31                     ; 1 |
||  CMPGTSP   .S2     B28, B29, B1                 ; 1 | if src1 > src2, then dst = 1
; 9
    [A0]MV    .L1     A27, A31                     ; 1 |
||  CMPGTSP   .S1     A26, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  [B1]MV    .L2     B28, B29                     ; 1 |
||  CMPGTSP   .S2     B27, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 10
    [A1]MV    .L1     A26, A29                     ; 1 |
||  CMPGTSP   .S1     A25, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B27, B31                     ; 1 |
||  CMPGTSP   .S2     B26, B29, B1                 ; 1 | if src1 > src2, then dst = 1
; 11
    [A0]MV    .L1     A25, A31                     ; 1 |
||  CMPGTSP   .S1     A24, A29, A1                 ; 1 | if src1 > src2, then dst = 1
||  [B1]MV    .L2     B26, B29                     ; 1 |
||  CMPGTSP   .S2     B25, B31, B0                 ; 1 | if src1 > src2, then dst = 1
; 12
    [A1]MV    .L1     A24, A29                     ; 1 |
||  CMPGTSP   .S1     A22, A31, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B25, B31                     ; 1 |
||  B         .S2     B3                           ; 6 | return from function
; 13
    [A0]MV    .L1     A22, A31                     ; 1 |
||  MV        .L2     B29, B4                      ; 1 |
||  CMPGTSP   .S2     B31, B29, B0                 ; 1 | if src1 > src2, then dst = 1
; 14
    MV        .L1     A29, A4                      ; 1 |
||  CMPGTSP   .S1     A31, A29, A0                 ; 1 | if src1 > src2, then dst = 1
||  [B0]MV    .L2     B31, B4                      ; 1 |
; 15
    [A0]MV    .L1     A31, A4                      ; 1 |
; 16
    CMPGTSP   .S1     A4, B4, A0                   ; 1 | if src1 > src2, then dst = 1
; 17
    [!A0]MV   .L1     B4, A4                       ; 1 |
