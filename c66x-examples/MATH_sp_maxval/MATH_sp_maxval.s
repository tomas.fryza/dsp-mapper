;;
;  MATH_sp_maxval.s
;
    .global MATH_sp_maxval
MATH_sp_maxval:
; 1
    LDDW      .D1     *A4, A31:A30                 ; 5 | load two 4-B inputs from A
||  ADD       .L2     A4, 8, B5                    ; 1 | pointer to input A
||  SHR       .S2     B4, 4, B2                    ; 1 | get number of input blocks (per sixteen)
; 2
    LDDW      .D1     *++A4[2], A29:A28            ; 5 | load two 4-B inputs from A
||  SUB       .S2     B2, 2, B2                    ; 1 | decrement number of input blocks
||  LDDW      .D2     *B5, B31:B30                 ; 5 | load two 4-B inputs from A
; 3
    MVKL      .S1     0x80000000, A3               ; 1 | sign bit
||  LDDW      .D1     *++A4[2], A27:A26            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *++B5[2], B29:B28            ; 5 | load two 4-B inputs from A
; 4
    MVKH      .S1     0x80000000, A3               ; 1 | sign bit
||  LDDW      .D1     *++A4[2], A25:A24            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *++B5[2], B27:B26            ; 5 | load two 4-B inputs from A
; 5
    MV        .L1     A3, A2                       ; 1 | sign bit
||  MVKL      .S1     0x3f800000, A5               ; 1 | float 1
||  LDDW      .D1     *++A4[2], A23:A22            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *++B5[2], B25:B24            ; 5 | load two 4-B inputs from A
; 6
    MVKH      .S1     0x3f800000, A5               ; 1 | float 1
||  LDDW      .D1     *++A4[2], A21:A20            ; 5 | load two 4-B inputs from A
||  LDDW      .D2     *++B5[2], B23:B22            ; 5 | load two 4-B inputs from A
; 7
    DSUBSP    .L1     A31:A30, A29:A28, A7:A6      ; 3 |
||  LDDW      .D1     *++A4[2], A19:A18            ; 5 | load two 4-B inputs from A
||  MV        .S2     A2, B4                       ; 1 | sign bit
||  LDDW      .D2     *++B5[2], B21:B20            ; 5 | load two 4-B inputs from A
; 8
    LDDW      .D1     *++A4[2], A17:A16            ; 5 | load two 4-B inputs from A
||  DSUBSP    .L2     B31:B30, B29:B28, B7:B6      ; 3 |
||  LDDW      .D2     *++B5[2], B19:B18            ; 5 | load two 4-B inputs from A
; 9
    DSUBSP    .L1     A27:A26, A25:A24, A9:A8      ; 3 |
||  LDDW      .D2     *++B5[2], B17:B16            ; 5 | load two 4-B inputs from A
; 10
    AND       .L1     A7:A6, A3:A2, A1:A0          ; 1 |
||  LDDW      .D1     *++A4[2], A29:A28            ; 5 | load two 4-B inputs from A
||  DSUBSP    .L2     B27:B26, B25:B24, B9:B8      ; 3 |
; 11
    DSUBSP    .L1     A23:A22, A21:A20, A11:A10    ; 3 |
||  [A1]MV    .S1     A29, A31                     ; 1 | move greater
||  [A0]MPYSP .M1     A28, A5, A30                 ; 4 | move greater
||  AND       .L2     B7:B6, A3:A2, B1:B0          ; 1 |
||  LDDW      .D2     *++B5[2], B29:B28            ; 5 | load two 4-B inputs from A
; 12
    AND       .L1     A9:A8, A3:A2, A1:A0          ; 1 |
||  LDDW      .D1     *++A4[2], A25:A24            ; 5 | load two 4-B inputs from A
||  DSUBSP    .L2     B23:B22, B21:B20, B11:B10    ; 3 |
||  [B1]MV    .S2     B29, B31                     ; 1 | move greater
||  [B0]MPYSP .M2     B28, A5, B30                 ; 4 | move greater
; 13
loop:
    DSUBSP    .L1     A19:A18, A17:A16, A13:A12    ; 3 |
||  [A1]MV    .S1     A25, A27                     ; 1 | move greater
||  [A0]MPYSP .M1     A24, A5, A26                 ; 4 | move greater
||  AND       .L2     B9:B8, A3:A2, B1:B0          ; 1 |
||  SUB       .S2     B2, 1, B2                    ; 1 | decrement number of input blocks
||  LDDW      .D2     *++B5[2], B25:B24            ; 5 | load two 4-B inputs from A
; 14
    AND       .L1     A11:A10, A3:A2, A1:A0        ; 1 |
||  LDDW      .D1     *++A4[2], A21:A20            ; 5 | load two 4-B inputs from A
||  DSUBSP    .L2     B19:B18, B17:B16, B13:B12    ; 3 |
||  [B1]MV    .S2     B25, B27                     ; 1 | move greater
||  [B0]MPYSP .M2     B24, A5, B26                 ; 4 | move greater
; 15
    DSUBSP    .L1     A31:A30, A29:A28, A7:A6      ; 3 |
||  [A1]MV    .S1     A21, A23                     ; 1 | move greater
||  [A0]MPYSP .M1     A20, A5, A22                 ; 4 | move greater
||  AND       .L2     B11:B10, A3:A2, B1:B0        ; 1 |
||  [B2]B     .S2     loop                         ; 6 | if B2<>0 go back to loop
||  LDDW      .D2     *++B5[2], B21:B20            ; 5 | load two 4-B inputs from A
; 16
    AND       .L1     A13:A12, A3:A2, A1:A0        ; 1 |
||  LDDW      .D1     *++A4[2], A17:A16            ; 5 | load two 4-B inputs from A
||  DSUBSP    .L2     B31:B30, B29:B28, B7:B6      ; 3 |
||  [B1]MV    .S2     B21, B23                     ; 1 | move greater
||  [B0]MPYSP .M2     B20, A5, B22                 ; 4 | move greater
; 17
    DSUBSP    .L1     A27:A26, A25:A24, A9:A8      ; 3 |
||  [A1]MV    .S1     A17, A19                     ; 1 | move greater
||  [A0]MPYSP .M1     A16, A5, A18                 ; 4 | move greater
||  AND       .L2     B13:B12, A3:A2, B1:B0        ; 1 |
||  LDDW      .D2     *++B5[2], B17:B16            ; 5 | load two 4-B inputs from A
; 18
    AND       .L1     A7:A6, A3:A2, A1:A0          ; 1 |
||  LDDW      .D1     *++A4[2], A29:A28            ; 5 | load two 4-B inputs from A
||  DSUBSP    .L2     B27:B26, B25:B24, B9:B8      ; 3 |
||  [B1]MV    .S2     B17, B19                     ; 1 | move greater
||  [B0]MPYSP .M2     B16, A5, B18                 ; 4 | move greater
; 19
    DSUBSP    .L1     A23:A22, A21:A20, A11:A10    ; 3 |
||  [A1]MV    .S1     A29, A31                     ; 1 | move greater
||  [A0]MPYSP .M1     A28, A5, A30                 ; 4 | move greater
||  AND       .L2     B7:B6, A3:A2, B1:B0          ; 1 |
||  LDDW      .D2     *++B5[2], B29:B28            ; 5 | load two 4-B inputs from A
; 20
    AND       .L1     A9:A8, A3:A2, A1:A0          ; 1 |
||  LDDW      .D1     *++A4[2], A25:A24            ; 5 | load two 4-B inputs from A
||  DSUBSP    .L2     B23:B22, B21:B20, B11:B10    ; 3 |
||  [B1]MV    .S2     B29, B31                     ; 1 | move greater
||  [B0]MPYSP .M2     B28, A5, B30                 ; 4 | move greater
; 21
    DSUBSP    .L1     A19:A18, A17:A16, A13:A12    ; 3 |
||  [A1]MV    .S1     A25, A27                     ; 1 | move greater
||  [A0]MPYSP .M1     A24, A5, A26                 ; 4 | move greater
||  AND       .L2     B9:B8, A3:A2, B1:B0          ; 1 |
; 22
    AND       .L1     A11:A10, A3:A2, A1:A0        ; 1 |
||  CMPGTSP   .S1     A27, A31, A2                 ; 1 | if src1 > src2, then dst = 1
||  DSUBSP    .L2     B19:B18, B17:B16, B13:B12    ; 3 |
||  [B1]MV    .S2     B25, B27                     ; 1 | move greater
||  [B0]MPYSP .M2     B24, A5, B26                 ; 4 | move greater
||  MV        .D2     B4, B5                       ; 1 | sign bit
; 23
    [A1]MV    .S1     A21, A23                     ; 1 | move greater
||  [A0]MPYSP .M1     A20, A5, A22                 ; 4 | move greater
||  [A2]MV    .D1     A27, A31                     ; 1 | move greater
||  AND       .L2     B11:B10, B5:B4, B1:B0        ; 1 |
||  CMPGTSP   .S2     B27, B31, B2                 ; 1 | if src1 > src2, then dst = 1
; 24
    AND       .L1     A13:A12, A3:A2, A1:A0        ; 1 |
||  CMPGTSP   .S1     A23, A31, A2                 ; 1 | if src1 > src2, then dst = 1
||  [B1]MV    .S2     B21, B23                     ; 1 | move greater
||  [B0]MPYSP .M2     B20, A5, B22                 ; 4 | move greater
||  [B2]MV    .D2     B27, B31                     ; 1 | move greater
; 25
    [A1]MV    .S1     A17, A19                     ; 1 | move greater
||  [A0]MPYSP .M1     A16, A5, A18                 ; 4 | move greater
||  [A2]MV    .D1     A23, A31                     ; 1 | move greater
||  AND       .L2     B13:B12, B5:B4, B1:B0        ; 1 |
||  CMPGTSP   .S2     B23, B31, B2                 ; 1 | if src1 > src2, then dst = 1
; 26
    CMPGTSP   .S1     A26, A30, A1                 ; 1 | if src1 > src2, then dst = 1
||  [B1]MV    .S2     B17, B19                     ; 1 | move greater
||  [B0]MPYSP .M2     B16, A5, B18                 ; 4 | move greater
||  [B2]MV    .D2     B23, B31                     ; 1 | move greater
; 27
    CMPGTSP   .S1     A19, A31, A2                 ; 1 | if src1 > src2, then dst = 1
||  [A1]MV    .D1     A26, A30                     ; 1 | move greater
||  CMPGTSP   .S2     B26, B30, B1                 ; 1 | if src1 > src2, then dst = 1
; 28
    CMPGTSP   .S1     A22, A30, A1                 ; 1 | if src1 > src2, then dst = 1
||  [A2]MV    .D1     A19, A31                     ; 1 | move greater
||  CMPGTSP   .S2     B19, B31, B2                 ; 1 | if src1 > src2, then dst = 1
||  [B1]MV    .D2     B26, B30                     ; 1 | move greater
; 29
    CMPGTSP   .S1     A18, A31, A2                 ; 1 | if src1 > src2, then dst = 1
||  [A1]MV    .D1     A22, A30                     ; 1 | move greater
||  CMPGTSP   .S2     B22, B30, B1                 ; 1 | if src1 > src2, then dst = 1
||  [B2]MV    .D2     B19, B31                     ; 1 | move greater
; 30
    [A2]MV    .D1     A18, A31                     ; 1 | move greater
||  CMPGTSP   .S2     B18, B31, B2                 ; 1 | if src1 > src2, then dst = 1
||  [B1]MV    .D2     B22, B30                     ; 1 | move greater
; 31
    CMPGTSP   .S1     A30, A31, A2                 ; 1 | if src1 > src2, then dst = 1
||  B         .S2     B3                           ; 6 | return from function
||  [B2]MV    .D2     B18, B31                     ; 1 | move greater
; 32
    [A2]MV    .D1     A30, A31                     ; 1 | move greater
||  CMPGTSP   .S2     B30, B31, B2                 ; 1 | if src1 > src2, then dst = 1
; 33
    MV        .L1     A31, A4                      ; 1 |
||  [B2]MV    .D2     B30, B31                     ; 1 | move greater
; 34
    CMPGTSP   .S1     B31, A4, A2                  ; 1 | if src1 > src2, then dst = 1
; 35
    [A2]MV    .D1     B31, A4                      ; 1 | move greater
; 36
    NOP
