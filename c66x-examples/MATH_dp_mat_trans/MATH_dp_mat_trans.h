/**
 ***********************************************************************
 * @file MATH_dp_mat_trans.h
 *
 * @brief Functions for transposition of double-precision float matrix.
 * @author Tomas Fryza, Brno University of Technology, Czech Republic
 */


#ifndef MATH_DP_MAT_TRANS_H_INCLUDED
#define MATH_DP_MAT_TRANS_H_INCLUDED


/**
 ***********************************************************************
 * @ingroup MATH
 * @{
 * @defgroup MATH_dp_mat_trans MATH_dp_mat_trans
 * @brief Transposition of double-precision float matrix.
 * @{
 */


/**
 ***********************************************************************
 * @brief Transposition of double-precision square matrix of order 4.
 *
 * @param x - Pointer to input matrix
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 4.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 22<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_dp_mat_trans4(double *x, double *y);


/**
 ***********************************************************************
 * @brief Transposition of double-precision square matrix of order 8.
 *
 * @param x - Pointer to input matrix
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 8.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 76<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_dp_mat_trans8(double *x, double *y);


/**
 ***********************************************************************
 * @brief Transposition of double-precision square matrix of order N.
 *
 * @param x - Pointer to input matrix
 * @param y - Pointer to output matrix
 * @param N - Order of square matrix
 *
 * @par Assumptions:
 *    Number of rows and columns must be a multiple of 4 and >= 8.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: N*N + 11<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_dp_mat_trans(double *x, double *y, int N);


/** @} defgroup MATH_dp_mat_trans */
#endif /* MATH_DP_MAT_TRANS_H_INCLUDED */
