;;
;  MATH_dp_mat_trans8.s
;
    .global MATH_dp_mat_trans8
MATH_dp_mat_trans8:
; 1
    MVKL      .S1     0x00000008, A6               ; 1 | N=8
; 2
    MVKH      .S1     0x00000008, A6               ; 1 | N=8
||  ADD       .L2     A4, 8, B7                    ; 1 | first pointer to input X
; 3
    MV        .L1     A4, A7                       ; 1 | first pointer to input X
||  SHL       .S1     A6, 3, A5                    ; 1 | 1*N*8
||  ADD       .L2     B7, 8, B7                    ; 1 | first pointer to input X
||  SHL       .S2     A6, 3, B5                    ; 1 | 1*N*8
; 4
    ADD       .L1     A4, A5, A8                   ; 1 | second pointer to input X
||  SHL       .S1     A6, 4, A11                   ; 1 | 2*N*8
||  ADD       .L2     B7, B5, B8                   ; 1 | second pointer to input X
||  SHL       .S2     A6, 4, B11                   ; 1 | 2*N*8
; 5
    MV        .L1     B4, A9                       ; 1 | first pointer to output Y
||  SHR       .S1     A6, 2, A1                    ; 1 | get number of horizontal blocks (per four)
||  ADD       .L2     B4, B11, B9                  ; 1 | first pointer to output Y
||  SHR       .S2     A6, 2, B1                    ; 1 | get number of horizontal blocks (per four)
; 6
    ADD       .L1     A9, A5, A10                  ; 1 | second pointer to output Y
||  SHR       .S1     A6, 2, A0                    ; 1 | get number of vertical blocks (per four)
||  ADD       .L2     B9, B5, B10                  ; 1 | second pointer to output Y
||  SHR       .S2     A6, 2, B0                    ; 1 | get number of vertical blocks (per four)
; 7
loopVert:
    SUB       .S1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  LDDW      .D1     *A7++[1], A31:A30            ; 5 | load 64-bit input
||  SUB       .S2     B0, 1, B0                    ; 1 | decrement number of vertical blocks
||  LDDW      .D2     *B7++[1], B31:B30            ; 5 | load 64-bit input
; 8
    LDDW      .D1     *A8++[1], A29:A28            ; 5 | load 64-bit input
||  LDDW      .D2     *B8++[1], B29:B28            ; 5 | load 64-bit input
; 9
    LDDW      .D1     *A7--[1], A27:A26            ; 5 | load 64-bit input
||  LDDW      .D2     *B7--[1], B27:B26            ; 5 | load 64-bit input
; 10
    ADD       .L1     A7, A11, A7                  ; 1 | first pointer to input X
||  LDDW      .D1     *A8--[1], A25:A24            ; 5 | load 64-bit input
||  ADD       .L2     B7, B11, B7                  ; 1 | first pointer to input X
||  LDDW      .D2     *B8--[1], B25:B24            ; 5 | load 64-bit input
; 11
    ADD       .L1     A8, A11, A8                  ; 1 | second pointer to input X
||  LDDW      .D1     *A7++[1], A23:A22            ; 5 | load 64-bit input
||  ADD       .L2     B8, B11, B8                  ; 1 | second pointer to input X
||  LDDW      .D2     *B7++[1], B23:B22            ; 5 | load 64-bit input
; 12
    LDDW      .D1     *A8++[1], A21:A20            ; 5 | load 64-bit input
||  LDDW      .D2     *B8++[1], B21:B20            ; 5 | load 64-bit input
; 13
loopHoriz:
    LDDW      .D1     *A7--[1], A19:A18            ; 5 | load 64-bit input
||  LDDW      .D2     *B7--[1], B19:B18            ; 5 | load 64-bit input
; 14
    LDDW      .D1     *A8--[1], A17:A16            ; 5 | load 64-bit input
||  LDDW      .D2     *B8--[1], B17:B16            ; 5 | load 64-bit input
; 15
    [A0]ADD   .L1     A7, A11, A7                  ; 1 | first pointer to input X
||  STDW      .D1     A31:A30, *A9++[1]            ; 1 | store 64-bit output
||  STDW      .D2     B31:B30, *B9++[1]            ; 1 | store 64-bit output
; 16
    [A0]ADD   .L1     A8, A11, A8                  ; 1 | second pointer to input X
||  STDW      .D1     A27:A26, *A10++[1]           ; 1 | store 64-bit output
||  [B0]ADD   .L2     B7, B11, B7                  ; 1 | first pointer to input X
||  STDW      .D2     B27:B26, *B10++[1]           ; 1 | store 64-bit output
; 17
    [!A0]ADD  .L1     A4, 8, A4                    ; 1 | update pointer to input X
||  [A0]B     .S1     loopVert                     ; 6 | if number of vertical blocks <> 0 go back
||  STDW      .D1     A29:A28, *A9++[1]            ; 1 | store 64-bit output
||  [B0]ADD   .L2     B8, B11, B8                  ; 1 | second pointer to input X
||  STDW      .D2     B29:B28, *B9++[1]            ; 1 | store 64-bit output
; 18
    [!A0]ADD  .L1     A4, 8, A4                    ; 1 | update pointer to input X
||  STDW      .D1     A25:A24, *A10++[1]           ; 1 | store 64-bit output
||  STDW      .D2     B25:B24, *B10++[1]           ; 1 | store 64-bit output
; 19
    [!A0]ADD  .L1     A4, 8, A4                    ; 1 | update pointer to input X
||  STDW      .D1     A23:A22, *A9++[1]            ; 1 | store 64-bit output
||  STDW      .D2     B23:B22, *B9++[1]            ; 1 | store 64-bit output
; 20
    [!A0]ADD  .L1     A4, 8, A4                    ; 1 | update pointer to input X
||  STDW      .D1     A19:A18, *A10++[1]           ; 1 | store 64-bit output
||  STDW      .D2     B19:B18, *B10++[1]           ; 1 | store 64-bit output
; 21
    [!A0]MV   .L1     A4, A7                       ; 1 | first pointer to input X
||  STDW      .D1     A21:A20, *A9++[1]            ; 1 | store 64-bit output
||  [!B0]ADD  .L2     A4, 8, B7                    ; 1 | first pointer to input X
||  STDW      .D2     B21:B20, *B9++[1]            ; 1 | store 64-bit output
; 22
    [!A0]ADD  .L1     A4, A5, A8                   ; 1 | second pointer to input X
||  [!A0]SUB  .S1     A1, 1, A1                    ; 1 | decrement number of horizontal blocks
||  STDW      .D1     A17:A16, *A10++[1]           ; 1 | store 64-bit output
||  [!B0]ADD  .L2     B7, 8, B7                    ; 1 | first pointer to input X
||  [!B0]SUB  .S2     B1, 1, B1                    ; 1 | decrement number of horizontal blocks
||  STDW      .D2     B17:B16, *B10++[1]           ; 1 | store 64-bit output
; 23
    [A1]B     .S1     loopHoriz                    ; 6 | if number of horizontal blocks <> 0 go back
||  LDDW      .D1     *A7++[1], A31:A30            ; 5 | load 64-bit input
||  ADD       .L2     B7, B5, B8                   ; 1 | second pointer to input X
||  [!B1]B    .S2     B3                           ; 6 | if number of horizontal blocks == 0 return from function
||  LDDW      .D2     *B7++[1], B31:B30            ; 5 | load 64-bit input
; 24
    SHR       .S1     A6, 2, A0                    ; 1 | get number of vertical blocks (per four)
||  LDDW      .D1     *A8++[1], A29:A28            ; 5 | load 64-bit input
||  SHR       .S2     A6, 2, B0                    ; 1 | get number of vertical blocks (per four)
||  LDDW      .D2     *B8++[1], B29:B28            ; 5 | load 64-bit input
; 25
    SUB       .S1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  LDDW      .D1     *A7--[1], A27:A26            ; 5 | load 64-bit input
||  ADD       .L2     B4, B11, B4                  ; 1 | update pointer to output Y
||  SUB       .S2     B0, 1, B0                    ; 1 | decrement number of vertical blocks
||  LDDW      .D2     *B7--[1], B27:B26            ; 5 | load 64-bit input
; 26
    ADD       .L1     A7, A11, A7                  ; 1 | first pointer to input X
||  LDDW      .D1     *A8--[1], A25:A24            ; 5 | load 64-bit input
||  ADD       .L2     B7, B11, B7                  ; 1 | first pointer to input X
||  ADD       .S2     B4, B11, B4                  ; 1 | update pointer to output Y
||  LDDW      .D2     *B8--[1], B25:B24            ; 5 | load 64-bit input
; 27
    ADD       .L1     A8, A11, A8                  ; 1 | second pointer to input X
||  MV        .S1     B4, A9                       ; 1 | first pointer to output Y
||  LDDW      .D1     *A7++[1], A23:A22            ; 5 | load 64-bit input
||  ADD       .L2     B8, B11, B8                  ; 1 | second pointer to input X
||  ADD       .S2     B4, B11, B9                  ; 1 | first pointer to output Y
||  LDDW      .D2     *B7++[1], B23:B22            ; 5 | load 64-bit input
; 28
    ADD       .S1     A9, A5, A10                  ; 1 | second pointer to output Y
||  LDDW      .D1     *A8++[1], A21:A20            ; 5 | load 64-bit input
||  ADD       .S2     B9, B5, B10                  ; 1 | second pointer to output Y
||  LDDW      .D2     *B8++[1], B21:B20            ; 5 | load 64-bit input
