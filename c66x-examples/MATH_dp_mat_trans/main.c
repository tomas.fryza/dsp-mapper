/***********************************************************************
 * Title: Usage of transposition of double-precision float matrix functions
 * Author: Tomas Fryza, Brno University of Technology
 * Date: August 2017
 */


#include <stdio.h>
#include "MATH_dp_mat_trans.h"


/*
 *  Constants and macros
 */
#define     N   64


/*
 *  Function prototypes
 */
void init_mat(void);
void MATH_dp_mat_trans_cn(double*);


/*
 *  Global variables
 */
double x[N*N] = {0.0};
double y[N*N] = {0.0};


/***********************************************************************
 * Main function
 */
int main(void)
{
    int i;
    double diff = 0.0;

    puts("Start");

    /* Set the input data */
    init_mat();

    /* Matrix transposition in ASM-code */
//    MATH_dp_mat_trans4(x, y);
//    MATH_dp_mat_trans8(x, y);
    MATH_dp_mat_trans(x, y, N);

    /* Matrix transposition in C-code */
    MATH_dp_mat_trans_cn(x);

    /* Compare ASM- and C-code results */
    for (i = 0; i < (N*N); i++) {
        diff += y[i] - x[i];
    }

    puts("Stop");
    for (;;) {
    }
}


/***********************************************************************
 * Set the input data
 * param None
 * return None
 */
void init_mat()
{
    int i;
    for (i = 0; i < (N*N); i++) {
        x[i] = i;
    }
}


/***********************************************************************
 * Matrix transposition in C-code. Transposed matrix overwrites the input
 * param *x0 - Pointer to input matrix
 * return None
 */
void MATH_dp_mat_trans_cn(double *x0)
{
    int i, j;
    int idx;
    double temp = 0.0;

    for (i = 0; i < (N-1); i++) {
        idx = i*(N+1) + 1;
        for (j = 0; j < (N-1-i); j++) {
            temp = x0[(idx+j) + (j+1)*(N-1)];
            x0[(idx+j) + (j+1)*(N-1)] = x0[idx+j];
            x0[idx+j] = temp;
        }
    }
}
