;;
;  MATH_dp_mat_trans4.s
;
    .global MATH_dp_mat_trans4
MATH_dp_mat_trans4:
; 1
    MVKL      .S1     0x00000004, A6               ; 1 | N=4
; 2
    MVKH      .S1     0x00000004, A6               ; 1 | N=4
||  ADD       .L2     A4, 8, B7                    ; 1 | first pointer to input X
; 3
    MV        .L1     A4, A7                       ; 1 | first pointer to input X
||  SHL       .S1     A6, 3, A5                    ; 1 | 1*N*8
||  MV        .D1     B4, A9                       ; 1 | first pointer to output Y
||  ADD       .L2     B7, 8, B7                    ; 1 | first pointer to input X
||  SHL       .S2     A6, 3, B5                    ; 1 | 1*N*8
; 4
    ADD       .L1     A4, A5, A8                   ; 1 | second pointer to input X
||  SHL       .S1     A6, 4, A11                   ; 1 | 2*N*8
||  ADD       .D1     A9, A5, A10                  ; 1 | second pointer to output Y
||  ADD       .L2     B7, B5, B8                   ; 1 | second pointer to input X
||  SHL       .S2     A6, 4, B11                   ; 1 | 2*N*8
; 5
    ADD       .D2     B4, B11, B9                  ; 1 | first pointer to output Y
; 6
    ADD       .D2     B9, B5, B10                  ; 1 | second pointer to output Y
; 7
    LDDW      .D1     *A7++[1], A31:A30            ; 5 | load 64-bit input
||  LDDW      .D2     *B7++[1], B31:B30            ; 5 | load 64-bit input
; 8
    LDDW      .D1     *A8++[1], A29:A28            ; 5 | load 64-bit input
||  LDDW      .D2     *B8++[1], B29:B28            ; 5 | load 64-bit input
; 9
    LDDW      .D1     *A7--[1], A27:A26            ; 5 | load 64-bit input
||  LDDW      .D2     *B7--[1], B27:B26            ; 5 | load 64-bit input
; 10
    ADD       .L1     A7, A11, A7                  ; 1 | first pointer to input X
||  LDDW      .D1     *A8--[1], A25:A24            ; 5 | load 64-bit input
||  ADD       .L2     B7, B11, B7                  ; 1 | first pointer to input X
||  LDDW      .D2     *B8--[1], B25:B24            ; 5 | load 64-bit input
; 11
    ADD       .L1     A8, A11, A8                  ; 1 | second pointer to input X
||  LDDW      .D1     *A7++[1], A23:A22            ; 5 | load 64-bit input
||  ADD       .L2     B8, B11, B8                  ; 1 | second pointer to input X
||  LDDW      .D2     *B7++[1], B23:B22            ; 5 | load 64-bit input
; 12
    LDDW      .D1     *A8++[1], A21:A20            ; 5 | load 64-bit input
||  LDDW      .D2     *B8++[1], B21:B20            ; 5 | load 64-bit input
; 13
    LDDW      .D1     *A7--[1], A19:A18            ; 5 | load 64-bit input
||  LDDW      .D2     *B7--[1], B19:B18            ; 5 | load 64-bit input
; 14
    LDDW      .D1     *A8--[1], A17:A16            ; 5 | load 64-bit input
||  LDDW      .D2     *B8--[1], B17:B16            ; 5 | load 64-bit input
; 15
    STDW      .D1     A31:A30, *A9++[1]            ; 1 | store 64-bit output
||  STDW      .D2     B31:B30, *B9++[1]            ; 1 | store 64-bit output
; 16
    STDW      .D1     A27:A26, *A10++[1]           ; 1 | store 64-bit output
||  STDW      .D2     B27:B26, *B10++[1]           ; 1 | store 64-bit output
; 17
    STDW      .D1     A29:A28, *A9++[1]            ; 1 | store 64-bit output
||  B         .S2     B3                           ; 6 | return from function
||  STDW      .D2     B29:B28, *B9++[1]            ; 1 | store 64-bit output
; 18
    STDW      .D1     A25:A24, *A10++[1]           ; 1 | store 64-bit output
||  STDW      .D2     B25:B24, *B10++[1]           ; 1 | store 64-bit output
; 19
    STDW      .D1     A23:A22, *A9++[1]            ; 1 | store 64-bit output
||  STDW      .D2     B23:B22, *B9++[1]            ; 1 | store 64-bit output
; 20
    STDW      .D1     A19:A18, *A10++[1]           ; 1 | store 64-bit output
||  STDW      .D2     B19:B18, *B10++[1]           ; 1 | store 64-bit output
; 21
    STDW      .D1     A21:A20, *A9++[1]            ; 1 | store 64-bit output
||  STDW      .D2     B21:B20, *B9++[1]            ; 1 | store 64-bit output
; 22
    STDW      .D1     A17:A16, *A10++[1]           ; 1 | store 64-bit output
||  STDW      .D2     B17:B16, *B10++[1]           ; 1 | store 64-bit output
;
;
;;
;  CPU cycles:  22
;   .L1 :  4  ( 18.18 % )
;   .S1 :  4  ( 18.18 % )
;   .M1 :  0  ( 0.0 % )
;   .D1 :  18  ( 81.82 % )
;   .L2 :  5  ( 22.73 % )
;   .S2 :  3  ( 13.64 % )
;   .M2 :  0  ( 0.0 % )
;   .D2 :  18  ( 81.82 % )
