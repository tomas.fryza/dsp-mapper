;;
;  MATH_dp_mat_mul4.s
;
    .global MATH_dp_mat_mul4
MATH_dp_mat_mul4:
; 1
    MVKL      .S2     0x00000004, B6               ; 1 | N=4
; 2
    MVKH      .S2     0x00000004, B6               ; 1 | N=4
; 3
    SHL       .S1     B6, 3, A5                    ; 1 | 1*N*8
||  SHL       .S2     B6, 3, B5                    ; 1 | 1*N*8
; 4
    MV        .L1     A4, A7                       ; 1 | pointer to input A
||  SHL       .S1     B6, 4, A11                   ; 1 | 2*N*8
||  ADD       .L2     A4, B5, B7                   ; 1 | pointer to input A
||  SHL       .S2     B6, 4, B11                   ; 1 | 2*N*8
; 5
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 6
    MV        .L1     A6, A10                      ; 1 | pointer to output Y
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  ADD       .L2     A6, B5, B10                  ; 1 | pointer to output Y
; 7
    LDDW      .D1     *A3++[3], A25:A24            ; 5 | load 8-B input from B
; 8
    LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 9
    LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
; 10
    LDDW      .D1     *A3++[3], A21:A20            ; 5 | load 8-B input from B
; 11
    FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 12
    FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 13
    LDDW      .D1     *A3++[3], A17:A16            ; 5 | load 8-B input from B
; 14
    FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 15
    FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 16
    ADD       .S1     A7, A5, A7                   ; 1 | update pointer to input A
||  LDDW      .D1     *A3++[3], A21:A20            ; 5 | load 8-B input from B
||  ADD       .S2     B7, B5, B7                   ; 1 | update pointer to input A
; 17
    MV        .S1     B4, A3                       ; 1 | pointer to input B
||  FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 18
    FADDDP    .L1     A27:A26, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FADDDP    .L2     B27:B26, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
; 19
    FADDDP    .L1     A25:A24, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3++[3], A25:A24            ; 5 | load 8-B input from B
||  FADDDP    .L2     B25:B24, B21:B20, B29:B28    ; 3 |
; 20
    FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 21
    FADDDP    .L1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .L2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 22
    FADDDP    .L1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A3++[3], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .L2     B29:B28, B17:B16, B29:B28    ; 3 |
; 23
    FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 24
    FADDDP    .L1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FADDDP    .L2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 25
    FADDDP    .L1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3++[3], A17:A16            ; 5 | load 8-B input from B
||  FADDDP    .L2     B29:B28, B21:B20, B29:B28    ; 3 |
; 26
    FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  ADD       .S2     B4, 8, B4                    ; 1 |
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 27
    FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  ADD       .S2     B4, 8, B4                    ; 1 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 28
    LDDW      .D1     *A3++[3], A21:A20            ; 5 | load 8-B input from B
||  ADD       .S2     A4, B5, B7                   ; 1 | update pointer to input A
; 29
    FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  STDW      .D1     A31:A30, *A10++[1]           ; 1 | store 8-B output
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  STDW      .D2     B31:B30, *B10++[1]           ; 1 | store 8-B output
; 30
    FADDDP    .L1     A27:A26, A23:A22, A31:A30    ; 3 |
||  MV        .S1     A4, A7                       ; 1 | update pointer to input A
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  STDW      .D1     A29:A28, *A10--[1]           ; 1 | store 8-B output
||  FADDDP    .L2     B27:B26, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  STDW      .D2     B29:B28, *B10--[1]           ; 1 | store 8-B output
; 31
    FADDDP    .L1     A25:A24, A21:A20, A29:A28    ; 3 |
||  MV        .S1     B4, A3                       ; 1 | pointer to input B
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FADDDP    .L2     B25:B24, B21:B20, B29:B28    ; 3 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 32
    FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
; 33
    FADDDP    .L1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[3], A25:A24            ; 5 | load 8-B input from B
||  FADDDP    .L2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 34
    FADDDP    .L1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FADDDP    .L2     B29:B28, B17:B16, B29:B28    ; 3 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 35
    LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
; 36
    FADDDP    .L1     A31:A30, A23:A22, A31:A30    ; 3 |
||  LDDW      .D1     *A3++[3], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .L2     B31:B30, B23:B22, B31:B30    ; 3 |
; 37
    FADDDP    .L1     A29:A28, A21:A20, A29:A28    ; 3 |
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FADDDP    .L2     B29:B28, B21:B20, B29:B28    ; 3 |
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 38
    FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 39
    LDDW      .D1     *A3++[3], A17:A16            ; 5 | load 8-B input from B
; 40
    FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 41
    FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 42
    ADD       .S1     A10, A11, A10                ; 1 | update pointer to output Y
||  LDDW      .D1     *A3++[3], A21:A20            ; 5 | load 8-B input from B
||  ADD       .S2     B10, B11, B10                ; 1 | update pointer to output Y
; 43
    FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  STDW      .D1     A31:A30, *A10++[1]           ; 1 | store 8-B output
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  STDW      .D2     B31:B30, *B10++[1]           ; 1 | store 8-B output
; 44
    FADDDP    .L1     A27:A26, A23:A22, A31:A30    ; 3 |
||  ADD       .S1     A7, A5, A7                   ; 1 | update pointer to input A
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  STDW      .D1     A29:A28, *A10--[1]           ; 1 | store 8-B output
||  FADDDP    .L2     B27:B26, B23:B22, B31:B30    ; 3 |
||  ADD       .S2     B7, B5, B7                   ; 1 | update pointer to input A
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  STDW      .D2     B29:B28, *B10--[1]           ; 1 | store 8-B output
; 45
    FADDDP    .L1     A25:A24, A21:A20, A29:A28    ; 3 |
||  MV        .S1     B4, A3                       ; 1 | update pointer to input B
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FADDDP    .L2     B25:B24, B21:B20, B29:B28    ; 3 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 46
    FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
; 47
    FADDDP    .L1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[3], A25:A24            ; 5 | load 8-B input from B
||  FADDDP    .L2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 48
    FADDDP    .L1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FADDDP    .L2     B29:B28, B17:B16, B29:B28    ; 3 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 49
    LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
; 50
    FADDDP    .L1     A31:A30, A23:A22, A31:A30    ; 3 |
||  LDDW      .D1     *A3++[3], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .L2     B31:B30, B23:B22, B31:B30    ; 3 |
; 51
    FADDDP    .L1     A29:A28, A21:A20, A29:A28    ; 3 |
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FADDDP    .L2     B29:B28, B21:B20, B29:B28    ; 3 |
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 52
    FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 53
    LDDW      .D1     *A3++[3], A17:A16            ; 5 | load 8-B input from B
; 54
    ADD       .S1     A6, 8, A6                    ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 55
    ADD       .S1     A6, 8, A6                    ; 1 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 56
    MV        .S1     A6, A10                      ; 1 | update pointer to output Y
||  LDDW      .D1     *A3++[3], A21:A20            ; 5 | load 8-B input from B
||  ADD       .S2     A6, B5, B10                  ; 1 | update pointer to output Y
; 57
    FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  STDW      .D1     A31:A30, *A10++[1]           ; 1 | store 8-B output
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  STDW      .D2     B31:B30, *B10++[1]           ; 1 | store 8-B output
; 58
    FADDDP    .L1     A27:A26, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  STDW      .D1     A29:A28, *A10--[1]           ; 1 | store 8-B output
||  FADDDP    .L2     B27:B26, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  STDW      .D2     B29:B28, *B10--[1]           ; 1 | store 8-B output
; 59
    FADDDP    .L1     A25:A24, A21:A20, A29:A28    ; 3 |
||  FADDDP    .L2     B25:B24, B21:B20, B29:B28    ; 3 |
; 60
    FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
; 61
    FADDDP    .L1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  FADDDP    .L2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 62
    FADDDP    .L1     A29:A28, A17:A16, A29:A28    ; 3 |
||  FADDDP    .L2     B29:B28, B17:B16, B29:B28    ; 3 |
; 63
    B         .S2     B3                           ; 6 | return from function
; 64
    FADDDP    .L1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FADDDP    .L2     B31:B30, B23:B22, B31:B30    ; 3 |
; 65
    FADDDP    .L1     A29:A28, A21:A20, A29:A28    ; 3 |
||  FADDDP    .L2     B29:B28, B21:B20, B29:B28    ; 3 |
; 66
    ADD       .S1     A10, A11, A10                ; 1 | update pointer to output Y
||  ADD       .S2     B10, B11, B10                ; 1 | update pointer to output Y
; 67
    STDW      .D1     A31:A30, *A10++[1]           ; 1 | store 8-B output
||  STDW      .D2     B31:B30, *B10++[1]           ; 1 | store 8-B output
; 68
    STDW      .D1     A29:A28, *A10--[1]           ; 1 | store 8-B output
||  STDW      .D2     B29:B28, *B10--[1]           ; 1 | store 8-B output
