/**
 ***********************************************************************
 * @file MATH_dp_mat_mul.h
 *
 * @brief Multiplication of double-precision float matrices.
 * @author Tomas Fryza, Brno University of Technology, Czech Republic
 */


#ifndef MATH_DP_MAT_MUL_H_INCLUDED
#define MATH_DP_MAT_MUL_H_INCLUDED


/**
 ***********************************************************************
 * @ingroup MATH
 * @{
 * @defgroup MATH_dp_mat_mul MATH_dp_mat_mul
 * @brief Multiplication of double-precision float matrices.
 * @{
 */


/**
 ***********************************************************************
 * @brief Multiplication of double-precision square matrices of order 4.
 *
 * @param a - Pointer to input matrix A
 * @param b - Pointer to input matrix B
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns matrices is equal to 4.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 68<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_dp_mat_mul4(double *a, double *b, double *y);


/**
 ***********************************************************************
 * @brief Multiplication of double-precision square matrices of order 8.
 *
 * @param a - Pointer to input matrix A
 * @param b - Pointer to input matrix B
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 8.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 430<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_dp_mat_mul8(double *a, double *b, double *y);


/**
 ***********************************************************************
 * @brief Multiplication of double-precision square matrices of order N.
 *
 * @param a - Pointer to input matrix A
 * @param b - Pointer to input matrix B
 * @param y - Pointer to output matrix
 * @param N - Order of square matrix
 *
 * @par Assumptions:
 *    Number of rows and columns must be a multiple of 8 and >= 16.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 3/4*N*N*N + 1/2*N*N + 5*N + 8<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_dp_mat_mul(double *a, double *b, double *y, int N);


/** @} defgroup MATH_dp_mat_mul */
#endif /* MATH_DP_MAT_MUL_H_INCLUDED */
