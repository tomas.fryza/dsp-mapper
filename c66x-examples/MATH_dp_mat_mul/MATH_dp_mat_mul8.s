;;
;  MATH_dp_mat_mul8.s
;
    .global MATH_dp_mat_mul8
MATH_dp_mat_mul8:
; 1
    MVKL      .S2     0x00000008, B6               ; 1 | N=8
; 2
    MVKH      .S2     0x00000008, B6               ; 1 | N=8
; 3
    SHL       .S1     B6, 3, A5                    ; 1 | 1*N*8
||  SHL       .S2     B6, 3, B5                    ; 1 | 1*N*8
; 4
    MV        .L1     A4, A7                       ; 1 | pointer to input A
||  SHL       .S1     B6, 4, A11                   ; 1 | 2*N*8
||  ADD       .L2     A4, B5, B7                   ; 1 | pointer to input A
||  SHL       .S2     B6, 4, B11                   ; 1 | 2*N*8
; 5
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 6
    SHR       .S1     B6, 1, A1                    ; 1 | get number of horizontal blocks (per two)
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  SHR       .S2     B6, 1, B1                    ; 1 | get number of horizontal blocks (per two)
; 7
    LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
; 8
    ADD       .L1     A3, A5, A3                   ; 1 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 9
    LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
; 10
    LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
; 11
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 12
    FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 13
    LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
; 14
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 15
loopHoriz:
    MV        .L1     A6, A10                      ; 1 | pointer to output Y
||  SHR       .S1     B6, 1, A0                    ; 1 | get number of vertical blocks (per two)
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  SHR       .S2     B6, 1, B0                    ; 1 | get number of vertical blocks (per two)
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 16
    SUB       .L1     A10, A11, A10                ; 1 | pointer to output Y
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  ADD       .L2     A6, B5, B10                  ; 1 | pointer to output Y
; 17
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  SUB       .L2     B10, B11, B10                ; 1 | pointer to output Y
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 18
    SUB       .L1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  FADDDP    .S1     A27:A26, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  SUB       .L2     B0, 1, B0                    ; 1 | decrement number of vertical blocks
||  FADDDP    .S2     B27:B26, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
; 19
    FADDDP    .S1     A25:A24, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
||  FADDDP    .S2     B25:B24, B21:B20, B29:B28    ; 3 |
; 20
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 21
    FADDDP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 22
    FADDDP    .S1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B17:B16, B29:B28    ; 3 |
; 23
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 24
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 25
    FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 26
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 27
    ADD       .L1     A7, A5, A7                   ; 1 | update pointer to input A
||  FADDDP    .S1     A31:A30, A27:A26, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  ADD       .L2     B7, B5, B7                   ; 1 | update pointer to input A
||  FADDDP    .S2     B31:B30, B27:B26, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 28
    FADDDP    .S1     A29:A28, A25:A24, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B25:B24, B29:B28    ; 3 |
; 29
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 30
loopVert:
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
; 31
    SUB       .L1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
||  SUB       .L2     B0, 1, B0                    ; 1 | decrement number of vertical blocks
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 32
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 33
    FADDDP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 34
    FADDDP    .S1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B17:B16, B29:B28    ; 3 |
; 35
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 36
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 37
    FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 38
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 39
    FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 40
    ADD       .L1     A10, A11, A10                ; 1 | pointer to output Y
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  ADD       .L2     B10, B11, B10                ; 1 | pointer to output Y
; 41
    FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  STDW      .D1     A31:A30, *A10++[1]           ; 1 | store 8-B output
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  STDW      .D2     B31:B30, *B10++[1]           ; 1 | store 8-B output
; 42
    FADDDP    .S1     A27:A26, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  STDW      .D1     A29:A28, *A10--[1]           ; 1 | store 8-B output
||  FADDDP    .S2     B27:B26, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  STDW      .D2     B29:B28, *B10--[1]           ; 1 | store 8-B output
; 43
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FADDDP    .S1     A25:A24, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FADDDP    .S2     B25:B24, B21:B20, B29:B28    ; 3 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 44
    FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
; 45
    FADDDP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 46
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FADDDP    .S1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FADDDP    .S2     B29:B28, B17:B16, B29:B28    ; 3 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 47
    LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
; 48
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
; 49
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 50
    [A0]B     .S1     loopVert                     ; 6 | if number of vertical blocks <> 0 go back
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  [!B0]ADD  .L2     B4, 8, B4                    ; 1 | update pointer to input B
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 51
    LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
||  [!B0]ADD  .L2     B4, 8, B4                    ; 1 | update pointer to input B
; 52
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 53
    [A0]ADD   .L1     A7, A5, A7                   ; 1 | update pointer to input A
||  FADDDP    .S1     A31:A30, A27:A26, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  [B0]ADD   .L2     B7, B5, B7                   ; 1 | update pointer to input A
||  FADDDP    .S2     B31:B30, B27:B26, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 54
    [!A0]MV   .L1     A4, A7                       ; 1 | update pointer to input A
||  FADDDP    .S1     A29:A28, A25:A24, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  [!B0]ADD  .L2     A4, B5, B7                   ; 1 | update pointer to input A
||  FADDDP    .S2     B29:B28, B25:B24, B29:B28    ; 3 |
; 55
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 56
    ADD       .L1     A6, 8, A6                    ; 1 | update pointer to output Y
||  FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
; 57
    ADD       .L1     A6, 8, A6                    ; 1 | update pointer to output Y
||  FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 58
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 59
    FADDDP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 60
    SUB       .L1     A1, 1, A1                    ; 1 | decrement number of horizontal blocks
||  FADDDP    .S1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  SUB       .L2     B1, 1, B1                    ; 1 | decrement number of horizontal blocks
||  FADDDP    .S2     B29:B28, B17:B16, B29:B28    ; 3 |
; 61
    ADD       .L1     A3, A5, A3                   ; 1 |
||  [A1]B     .S1     loopHoriz                    ; 6 | if number of horizontal blocks <> 0 go back
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  [!B1]B    .S2     B3                           ; 6 | if number of horizontal blocks == 0 return from function
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 62
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 63
    ADD       .L1     A10, A11, A10                ; 1 | pointer to output Y
||  FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
||  ADD       .L2     B10, B11, B10                ; 1 | pointer to output Y
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 64
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 65
    STDW      .D1     A31:A30, *A10++[1]           ; 1 | store 8-B output
||  STDW      .D2     B31:B30, *B10++[1]           ; 1 | store 8-B output
; 66
    STDW      .D1     A29:A28, *A10--[1]           ; 1 | store 8-B output
||  STDW      .D2     B29:B28, *B10--[1]           ; 1 | store 8-B output
