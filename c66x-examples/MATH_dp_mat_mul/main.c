/***********************************************************************
 * Title: Usage of multiplication of double-precision float matrices functions
 * Author: Tomas Fryza, Brno University of Technology
 * Date: September 2017
 */


#include <stdio.h>
#include "MATH_dp_mat_mul.h"


/*
 *  Constants and macros
 */
#define     N   64


/*
 *  Function prototypes
 */
void init_mat(void);
void MATH_dp_mat_mul_cn(double*, double*, double*);


/*
 *  Global variables
 */
double a[N*N] = {0.0};
double b[N*N] = {0.0};
double x[N*N] = {0.0};
double y[N*N] = {0.0};


/***********************************************************************
 * Main function
 */
int main(void)
{
    int i;
    int ndiff = 0;

    puts("Start");

    /* Set the input data */
    init_mat();

    /* Matrix multiplication in ASM-code */
//    MATH_dp_mat_mul4(a, b, y);
//    MATH_dp_mat_mul8(a, b, y);
    MATH_dp_mat_mul(a, b, y, N);

    /* Matrix multiplication in C-code */
    MATH_dp_mat_mul_cn(a, b, x);

    /* Compare ASM- and C-code results */
    for (i = 0; i < (N*N); i++) {
        if (y[i]-x[i] != 0) {
            ndiff++;
        }
    }

    puts("Stop");
    for (;;) {
    }
}


/***********************************************************************
 * Set the input data
 * param None
 * return None
 */
void init_mat()
{
    int i;
    for (i = 0; i < (N*N); i++) {
        a[i] = i;
        b[i] = i;
//        a[i] = 2.0e150 * i;
//        b[i] = 2.0e150 * i;
    }
}


/***********************************************************************
 * Matrix multiplication in C-code. Non-optimized algorithm
 * param *pa - Pointer to input matrix A
 * param *pb - Pointer to input matrix B
 * param *px - Pointer to output matrix
 * return None
 */
void MATH_dp_mat_mul_cn(double *pa, double *pb, double *px)
{
    int m, n, i;
    double temp = 0.0;

    for (m = 0; m < N; m++) {
        for (n = 0; n < N; n++) {
            temp = 0.0;
            for (i = 0; i < N; i++) {
                temp = temp + *(pa+m*N+i) * *(pb+n+N*i);
            }
            *(px+n+m*N) = temp;
        }
    }
}
