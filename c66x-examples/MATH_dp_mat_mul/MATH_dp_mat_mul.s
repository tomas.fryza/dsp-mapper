;;
;  MATH_dp_mat_mul.s
;
    .global MATH_dp_mat_mul
MATH_dp_mat_mul:
; 1
    SHL       .S1     B6, 3, A5                    ; 1 | 1*N*8
||  SHL       .S2     B6, 3, B5                    ; 1 | 1*N*8
; 2
    MV        .L1     A4, A7                       ; 1 | pointer to input A
||  SHL       .S1     B6, 4, A11                   ; 1 | 2*N*8
||  ADD       .L2     A4, B5, B7                   ; 1 | pointer to input A
||  SHL       .S2     B6, 4, B11                   ; 1 | 2*N*8
; 3
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 4
    SHR       .S1     B6, 1, A1                    ; 1 | get number of horizontal blocks (per two)
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  SHR       .S2     B6, 1, B1                    ; 1 | get number of horizontal blocks (per two)
; 5
    LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
; 6
    ADD       .L1     A3, A5, A3                   ; 1 |
||  SHR       .S1     B6, 3, A2                    ; 1 | get number of horizontal blocks (per eight)
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  SHR       .S2     B6, 3, B2                    ; 1 | get number of horizontal blocks (per eight)
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 7
    SUB       .S1     A2, 1, A2                    ; 1 | decrement number of horizontal blocks
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  SUB       .S2     B2, 1, B2                    ; 1 | decrement number of horizontal blocks
; 8
    LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
; 9
loopHoriz:
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 10
    MV        .L1     A6, A10                      ; 1 | pointer to output Y
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 11
    SUB       .L1     A10, A11, A10                ; 1 | pointer to output Y
||  LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
||  ADD       .L2     A6, B5, B10                  ; 1 | pointer to output Y
; 12
    ADD       .L1     A3, A5, A3                   ; 1 |
||  SHR       .S1     B6, 1, A0                    ; 1 | get number of vertical blocks (per two)
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  SUB       .L2     B10, B11, B10                ; 1 | pointer to output Y
||  SHR       .S2     B6, 1, B0                    ; 1 | get number of vertical blocks (per two)
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 13
loopVert:
    SUB       .S1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  SUB       .S2     B0, 1, B0                    ; 1 | decrement number of vertical blocks
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 14
    LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
; 15
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 16
    FADDDP    .S1     A27:A26, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FADDDP    .S2     B27:B26, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
; 17
    FADDDP    .S1     A25:A24, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
||  FADDDP    .S2     B25:B24, B21:B20, B29:B28    ; 3 |
; 18
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 19
    FADDDP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 20
    FADDDP    .S1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B17:B16, B29:B28    ; 3 |
; 21
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 22
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 23
    FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 24
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 25
    FADDDP    .S1     A31:A30, A27:A26, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B27:B26, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 26
    FADDDP    .S1     A29:A28, A25:A24, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B25:B24, B29:B28    ; 3 |
; 27
nextBlock:
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 28
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
; 29
    SUB       .L1     A2, 1, A2                    ; 1 | decrement number of horizontal blocks
||  FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
||  SUB       .L2     B2, 1, B2                    ; 1 | decrement number of horizontal blocks
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 30
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 31
    FADDDP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 32
    FADDDP    .S1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B17:B16, B29:B28    ; 3 |
; 33
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 34
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 35
    FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 36
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 37
    FADDDP    .S1     A31:A30, A27:A26, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B27:B26, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 38
    FADDDP    .S1     A29:A28, A25:A24, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B25:B24, B29:B28    ; 3 |
; 39
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 40
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
; 41
    FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 42
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 43
    FADDDP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 44
    FADDDP    .S1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B17:B16, B29:B28    ; 3 |
; 45
    ADD       .L1     A3, A5, A3                   ; 1 |
||  [A2]B     .S1     nextBlock                    ; 6 | if number of horizontal blocks <> 0 go back
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 46
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 47
    FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 48
    ADD       .L1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 49
    FADDDP    .S1     A31:A30, A27:A26, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B27:B26, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 50
    [!A2]ADD  .L1     A7, A5, A7                   ; 1 | update pointer to input A
||  FADDDP    .S1     A29:A28, A25:A24, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  [!B2]ADD  .L2     B7, B5, B7                   ; 1 | update pointer to input A
||  FADDDP    .S2     B29:B28, B25:B24, B29:B28    ; 3 |
; 51
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  FMPYDP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 52
    FADDDP    .L1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
||  FADDDP    .L2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
; 53
    FADDDP    .L1     A29:A28, A21:A20, A29:A28    ; 3 |
||  SHR       .S1     B6, 3, A2                    ; 1 | get number of horizontal blocks (per eight)
||  LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
||  FADDDP    .L2     B29:B28, B21:B20, B29:B28    ; 3 |
||  SHR       .S2     B6, 3, B2                    ; 1 | get number of horizontal blocks (per eight)
; 54
    ADD       .S1     A3, A5, A3                   ; 1 |
||  FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 55
    FADDDP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  FMPYDP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
||  FMPYDP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 56
    SUB       .L1     A2, 1, A2                    ; 1 | decrement number of horizontal blocks
||  FADDDP    .S1     A29:A28, A17:A16, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
||  SUB       .L2     B2, 1, B2                    ; 1 | decrement number of horizontal blocks
||  FADDDP    .S2     B29:B28, B17:B16, B29:B28    ; 3 |
; 57
    ADD       .L1     A3, A5, A3                   ; 1 |
||  [A0]B     .S1     loopVert                     ; 6 | if number of vertical blocks <> 0 go back
||  FMPYDP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  FMPYDP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 58
    FADDDP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  FMPYDP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *A3++[1], A19:A18            ; 5 | load 8-B input from B
||  FADDDP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
||  FMPYDP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 59
    ADD       .L1     A10, A11, A10                ; 1 | pointer to output Y
||  FADDDP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3--[1], A17:A16            ; 5 | load 8-B input from B
||  ADD       .L2     B10, B11, B10                ; 1 | pointer to output Y
||  FADDDP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 60
    FMPYDP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  [!B0]ADD  .L2     B4, 8, B4                    ; 1 | update pointer to input B
||  FMPYDP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 61
    ADD       .L1     A3, A5, A3                   ; 1 |
||  STDW      .D1     A31:A30, *A10++[1]           ; 1 | store 8-B output
||  [!B0]ADD  .L2     B4, 8, B4                    ; 1 | update pointer to input B
||  STDW      .D2     B31:B30, *B10++[1]           ; 1 | store 8-B output
; 62
    [!A0]MV   .L1     A4, A7                       ; 1 | pointer to input A
||  [!A0]SUB  .S1     A1, 1, A1                    ; 1 | decrement number of horizontal blocks
||  STDW      .D1     A29:A28, *A10--[1]           ; 1 | store 8-B output
||  [!B0]ADD  .L2     A4, B5, B7                   ; 1 | update pointer to input A
||  [!B0]SUB  .S2     B1, 1, B1                    ; 1 | decrement number of horizontal blocks
||  STDW      .D2     B29:B28, *B10--[1]           ; 1 | store 8-B output
; 63
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  [A1]B     .S1     loopHoriz                    ; 6 | if number of horizontal blocks <> 0 go back
||  LDDW      .D1     *A7++[1], A9:A8              ; 5 | load 8-B input from A
||  [!B1]B    .S2     B3                           ; 6 | if number of horizontal blocks == 0 return from function
||  LDDW      .D2     *B7++[1], B9:B8              ; 5 | load 8-B input from A
; 64
    ADD       .L1     A6, 8, A6                    ; 1 | update pointer to output Y
||  LDDW      .D1     *A3++[1], A27:A26            ; 5 | load 8-B input from B
; 65
    ADD       .L1     A6, 8, A6                    ; 1 | update pointer to output Y
||  LDDW      .D1     *A3--[1], A25:A24            ; 5 | load 8-B input from B
; 66
    ADD       .L1     A3, A5, A3                   ; 1 |
||  LDDW      .D1     *A7++[1], A13:A12            ; 5 | load 8-B input from A
||  LDDW      .D2     *B7++[1], B13:B12            ; 5 | load 8-B input from A
; 67
    LDDW      .D1     *A3++[1], A23:A22            ; 5 | load 8-B input from B
; 68
    LDDW      .D1     *A3--[1], A21:A20            ; 5 | load 8-B input from B
