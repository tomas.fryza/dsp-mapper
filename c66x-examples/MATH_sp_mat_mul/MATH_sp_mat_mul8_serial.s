;;
;  MATH_sp_mat_mul8_serial.s
;
    .global MATH_sp_mat_mul8_serial
MATH_sp_mat_mul8_serial:
; 1
    MVKL      .S2     0x00000008, B6               ; 1 | N=8
; 2
    MVKH      .S2     0x00000008, B6               ; 1 | N=8
; 3
    SHL       .S1     B6, 2, A5                    ; 1 | 1*N*4
||  SHL       .S2     B6, 2, B5                    ; 1 | 1*N*4
; 4
    SHL       .S1     B6, 3, A11                   ; 1 | 2*N*4
||  SHL       .S2     B6, 3, B11                   ; 1 | 2*N*4
; 5
    MV        .L1     A4, A7                       ; 1 | pointer to input A
||  SHR       .S1     B6, 1, A1                    ; 1 | get number of horizontal blocks (per two)
||  ADD       .L2     A4, B5, B7                   ; 1 | pointer to input A
||  SHR       .S2     B6, 1, B1                    ; 1 | get number of horizontal blocks (per two)
; 6
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 7
    SHR       .S1     B6, 1, A0                    ; 1 | get number of vertical blocks (per two)
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
; 8
    NOP
; 9
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 10
    SUB       .L1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
; 11
    MV        .L1     A13, A12                     ; 1 |
||  MV        .L2     B13, B12                     ; 1 |
; 12
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 13
    LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
; 14
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 15
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 16
    LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
; 17
    MV        .L1     A13, A12                     ; 1 |
||  MV        .L2     B13, B12                     ; 1 |
; 18
loopHoriz:
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 19
    DADDSP    .S1     A27:A26, A25:A24, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B27:B26, B25:B24, B31:B30    ; 3 |
; 20
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 21
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 22
    DADDSP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A17:A16                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
; 23
    MV        .L1     A13, A12                     ; 1 |
||  MV        .L2     B13, B12                     ; 1 |
; 24
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A13:A12, A19:A18    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B13:B12, B19:B18    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 25
    DADDSP    .S1     A31:A30, A21:A20, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B21:B20, B31:B30    ; 3 |
; 26
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 27
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 28
    DADDSP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
; 29
    MV        .L1     A13, A12                     ; 1 |
||  ADD       .S1     A7, A5, A7                   ; 1 | update pointer to input A
||  MV        .L2     B13, B12                     ; 1 |
||  ADD       .S2     B7, B5, B7                   ; 1 | update pointer to input A
; 30
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  DMPYSP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 31
    SUB       .L1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  DADDSP    .S1     A31:A30, A17:A16, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B17:B16, B31:B30    ; 3 |
; 32
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 33
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 34
    SUB       .L1     A1, 1, A1                    ; 1 | decrement number of horizontal blocks
||  DADDSP    .S1     A31:A30, A21:A20, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  SUB       .L2     B1, 1, B1                    ; 1 | decrement number of horizontal blocks
||  DADDSP    .S2     B31:B30, B21:B20, B31:B30    ; 3 |
; 35
    MV        .L1     A13, A12                     ; 1 |
||  MV        .L2     B13, B12                     ; 1 |
; 36
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 37
    DADDSP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
; 38
    MV        .L1     A9, A8                       ; 1 |
||  MV        .S1     A6, A10                      ; 1 | pointer to output Y
||  MV        .L2     B9, B8                       ; 1 |
||  ADD       .S2     A6, B5, B10                  ; 1 | pointer to output Y
; 39
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 40
    LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
; 41
    MV        .L1     A13, A12                     ; 1 |
||  STDW      .D1     A31:A30, *A10                ; 1 | store two 4-B outputs
||  MV        .L2     B13, B12                     ; 1 |
||  STDW      .D2     B31:B30, *B10                ; 1 | store two 4-B outputs
; 42
loopVert:
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 43
    DADDSP    .S1     A27:A26, A25:A24, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B27:B26, B25:B24, B31:B30    ; 3 |
; 44
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 45
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 46
    DADDSP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A17:A16                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
; 47
    MV        .L1     A13, A12                     ; 1 |
||  MV        .L2     B13, B12                     ; 1 |
; 48
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A13:A12, A19:A18    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B13:B12, B19:B18    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 49
    DADDSP    .S1     A31:A30, A21:A20, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B21:B20, B31:B30    ; 3 |
; 50
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 51
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 52
    DADDSP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
; 53
    MV        .L1     A13, A12                     ; 1 |
||  ADD       .S1     A7, A5, A7                   ; 1 | update pointer to input A
||  MV        .L2     B13, B12                     ; 1 |
||  ADD       .S2     B7, B5, B7                   ; 1 | update pointer to input A
; 54
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  DMPYSP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 55
    SUB       .L1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  DADDSP    .S1     A31:A30, A17:A16, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B17:B16, B31:B30    ; 3 |
; 56
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 57
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 58
    DADDSP    .S1     A31:A30, A21:A20, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B21:B20, B31:B30    ; 3 |
; 59
    MV        .L1     A13, A12                     ; 1 |
||  MV        .L2     B13, B12                     ; 1 |
; 60
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  [A0]B     .S1     loopVert                     ; 6 | if number of vertical blocks <> 0 go back
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 61
    ADD       .L1     A10, A11, A10                ; 1 | update pointer to output Y
||  DADDSP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  ADD       .L2     B10, B11, B10                ; 1 | update pointer to output Y
||  DADDSP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
; 62
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 63
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 64
    LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
; 65
    MV        .L1     A13, A12                     ; 1 |
||  STDW      .D1     A31:A30, *A10                ; 1 | store two 4-B outputs
||  MV        .L2     B13, B12                     ; 1 |
||  STDW      .D2     B31:B30, *B10                ; 1 | store two 4-B outputs
; 66
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 67
    DADDSP    .S1     A27:A26, A25:A24, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B27:B26, B25:B24, B31:B30    ; 3 |
; 68
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 69
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 70
    DADDSP    .S1     A31:A30, A23:A22, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A17:A16                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B23:B22, B31:B30    ; 3 |
; 71
    MV        .L1     A13, A12                     ; 1 |
||  MV        .L2     B13, B12                     ; 1 |
; 72
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A13:A12, A19:A18    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B13:B12, B19:B18    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 73
    DADDSP    .S1     A31:A30, A21:A20, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B21:B20, B31:B30    ; 3 |
; 74
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 75
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 76
    DADDSP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  ADD       .L2     B4, 8, B4                    ; 1 | update pointer to input B
||  DADDSP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
; 77
    MV        .L1     A13, A12                     ; 1 |
||  MV        .S1     A4, A7                       ; 1 | pointer to input A
||  MV        .L2     B13, B12                     ; 1 |
||  ADD       .S2     A4, B5, B7                   ; 1 | pointer to input A
; 78
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  DMPYSP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 79
    DADDSP    .S1     A31:A30, A17:A16, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B17:B16, B31:B30    ; 3 |
; 80
    MV        .L1     A9, A8                       ; 1 |
||  SHR       .S1     B6, 1, A0                    ; 1 | get number of vertical blocks (per two)
||  MV        .L2     B9, B8                       ; 1 |
; 81
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 82
    ADD       .L1     A10, A11, A10                ; 1 | update pointer to output Y
||  DADDSP    .S1     A31:A30, A21:A20, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  ADD       .L2     B10, B11, B10                ; 1 | update pointer to output Y
||  DADDSP    .S2     B31:B30, B21:B20, B31:B30    ; 3 |
; 83
    MV        .L1     A13, A12                     ; 1 |
||  SUB       .S1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  MV        .L2     B13, B12                     ; 1 |
; 84
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  [A1]B     .S1     loopHoriz                    ; 6 | if number of horizontal blocks <> 0 go back
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  [!B1]B    .S2     B3                           ; 6 | if number of horizontal blocks == 0 return
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 85
    ADD       .L1     A6, 8, A6                    ; 1 | update pointer to output Y
||  DADDSP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
; 86
    MV        .L1     A9, A8                       ; 1 |
||  MV        .L2     B9, B8                       ; 1 |
; 87
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 88
    LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
; 89
    MV        .L1     A13, A12                     ; 1 |
||  STDW      .D1     A31:A30, *A10                ; 1 | store two 4-B outputs
||  MV        .L2     B13, B12                     ; 1 |
||  STDW      .D2     B31:B30, *B10                ; 1 | store two 4-B outputs
