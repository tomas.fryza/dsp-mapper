;;
;  MATH_sp_mat_mul8.s
;
    .global MATH_sp_mat_mul8
MATH_sp_mat_mul8:
; 1
    MVKL      .S2     0x00000008, B6               ; 1 | N=8
; 2
    MVKH      .S2     0x00000008, B6               ; 1 | N=8
; 3
    SHL       .S1     B6, 2, A5                    ; 1 | 1*N*4
||  SHL       .S2     B6, 2, B5                    ; 1 | 1*N*4
; 4
    SHL       .S1     B6, 3, A11                   ; 1 | 2*N*4
||  SHL       .S2     B6, 3, B11                   ; 1 | 2*N*4
; 5
    MV        .L1     A4, A7                       ; 1 | pointer to input A
||  SHR       .S1     B6, 1, A1                    ; 1 | get number of horizontal blocks (per two)
||  ADD       .L2     A4, B5, B7                   ; 1 | pointer to input A
||  SHR       .S2     B6, 1, B1                    ; 1 | get number of horizontal blocks (per two)
; 6
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 7
    SHR       .S1     B6, 1, A0                    ; 1 | get number of vertical blocks (per two)
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
; 8
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 9
    SUB       .L1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
; 10
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 11
    MV        .S1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B13, B12                     ; 1 |
; 12
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 13
    MV        .S1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B9, B8                       ; 1 |
; 14
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 15
    MV        .S1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B13, B12                     ; 1 |
; 16
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 17
    MV        .S1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A17:A16                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B9, B8                       ; 1 |
; 18
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A27:A26, A25:A24, A29:A28    ; 3 |
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B27:B26, B25:B24, B29:B28    ; 3 |
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 19
    MV        .S1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B13, B12                     ; 1 |
; 20
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A13:A12, A19:A18    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B13:B12, B19:B18    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 21
    ADD       .L1     A7, A5, A7                   ; 1 | update pointer to input A
||  MV        .S1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  ADD       .L2     B7, B5, B7                   ; 1 | update pointer to input A
||  MV        .S2     B9, B8                       ; 1 |
; 22
loopHoriz:
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  DADDSP    .S1     A23:A22, A21:A20, A31:A30    ; 3 |
||  DMPYSP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B23:B22, B21:B20, B31:B30    ; 3 |
||  DMPYSP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 23
    SUB       .L1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  MV        .S1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B13, B12                     ; 1 |
; 24
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A29:A28, A19:A18, A29:A28    ; 3 |
||  DMPYSP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B29:B28, B19:B18, B29:B28    ; 3 |
||  DMPYSP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 25
    MV        .S1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B9, B8                       ; 1 |
; 26
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A17:A16, A31:A30    ; 3 |
||  DMPYSP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B17:B16, B31:B30    ; 3 |
||  DMPYSP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 27
    MV        .L1     A13, A12                     ; 1 |
||  SUB       .S1     A1, 1, A1                    ; 1 | decrement number of horizontal blocks
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
||  SUB       .S2     B1, 1, B1                    ; 1 | decrement number of horizontal blocks
; 28
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 29
    MV        .L1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
; 30
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 31
    MV        .L1     A13, A12                     ; 1 |
||  MV        .S1     A6, A10                      ; 1 | pointer to output Y
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
||  ADD       .S2     A6, B5, B10                  ; 1 | pointer to output Y
; 32
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 33
    DADDSP    .L1     A31:A30, A29:A28, A31:A30    ; 3 |
||  MV        .S1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A17:A16                 ; 5 | load two 4-B inputs from B
||  DADDSP    .L2     B31:B30, B29:B28, B31:B30    ; 3 |
||  MV        .S2     B9, B8                       ; 1 |
; 34
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A27:A26, A25:A24, A29:A28    ; 3 |
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B27:B26, B25:B24, B29:B28    ; 3 |
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 35
    MV        .S1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B13, B12                     ; 1 |
; 36
loopVert:
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A13:A12, A19:A18    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B13:B12, B19:B18    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 37
    MV        .S1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B9, B8                       ; 1 |
; 38
    ADD       .L1     A7, A5, A7                   ; 1 | update pointer to input A
||  DADDSP    .S1     A23:A22, A21:A20, A31:A30    ; 3 |
||  DMPYSP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  STDW      .D1     A31:A30, *A10                ; 1 | store two 4-B outputs
||  ADD       .L2     B7, B5, B7                   ; 1 | update pointer to input A
||  DADDSP    .S2     B23:B22, B21:B20, B31:B30    ; 3 |
||  DMPYSP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  STDW      .D2     B31:B30, *B10                ; 1 | store two 4-B outputs
; 39
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  MV        .S1     A13, A12                     ; 1 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  MV        .S2     B13, B12                     ; 1 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 40
    SUB       .L1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  DADDSP    .S1     A29:A28, A19:A18, A29:A28    ; 3 |
||  DMPYSP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B29:B28, B19:B18, B29:B28    ; 3 |
||  DMPYSP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 41
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  MV        .S1     A9, A8                       ; 1 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  MV        .S2     B9, B8                       ; 1 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 42
    DADDSP    .S1     A31:A30, A17:A16, A31:A30    ; 3 |
||  DMPYSP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B31:B30, B17:B16, B31:B30    ; 3 |
||  DMPYSP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
; 43
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 44
    MV        .L1     A13, A12                     ; 1 |
||  DADDSP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
||  DADDSP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 45
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 46
    MV        .L1     A9, A8                       ; 1 |
||  DADDSP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
||  DADDSP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
; 47
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  [A0]B     .S1     loopVert                     ; 6 | if number of vertical blocks <> 0 go back
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 48
    MV        .L1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 49
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A29:A28, A31:A30    ; 3 |
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B29:B28, B31:B30    ; 3 |
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 50
    ADD       .L1     A10, A11, A10                ; 1 | update pointer to output Y
||  MV        .S1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A17:A16                 ; 5 | load two 4-B inputs from B
||  ADD       .L2     B10, B11, B10                ; 1 | update pointer to output Y
||  MV        .S2     B9, B8                       ; 1 |
; 51
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A27:A26, A25:A24, A29:A28    ; 3 |
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B27:B26, B25:B24, B29:B28    ; 3 |
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 52
    MV        .S1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B13, B12                     ; 1 |
; 53
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A13:A12, A19:A18    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B13:B12, B19:B18    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 54
    MV        .L1     A4, A7                       ; 1 | pointer to input A
||  MV        .S1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  ADD       .L2     A4, B5, B7                   ; 1 | pointer to input A
||  MV        .S2     B9, B8                       ; 1 |
; 55
    DADDSP    .S1     A23:A22, A21:A20, A31:A30    ; 3 |
||  DMPYSP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  STDW      .D1     A31:A30, *A10                ; 1 | store two 4-B outputs
||  ADD       .L2     B4, 8, B4                    ; 1 | update pointer to input B
||  DADDSP    .S2     B23:B22, B21:B20, B31:B30    ; 3 |
||  DMPYSP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
||  STDW      .D2     B31:B30, *B10                ; 1 | store two 4-B outputs
; 56
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  MV        .S1     A13, A12                     ; 1 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  MV        .S2     B13, B12                     ; 1 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 57
    DADDSP    .S1     A29:A28, A19:A18, A29:A28    ; 3 |
||  DMPYSP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  DADDSP    .S2     B29:B28, B19:B18, B29:B28    ; 3 |
||  DMPYSP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 58
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  MV        .S1     A9, A8                       ; 1 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  MV        .S2     B9, B8                       ; 1 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 59
    ADD       .L1     A10, A11, A10                ; 1 | update pointer to output Y
||  DADDSP    .S1     A31:A30, A17:A16, A31:A30    ; 3 |
||  DMPYSP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  ADD       .L2     B10, B11, B10                ; 1 | update pointer to output Y
||  DADDSP    .S2     B31:B30, B17:B16, B31:B30    ; 3 |
||  DMPYSP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
; 60
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  SHR       .S1     B6, 1, A0                    ; 1 | get number of vertical blocks (per two)
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 61
    MV        .L1     A13, A12                     ; 1 |
||  DADDSP    .S1     A29:A28, A21:A20, A29:A28    ; 3 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
||  DADDSP    .S2     B29:B28, B21:B20, B29:B28    ; 3 |
; 62
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  SUB       .S1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 63
    MV        .L1     A9, A8                       ; 1 |
||  DADDSP    .S1     A31:A30, A19:A18, A31:A30    ; 3 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
||  DADDSP    .S2     B31:B30, B19:B18, B31:B30    ; 3 |
; 64
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 65
    MV        .L1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 66
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A29:A28, A31:A30    ; 3 |
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B29:B28, B31:B30    ; 3 |
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 67
    MV        .L1     A9, A8                       ; 1 |
||  [A1]B     .S1     loopHoriz                    ; 6 | if number of horizontal blocks <> 0 go back
||  LDDW      .D1     *A3, A17:A16                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
||  [!B1]B    .S2     B3                           ; 6 | if number of horizontal blocks == 0 return
; 68
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A27:A26, A25:A24, A29:A28    ; 3 |
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B27:B26, B25:B24, B29:B28    ; 3 |
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 69
    ADD       .L1     A6, 8, A6                    ; 1 | update pointer to output Y
||  MV        .S1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .S2     B13, B12                     ; 1 |
; 70
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A19:A18, A13:A12, A19:A18    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A19:A18, B13:B12, B19:B18    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 71
    ADD       .L1     A7, A5, A7                   ; 1 | update pointer to input A
||  MV        .S1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A19:A18                 ; 5 | load two 4-B inputs from B
||  ADD       .L2     B7, B5, B7                   ; 1 | update pointer to input A
||  MV        .S2     B9, B8                       ; 1 |
; 72
    STDW      .D1     A31:A30, *A10                ; 1 | store two 4-B outputs
||  STDW      .D2     B31:B30, *B10                ; 1 | store two 4-B outputs
