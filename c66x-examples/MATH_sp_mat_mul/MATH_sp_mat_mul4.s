;;
;  MATH_sp_mat_mul4.s
;
    .global MATH_sp_mat_mul4
MATH_sp_mat_mul4:
; 1
    MVKL      .S2     0x00000004, B6               ; 1 | N=4
; 2
    MV        .L1     B4, A3                       ; 1 | pointer to input matrix B
||  MVKH      .S2     0x00000004, B6               ; 1 | N=4
; 3
    SHL       .S1     B6, 2, A5                    ; 1 | 1*N*4
||  SHL       .S2     B6, 2, B5                    ; 1 | 1*N*4
; 4
    MV        .L1     A4, A7                       ; 1 | pointer to input matrix A
||  SHL       .S1     B6, 3, A11                   ; 1 | 2*N*4
||  ADD       .L2     A4, B5, B7                   ; 1 | update pointer to input matrix A
||  SHL       .S2     B6, 3, B11                   ; 1 | 2*N*4
; 5
    MV        .L1     A6, A10                      ; 1 | pointer to output matrix Y
||  ADD       .L2     A6, B5, B10                  ; 1 | update pointer to output matrix Y
; 6
    ADD       .L1     A4, A11, A4                  ; 1 | update pointer to input matrix A
||  LDW       .D1     *A7, A9                      ; 5 | load 4-B input from matrix A
||  LDW       .D2     *B7, B9                      ; 5 | load 4-B input from matrix A
; 7
    LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from matrix B
; 8
    LDDW      .D1     *++A3[1], A25:A24            ; 5 | load two 4-B inputs from matrix B
; 9
    LDW       .D1     *++A7[1], A13                ; 5 | load 4-B input from matrix A
||  LDW       .D2     *++B7[1], B13                ; 5 | load 4-B input from matrix A
; 10
    LDDW      .D1     *++A3[1], A23:A22            ; 5 | load two 4-B inputs from matrix B
; 11
    MV        .S1     A9, A8                       ; 1 | copy value from matrix A
||  LDDW      .D1     *++A3[1], A21:A20            ; 5 | load two 4-B inputs from matrix B
||  MV        .S2     B9, B8                       ; 1 | copy value from matrix A
; 12
    DMPYSP    .M1     A27:A26, A9:A8, A27:A26      ; 4 |
||  LDW       .D1     *++A7[1], A9                 ; 5 | load 4-B input from matrix A
||  DMPYSP    .M2     A27:A26, B9:B8, B27:B26      ; 4 |
||  LDW       .D2     *++B7[1], B9                 ; 5 | load 4-B input from matrix A
; 13
    DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDDW      .D1     *++A3[1], A19:A18            ; 5 | load two 4-B inputs from matrix B
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
; 14
    MV        .S1     A13, A12                     ; 1 | copy value from matrix A
||  LDDW      .D1     *++A3[1], A17:A16            ; 5 | load two 4-B inputs from matrix B
||  MV        .S2     B13, B12                     ; 1 | copy value from matrix A
; 15
    DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *++A7[1], A13                ; 5 | load 4-B input from matrix A
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *++B7[1], B13                ; 5 | load 4-B input from matrix A
; 16
    DMPYSP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *++A3[1], A23:A22            ; 5 | load two 4-B inputs from matrix B
||  DMPYSP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 17
    MV        .L1     A4, A7                       ; 1 | pointer to input matrix A
||  MV        .S1     A9, A8                       ; 1 | copy value from matrix A
||  LDDW      .D1     *++A3[1], A21:A20            ; 5 | load two 4-B inputs from matrix B
||  ADD       .L2     A4, B5, B7                   ; 1 | update pointer to input matrix A
||  MV        .S2     B9, B8                       ; 1 | copy value from matrix A
; 18
    MV        .L1     B4, A3                       ; 1 | pointer to input matrix B
||  DMPYSP    .M1     A19:A18, A9:A8, A19:A18      ; 4 |
||  LDW       .D1     *A7, A9                      ; 5 | load 4-B input from matrix A
||  DMPYSP    .M2     A19:A18, B9:B8, B19:B18      ; 4 |
||  LDW       .D2     *B7, B9                      ; 5 | load 4-B input from matrix A
; 19
    DADDSP    .L1     A27:A26, A23:A22, A27:A26    ; 3 |
||  DMPYSP    .M1     A17:A16, A9:A8, A17:A16      ; 4 |
||  LDDW      .D1     *A3, A31:A30                 ; 5 | load two 4-B inputs from matrix B
||  DADDSP    .L2     B27:B26, B23:B22, B27:B26    ; 3 |
||  DMPYSP    .M2     A17:A16, B9:B8, B17:B16      ; 4 |
; 20
    DADDSP    .L1     A25:A24, A21:A20, A25:A24    ; 3 |
||  MV        .S1     A13, A12                     ; 1 | copy value from matrix A
||  LDDW      .D1     *++A3[1], A29:A28            ; 5 | load two 4-B inputs from matrix B
||  DADDSP    .L2     B25:B24, B21:B20, B25:B24    ; 3 |
||  MV        .S2     B13, B12                     ; 1 | copy value from matrix A
; 21
    DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *++A7[1], A13
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *++B7[1], B13
; 22
    DADDSP    .L1     A27:A26, A19:A18, A27:A26    ; 3 |
||  DMPYSP    .M1     A21:A20, A13:A12, A21:A20    ; 4 |
||  LDDW      .D1     *++A3[1], A19:A18            ; 5 | load two 4-B inputs from matrix B
||  DADDSP    .L2     B27:B26, B19:B18, B27:B26    ; 3 |
||  DMPYSP    .M2     A21:A20, B13:B12, B21:B20    ; 4 |
; 23
    DADDSP    .L1     A25:A24, A17:A16, A25:A24    ; 3 |
||  MV        .S1     A9, A8                       ; 1 | copy value from matrix A
||  LDDW      .D1     *++A3[1], A17:A16            ; 5 | load two 4-B inputs from matrix B
||  DADDSP    .L2     B25:B24, B17:B16, B25:B24    ; 3 |
||  MV        .S2     B9, B8                       ; 1 | copy value from matrix A
; 24
    DMPYSP    .M1     A31:A30, A9:A8, A31:A30      ; 4 |
||  LDW       .D1     *++A7[1], A9                 ; 5 | load 4-B input from matrix A
||  DMPYSP    .M2     A31:A30, B9:B8, B31:B30      ; 4 |
||  LDW       .D2     *++B7[1], B9                 ; 5 | load 4-B input from matrix A
; 25
    DADDSP    .L1     A27:A26, A23:A22, A27:A26    ; 3 |
||  DMPYSP    .M1     A29:A28, A9:A8, A29:A28      ; 4 |
||  LDDW      .D1     *++A3[1], A23:A22            ; 5 | load two 4-B inputs from matrix B
||  DADDSP    .L2     B27:B26, B23:B22, B27:B26    ; 3 |
||  DMPYSP    .M2     A29:A28, B9:B8, B29:B28      ; 4 |
; 26
    DADDSP    .L1     A25:A24, A21:A20, A25:A24    ; 3 |
||  MV        .S1     A13, A12                     ; 1 | copy value from matrix A
||  LDDW      .D1     *++A3[1], A21:A20            ; 5 | load two 4-B inputs from matrix B
||  DADDSP    .L2     B25:B24, B21:B20, B25:B24    ; 3 |
||  MV        .S2     B13, B12                     ; 1 | copy value from matrix A
; 27
    DMPYSP    .M1     A19:A18, A13:A12, A19:A18    ; 4 |
||  LDW       .D1     *++A7[1], A13
||  DMPYSP    .M2     A19:A18, B13:B12, B19:B18    ; 4 |
||  LDW       .D2     *++B7[1], B13
; 28
    DMPYSP    .M1     A17:A16, A13:A12, A17:A16    ; 4 |
||  LDDW      .D1     *++A3[1], A19:A18            ; 5 | load two 4-B inputs from matrix B
||  DMPYSP    .M2     A17:A16, B13:B12, B17:B16    ; 4 |
; 29
    MV        .S1     A9, A8                       ; 1 | copy value from matrix A
||  LDDW      .D1     *++A3[1], A17:A16            ; 5 | load two 4-B inputs from matrix B
||  MV        .S2     B9, B8                       ; 1 | copy value from matrix A
; 30
    DMPYSP    .M1     A23:A22, A9:A8, A23:A22      ; 4 |
||  STDW      .D1     A27:A26, *A10                ; 1 | store two 4-B outputs
||  DMPYSP    .M2     A23:A22, B9:B8, B23:B22      ; 4 |
||  STDW      .D2     B27:B26, *B10                ; 1 | store two 4-B outputs
; 31
    DADDSP    .L1     A31:A30, A19:A18, A31:A30    ; 3 |
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  STDW      .D1     A25:A24, *++A10[1]           ; 1 | store two 4-B outputs
||  DADDSP    .L2     B31:B30, B19:B18, B31:B30    ; 3 |
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  STDW      .D2     B25:B24, *++B10[1]           ; 1 | store two 4-B outputs
; 32
    DADDSP    .L1     A29:A28, A17:A16, A29:A28    ; 3 |
||  MV        .S1     A13, A12                     ; 1 | copy value from matrix A
||  DADDSP    .L2     B29:B28, B17:B16, B29:B28    ; 3 |
||  MV        .S2     B13, B12                     ; 1 | copy value from matrix A
; 33
    DMPYSP    .M1     A19:A18, A13:A12, A19:A18    ; 4 |
||  DMPYSP    .M2     A19:A18, B13:B12, B19:B18    ; 4 |
; 34
    DADDSP    .L1     A31:A30, A23:A22, A31:A30    ; 3 |
||  ADD       .S1     A6, A11, A6                  ; 1 | update pointer to output matrix Y
||  DMPYSP    .M1     A17:A16, A13:A12, A17:A16    ; 4 |
||  DADDSP    .L2     B31:B30, B23:B22, B31:B30    ; 3 |
||  DMPYSP    .M2     A17:A16, B13:B12, B17:B16    ; 4 |
; 35
    DADDSP    .L1     A29:A28, A21:A20, A29:A28    ; 3 |
||  MV        .S1     A6, A10                      ; 1 | pointer to output matrix Y
||  DADDSP    .L2     B29:B28, B21:B20, B29:B28    ; 3 |
||  ADD       .S2     A6, B5, B10                  ; 1 | update pointer to output matrix Y
; 36
    B         .S2     B3                           ; 6 | if number of column blocks == 0 return from function
; 37
    DADDSP    .L1     A31:A30, A19:A18, A31:A30    ; 3 |
||  DADDSP    .L2     B31:B30, B19:B18, B31:B30    ; 3 |
; 38
    DADDSP    .L1     A29:A28, A17:A16, A29:A28    ; 3 |
||  DADDSP    .L2     B29:B28, B17:B16, B29:B28    ; 3 |
; 39
    NOP
; 40
    STDW      .D1     A31:A30, *A10                ; 1 | store two 4-B outputs
||  STDW      .D2     B31:B30, *B10                ; 1 | store two 4-B outputs
; 41
    STDW      .D1     A29:A28, *++A10[1]           ; 1 | store two 4-B outputs
||  STDW      .D2     B29:B28, *++B10[1]           ; 1 | store two 4-B outputs
