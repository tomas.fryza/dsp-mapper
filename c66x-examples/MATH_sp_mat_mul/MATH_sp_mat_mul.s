;;
;  MATH_sp_mat_mul.s
;
    .global MATH_sp_mat_mul
MATH_sp_mat_mul:
; 1
    SHL       .S1     B6, 2, A5                    ; 1 | 1*N*4
||  SHL       .S2     B6, 2, B5                    ; 1 | 1*N*4
; 2
    MV        .L1     A4, A7                       ; 1 | pointer to input A
||  SHL       .S1     B6, 3, A11                   ; 1 | 2*N*4
||  ADD       .L2     A4, B5, B7                   ; 1 | pointer to input A
||  SHL       .S2     B6, 3, B11                   ; 1 | 2*N*4
; 3
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 4
    MV        .L1     A6, A10                      ; 1 | pointer to output Y
||  SHR       .S1     B6, 1, A1                    ; 1 | get number of horizontal blocks (per two)
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  ADD       .L2     A6, B5, B10                  ; 1 | pointer to output Y
||  SHR       .S2     B6, 1, B1                    ; 1 | get number of horizontal blocks (per two)
; 5
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  SHR       .S1     B6, 1, A0                    ; 1 | get number of vertical blocks (per two)
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  SHR       .S2     B6, 1, B0                    ; 1 | get number of vertical blocks (per two)
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 6
    SUB       .L1     A10, A11, A10                ; 1 | pointer to output Y
||  SHR       .S1     B6, 3, A2                    ; 1 | get number of horizontal blocks (per eight)
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  SUB       .L2     B10, B11, B10                ; 1 | pointer to output Y
||  SHR       .S2     B6, 3, B2                    ; 1 | get number of horizontal blocks (per eight)
; 7
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  SUB       .S1     A2, 1, A2                    ; 1 | decrement number of horizontal blocks
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  SUB       .S2     B2, 1, B2                    ; 1 | decrement number of horizontal blocks
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 8
    MV        .L1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 9
loopHoriz:
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A27:A26, A13:A12, A29:A28    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A27:A26, B13:B12, B29:B28    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 10
    MV        .L1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
; 11
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A25:A24, A9:A8, A31:A30      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A25:A24, B9:B8, B31:B30      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 12
    MV        .L1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 13
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 14
    MV        .L1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
; 15
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 16
    MV        .L1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 17
loopVert:
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A29:A28, A23:A22, A29:A28    ; 3 |
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B29:B28, B23:B22, B29:B28    ; 3 |
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 18
    MV        .L1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
; 19
nextBlock:
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A21:A20, A31:A30    ; 3 |
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B21:B20, B31:B30    ; 3 |
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 20
    MV        .L1     A13, A12                     ; 1 |
||  SUB       .S1     A2, 1, A2                    ; 1 | decrement number of horizontal blocks
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
||  SUB       .S2     B2, 1, B2                    ; 1 | decrement number of horizontal blocks
; 21
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A29:A28, A27:A26, A29:A28    ; 3 |
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B29:B28, B27:B26, B29:B28    ; 3 |
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 22
    MV        .L1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
; 23
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A25:A24, A31:A30    ; 3 |
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B25:B24, B31:B30    ; 3 |
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 24
    MV        .L1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 25
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A29:A28, A23:A22, A29:A28    ; 3 |
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B29:B28, B23:B22, B29:B28    ; 3 |
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 26
    MV        .L1     A9, A8                       ; 1 |
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
; 27
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A21:A20, A31:A30    ; 3 |
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B21:B20, B31:B30    ; 3 |
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 28
    MV        .L1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 29
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  [A2]B     .S1     nextBlock                    ; 6 | if number of horizontal blocks <> 0 go back
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 30
    MV        .L1     A9, A8                       ; 1 |
||  DADDSP    .S1     A29:A28, A27:A26, A29:A28    ; 3 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
||  DADDSP    .S2     B29:B28, B27:B26, B29:B28    ; 3 |
; 31
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A25:A24, A31:A30    ; 3 |
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B25:B24, B31:B30    ; 3 |
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 32
    MV        .L1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 33
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A29:A28, A23:A22, A29:A28    ; 3 |
||  DMPYSP    .M1     A27:A26, A13:A12, A27:A26    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B29:B28, B23:B22, B29:B28    ; 3 |
||  DMPYSP    .M2     A27:A26, B13:B12, B27:B26    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 34
    MV        .L1     A9, A8                       ; 1 |
||  [!A2]ADD  .S1     A7, A5, A7                   ; 1 | pointer to input A
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
||  [!B2]ADD  .S2     B7, B5, B7                   ; 1 | pointer to input A
; 35
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  DADDSP    .S1     A31:A30, A21:A20, A31:A30    ; 3 |
||  DMPYSP    .M1     A25:A24, A9:A8, A25:A24      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B21:B20, B31:B30    ; 3 |
||  DMPYSP    .M2     A25:A24, B9:B8, B25:B24      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 36
    MV        .L1     A13, A12                     ; 1 |
||  SHR       .S1     B6, 3, A2                    ; 1 | get number of horizontal blocks (per eight)
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
||  SHR       .S2     B6, 3, B2                    ; 1 | get number of horizontal blocks (per eight)
; 37
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A29:A28, A27:A26, A29:A28    ; 3 |
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B29:B28, B27:B26, B29:B28    ; 3 |
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 38
    MV        .L1     A9, A8                       ; 1 |
||  SUB       .S1     A2, 1, A2                    ; 1 | decrement number of horizontal blocks
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
||  SUB       .S2     B2, 1, B2                    ; 1 | decrement number of horizontal blocks
; 39
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A25:A24, A31:A30    ; 3 |
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B25:B24, B31:B30    ; 3 |
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 40
    MV        .L1     A13, A12                     ; 1 |
||  ADD       .S1     A10, A11, A10                ; 1 | update pointer to output Y
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
||  ADD       .S2     B10, B11, B10                ; 1 | pointer to output Y
; 41
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A29:A28, A23:A22, A17:A16    ; 3 |
||  DMPYSP    .M1     A27:A26, A13:A12, A29:A28    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DADDSP    .S2     B29:B28, B23:B22, B17:B16    ; 3 |
||  DMPYSP    .M2     A27:A26, B13:B12, B29:B28    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 42
    MV        .L1     A9, A8                       ; 1 |
||  SUB       .S1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  LDDW      .D1     *A3, A21:A20                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
||  SUB       .S2     B0, 1, B0                    ; 1 | decrement number of vertical blocks
; 43
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DADDSP    .S1     A31:A30, A21:A20, A19:A18    ; 3 |
||  DMPYSP    .M1     A25:A24, A9:A8, A31:A30      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  DADDSP    .S2     B31:B30, B21:B20, B19:B18    ; 3 |
||  DMPYSP    .M2     A25:A24, B9:B8, B31:B30      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 44
    MV        .L1     A13, A12                     ; 1 |
||  [A0]B     .S1     loopVert                     ; 6 |  if number of vertical blocks <> 0 go back
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 45
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A23:A22, A13:A12, A23:A22    ; 4 |
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  DMPYSP    .M2     A23:A22, B13:B12, B23:B22    ; 4 |
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 46
    MV        .L1     A9, A8                       ; 1 |
||  DADDSP    .S1     A17:A16, A19:A18, A17:A16    ; 3 |
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B9, B8                       ; 1 |
||  DADDSP    .S2     B17:B16, B19:B18, B17:B16    ; 3 |
; 47
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  DMPYSP    .M1     A21:A20, A9:A8, A21:A20      ; 4 |
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  [!B0]ADD  .S2     B4, 8, B4                    ; 1 | update original pointer to input B
||  DMPYSP    .M2     A21:A20, B9:B8, B21:B20      ; 4 |
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 48
    MV        .L1     A13, A12                     ; 1 |
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
; 49
    [!A0]MV   .L1     A4, A7                       ; 1 | pointer to input A
||  [!A0]SUB  .S1     A1, 1, A1                    ; 1 | decrement number of horizontal blocks
||  STDW      .D1     A17:A16, *A10                ; 1 | store two 4-B outputs
||  [!A0]ADD  .L2     A4, B5, B7                   ; 1 | pointer to input A
||  [!B0]SUB  .S2     B1, 1, B1                    ; 1 | decrement number of horizontal blocks
||  STDW      .D2     B17:B16, *B10                ; 1 | store two 4-B outputs
; 50
    MV        .L1     B4, A3                       ; 1 | pointer to input B
||  [A1]B     .S1     loopHoriz                    ; 6 |  if number of horizontal blocks <> 0 go back
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  [!B1]B    .S2     B3                           ; 6 |  if number of vertical blocks = 0 return
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 51
    ADD       .S1     A6, 8, A6                    ; 1 | update original pointer to output Y
||  LDDW      .D1     *A3, A27:A26                 ; 5 | load two 4-B inputs from B
; 52
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  MV        .S1     A6, A10                      ; 1 | pointer to output Y
||  LDW       .D1     *A7++[1], A9                 ; 5 | load 4-B input from A
||  ADD       .S2     A6, B5, B10                  ; 1 | pointer to output Y
||  LDW       .D2     *B7++[1], B9                 ; 5 | load 4-B input from A
; 53
    SUB       .L1     A10, A11, A10                ; 1 | pointer to output Y
||  SHR       .S1     B6, 1, A0                    ; 1 | get number of vertical blocks (per two)
||  LDDW      .D1     *A3, A25:A24                 ; 5 | load two 4-B inputs from B
||  SUB       .L2     B10, B11, B10                ; 1 | pointer to output Y
||  SHR       .S2     B6, 1, B0                    ; 1 | get number of vertical blocks (per two)
; 54
    ADD       .L1     A3, A5, A3                   ; 1 | update pointer to input B
||  SHR       .S1     B6, 3, A2                    ; 1 | get number of horizontal blocks (per eight)
||  LDW       .D1     *A7++[1], A13                ; 5 | load 4-B input from A
||  SHR       .S2     B6, 3, B2                    ; 1 | get number of horizontal blocks (per eight)
||  LDW       .D2     *B7++[1], B13                ; 5 | load 4-B input from A
; 55
    MV        .L1     A13, A12                     ; 1 |
||  SUB       .S1     A2, 1, A2                    ; 1 | decrement number of horizontal blocks
||  LDDW      .D1     *A3, A23:A22                 ; 5 | load two 4-B inputs from B
||  MV        .L2     B13, B12                     ; 1 |
||  SUB       .S2     B2, 1, B2                    ; 1 | decrement number of horizontal blocks
