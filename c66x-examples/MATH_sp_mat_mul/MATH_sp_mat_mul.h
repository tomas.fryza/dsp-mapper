/**
 ***********************************************************************
 * @file MATH_sp_mat_mul.h
 *
 * @brief Multiplication of single-precision float matrices.
 * @author Tomas Fryza, Brno University of Technology, Czech Republic
 */


#ifndef MATH_SP_MAT_MUL_H_INCLUDED
#define MATH_SP_MAT_MUL_H_INCLUDED


/**
 ***********************************************************************
 * @ingroup MATH
 * @{
 * @defgroup MATH_sp_mat_mul MATH_sp_mat_mul
 * @brief Multiplication of single-precision float matrices.
 * @{
 */


/**
 ***********************************************************************
 * @brief Multiplication of single-precision square matrices of order 4.
 *
 * @param a - Pointer to input matrix A
 * @param b - Pointer to input matrix B
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 4.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 41<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_sp_mat_mul4(float *a, float *b, float *y);


/**
 ***********************************************************************
 * @brief Multiplication of single-precision square matrices of order 8.
 *
 * @param a - Pointer to input matrix A
 * @param b - Pointer to input matrix B
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 8.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 293<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_sp_mat_mul8(float *a, float *b, float *y);


/**
 ***********************************************************************
 * @brief Multiplication of single-precision square matrices of order 8.
 *
 * @param a - Pointer to input matrix A
 * @param b - Pointer to input matrix B
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 8.<br>
 *
 * @par Implementation notes:
 *    No parallel distribution within individual SoPs is applied.
 *    CPU cycles: 401<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_sp_mat_mul8_serial(float *a, float *b, float *y);


/**
 ***********************************************************************
 * @brief Multiplication of single-precision square matrices of order N.
 *
 * @param a - Pointer to input matrix A
 * @param b - Pointer to input matrix B
 * @param y - Pointer to output matrix
 * @param N - Order of square matrix
 *
 * @par Assumptions:
 *    Number of rows and columns must be a multiple of 8 and >= 16.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 1/2*N*N*N + 1/4*N*N + 7*N + 8<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_sp_mat_mul(float *a, float *b, float *y, int N);


/** @} defgroup MATH_sp_mat_mul */
#endif /* MATH_SP_MAT_MUL_H_INCLUDED */
