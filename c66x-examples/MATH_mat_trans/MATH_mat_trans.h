/**
 ***********************************************************************
 * @file MATH_mat_trans.h
 *
 * @brief Functions for transposition of fixed-point matrix.
 * @author Tomas Fryza, Brno University of Technology, Czech Republic
 */


#ifndef MATH_MAT_TRANS_H_INCLUDED
#define MATH_MAT_TRANS_H_INCLUDED


/**
 ***********************************************************************
 * @ingroup MATH
 * @{
 * @defgroup MATH_mat_trans MATH_mat_trans
 * @brief Transposition of fixed-point matrix.
 * @{
 */


/**
 ***********************************************************************
 * @brief Transposition of fixed-point square matrix of order 4.
 *
 * @param x - Pointer to input matrix
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 4.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 15<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_mat_trans4(int *x, int *y);


/**
 ***********************************************************************
 * @brief Transposition of fixed-point square matrix of order 8.
 *
 * @param x - Pointer to input matrix
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 8.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 38<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_mat_trans8(int *x, int *y);


/**
 ***********************************************************************
 * @brief Transposition of fixed-point square matrix of order N.
 *
 * @param x - Pointer to input matrix
 * @param y - Pointer to output matrix
 * @param N - Order of square matrix
 *
 * @par Assumptions:
 *    Number of rows and columns must be a multiple of 8 and >= 8.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 10 + 8*N + 4N*(N/8-2)<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_mat_trans(int *x, int *y, int N);


/** @} defgroup MATH_mat_trans */
#endif /* MATH_MAT_TRANS_H_INCLUDED */
