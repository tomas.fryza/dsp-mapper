;;
;  MATH_mat_trans.s
;
    .global MATH_mat_trans
MATH_mat_trans:
; 1
    SHL       .S1     A6, 3, A2                    ; 1 | 2*N*4
||  MV        .S2     A6, B6                       ; 1 | copy matrix order
; 2
    MV        .L1     A4, A7                       ; 1 | first pointer to input matrix
||  SHL       .S1     A6, 2, A5                    ; 1 | 1*N*4
||  ADD       .L2     A4, 8, B7                    ; 1 | first pointer to input matrix
||  SHL       .S2     B6, 3, B2                    ; 1 | 2*N*4
; 3
    MV        .L1     B4, A9                       ; 1 | first pointer to output matrix
||  SHR       .S1     A6, 2, A1                    ; 1 | get number of horizontal blocks (per four)
||  ADD       .L2     B4, B2, B9                   ; 1 | first pointer to output matrix
||  SHL       .S2     B6, 2, B5                    ; 1 | 1*N*4
; 4
    ADD       .L1     A9, A5, A10                  ; 1 | second pointer to output matrix
||  SHR       .S1     A6, 3, A0                    ; 1 | get number of vertical blocks (per eight)
||  ADD       .L2     B9, B5, B10                  ; 1 | second pointer to output matrix
||  SHR       .S2     B6, 2, B1                    ; 1 | get number of horizontal blocks (per four)
; 5
loopVert:
    ADD       .L1     A7, A5, A8                   ; 1 | second pointer to input matrix
||  SUB       .S1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  LDDW      .D1     *A7, A31:A30                 ; 5 | load two 4-B inputs
||  ADD       .L2     B7, B5, B8                   ; 1 | second pointer to input matrix
||  LDDW      .D2     *B7, B31:B30                 ; 5 | load two 4-B inputs
; 6
    ADD       .L1     A7, A2, A7                   ; 1 | update first input pointer
||  LDDW      .D1     *A8, A29:A28                 ; 5 | load two 4-B inputs
||  ADD       .L2     B7, B2, B7                   ; 1 | update first input pointer
||  LDDW      .D2     *B8, B29:B28                 ; 5 | load two 4-B inputs
; 7
    ADD       .L1     A8, A2, A8                   ; 1 | update second input pointer
||  LDDW      .D1     *A7, A27:A26                 ; 5 | load two 4-B inputs
||  ADD       .L2     B8, B2, B8                   ; 1 | update second input pointer
||  LDDW      .D2     *B7, B27:B26                 ; 5 | load two 4-B inputs
; 8
    ADD       .L1     A7, A2, A7                   ; 1 | update first input pointer
||  LDDW      .D1     *A8, A25:A24                 ; 5 | load two 4-B inputs
||  ADD       .L2     B7, B2, B7                   ; 1 | update first input pointer
||  LDDW      .D2     *B8, B25:B24                 ; 5 | load two 4-B inputs
; 9
    ADD       .S1     A8, A2, A8                   ; 1 | update second input pointer
||  LDDW      .D1     *A7, A23:A22                 ; 5 | load two 4-B inputs
||  ADD       .S2     B8, B2, B8                   ; 1 | update second input pointer
||  LDDW      .D2     *B7, B23:B22                 ; 5 | load two 4-B inputs
; 10
    MV        .L1     A31, A12                     ; 1 | temp
||  ADD       .S1     A7, A2, A7                   ; 1 | update first input pointer
||  LDDW      .D1     *A8, A21:A20                 ; 5 | load two 4-B inputs
||  MV        .L2     B31, B12                     ; 1 | temp
||  ADD       .S2     B7, B2, B7                   ; 1 | update first input pointer
||  LDDW      .D2     *B8, B21:B20                 ; 5 | load two 4-B inputs
; 11
loopHoriz:
    MV        .L1     A28, A31                     ; 1 | swap
||  ADD       .S1     A8, A2, A8                   ; 1 | update second input pointer
||  LDDW      .D1     *A7, A19:A18                 ; 5 | load two 4-B inputs
||  MV        .L2     B28, B31                     ; 1 | swap
||  ADD       .S2     B8, B2, B8                   ; 1 | update second input pointer
||  LDDW      .D2     *B7, B19:B18                 ; 5 | load two 4-B inputs
; 12
    MV        .L1     A12, A28                     ; 1 | swap
||  ADD       .S1     A7, A2, A7                   ; 1 | update first input pointer
||  LDDW      .D1     *A8, A17:A16                 ; 5 | load two 4-B inputs
||  MV        .L2     B12, B28                     ; 1 | swap
||  ADD       .S2     B7, B2, B7                   ; 1 | update first input pointer
||  LDDW      .D2     *B8, B17:B16                 ; 5 | load two 4-B inputs
; 13
    MV        .S1     A27, A13                     ; 1 | temp
||  STDW      .D1     A31:A30, *A9++[1]            ; 1 | store two 4-B outputs
||  MV        .L2     A0, B0                       ; 1 | copy number of vertical blocks
||  MV        .S2     B27, B13                     ; 1 | temp
||  STDW      .D2     B31:B30, *B9++[1]            ; 1 | store two 4-B outputs
; 14
    MV        .L1     A23, A12                     ; 1 | temp
||  MV        .S1     A24, A27                     ; 1 | swap
||  STDW      .D1     A29:A28, *A10++[1]           ; 1 | store two 4-B outputs
||  MV        .L2     B23, B12                     ; 1 | temp
||  MV        .S2     B24, B27                     ; 1 | swap
||  STDW      .D2     B29:B28, *B10++[1]           ; 1 | store two 4-B outputs
; 15
    MV        .L1     A13, A24                     ; 1 | swap
||  [A0]B     .S1     loopVert                     ; 6 | if number of vertical blocks <> 0 go back
||  STDW      .D1     A27:A26, *A9++[1]            ; 1 | store two 4-B outputs
||  MV        .L2     B13, B24                     ; 1 | swap
||  STDW      .D2     B27:B26, *B9++[1]            ; 1 | store two 4-B outputs
; 16
    MV        .L1     A20, A23                     ; 1 | swap
||  MV        .S1     A19, A13                     ; 1 | temp
||  STDW      .D1     A25:A24, *A10++[1]           ; 1 | store two 4-B outputs
||  MV        .L2     B20, B23                     ; 1 | swap
||  MV        .S2     B19, B13                     ; 1 | temp
||  STDW      .D2     B25:B24, *B10++[1]           ; 1 | store two 4-B outputs
; 17
    MV        .L1     A12, A20                     ; 1 | swap
||  MV        .S1     A16, A19                     ; 1 | swap
||  STDW      .D1     A23:A22, *A9++[1]            ; 1 | store two 4-B outputs
||  MV        .L2     B12, B20                     ; 1 | swap
||  MV        .S2     B16, B19                     ; 1 | swap
||  STDW      .D2     B23:B22, *B9++[1]            ; 1 | store two 4-B outputs
; 18
    [!A0]ADD  .L1     A4, 8, A4                    ; 1 | update input pointer
||  MV        .S1     A13, A16                     ; 1 | swap
||  STDW      .D1     A21:A20, *A10++[1]           ; 1 | store two 4-B outputs
||  MV        .S2     B13, B16                     ; 1 | swap
||  STDW      .D2     B21:B20, *B10++[1]           ; 1 | store two 4-B outputs
; 19
    [!A0]ADD  .L1     A4, 8, A4                    ; 1 | update input pointer
||  STDW      .D1     A19:A18, *A9++[1]            ; 1 | store two 4-B outputs
||  STDW      .D2     B19:B18, *B9++[1]            ; 1 | store two 4-B outputs
; 20
    [!A0]MV   .L1     A4, A7                       ; 1 | first pointer to input matrix
||  [!A0]SUB  .S1     A1, 1, A1                    ; 1 | decrement number of horizontal blocks
||  STDW      .D1     A17:A16, *A10++[1]           ; 1 | store two 4-B outputs
||  [!B0]ADD  .L2     A4, 8, B7                    ; 1 | first pointer to input matrix
||  [!B0]SUB  .S2     B1, 1, B1                    ; 1 | decrement number of horizontal blocks
||  STDW      .D2     B17:B16, *B10++[1]           ; 1 | store two 4-B outputs
; 21
    ADD       .L1     A7, A5, A8                   ; 1 | second pointer to input matrix
||  [A1]B     .S1     loopHoriz                    ; 6 | if number of horizontal blocks <> 0 go back
||  LDDW      .D1     *A7, A31:A30                 ; 5 | load two 4-B inputs
||  ADD       .L2     B7, B5, B8                   ; 1 | second pointer to input matrix
||  [!B1]B    .S2     B3                           ; 6 | if number of horizontal blocks == 0 return from function
||  LDDW      .D2     *B7, B31:B30                 ; 5 | load two 4-B inputs
; 22
    ADD       .L1     A7, A2, A7                   ; 1 | update first input pointer
||  SHR       .S1     A6, 3, A0                    ; 1 | get number of vertical blocks (of eight)
||  LDDW      .D1     *A8, A29:A28                 ; 5 | load two 4-B inputs
||  ADD       .L2     B7, B2, B7                   ; 1 | update first input pointer
||  ADD       .S2     B4, B2, B4                   ; 1 | update output pointer
||  LDDW      .D2     *B8, B29:B28                 ; 5 | load two 4-B inputs
; 23
    ADD       .L1     A8, A2, A8                   ; 1 | update second input pointer
||  SUB       .S1     A0, 1, A0                    ; 1 | decrement number of vertical blocks
||  LDDW      .D1     *A7, A27:A26                 ; 5 | load two 4-B inputs
||  ADD       .L2     B8, B2, B8                   ; 1 | update second input pointer
||  ADD       .S2     B4, B2, B4                   ; 1 | update output pointer
||  LDDW      .D2     *B7, B27:B26                 ; 5 | load two 4-B inputs
; 24
    ADD       .L1     A7, A2, A7                   ; 1 | update first input pointer
||  MV        .S1     B4, A9                       ; 1 | first pointer to output matrix
||  LDDW      .D1     *A8, A25:A24                 ; 5 | load two 4-B inputs
||  ADD       .L2     B7, B2, B7                   ; 1 | update first input pointer
||  ADD       .S2     B4, B2, B9                   ; 1 | first pointer to output matrix
||  LDDW      .D2     *B8, B25:B24                 ; 5 | load two 4-B inputs
; 25
    ADD       .L1     A8, A2, A8                   ; 1 | update second input pointer
||  ADD       .S1     A9, A5, A10                  ; 1 | second pointer to output matrix
||  LDDW      .D1     *A7, A23:A22                 ; 5 | load two 4-B inputs
||  ADD       .L2     B8, B2, B8                   ; 1 | update second input pointer
||  ADD       .S2     B9, B5, B10                  ; 1 | second pointer to output matrix
||  LDDW      .D2     *B7, B23:B22                 ; 5 | load two 4-B inputs
; 26
    MV        .L1     A31, A12                     ; 1 | temp
||  ADD       .S1     A7, A2, A7                   ; 1 | update first input pointer
||  LDDW      .D1     *A8, A21:A20                 ; 5 | load two 4-B inputs
||  MV        .L2     B31, B12                     ; 1 | temp
||  ADD       .S2     B7, B2, B7                   ; 1 | update first input pointer
||  LDDW      .D2     *B8, B21:B20                 ; 5 | load two 4-B inputs
