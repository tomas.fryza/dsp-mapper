;;
;  MATH_mat_trans8.s
;
    .global MATH_mat_trans8
MATH_mat_trans8:
; 1
    MV        .L1     A4, A7                       ; 1 | pointer to input matrix
||  MVKL      .S1     0x00000008, A0               ; 1 | N=8
||  MV        .L2     A4, B7                       ; 1 | pointer to input matrix
||  MVKL      .S2     0x00000008, B0               ; 1 | N=8
; 2
    MV        .L1     A4, A8                       ; 1 | pointer to input matrix
||  MVKH      .S1     0x00000008, A0               ; 1 | N=8
||  ADD       .L2     B7, 8, B7                    ; 1 | update input pointer
||  MVKH      .S2     0x00000008, B0               ; 1 | N=8
; 3
    SHL       .S1     A0, 2, A5                    ; 1 | 1*N*4
||  MPY       .M1     A0, 2, A11                   ; 2 | 2N
||  MV        .L2     B7, B8                       ; 1 | pointer to input matrix
||  SHL       .S2     B0, 2, B5                    ; 1 | 1*N*4
||  MPY       .M2     B0, 2, B11                   ; 2 | 2N
; 4
    SHL       .S1     A0, 3, A6                    ; 1 | 2*N*4
||  SHL       .S2     B0, 3, B6                    ; 1 | 2*N*4
; 5
    SHR       .S1     A0, 2, A0                    ; 1 | get number of blocks of 4 columns
; 6
    ADD       .L1     A8, A5, A8                   ; 1 | update input pointer
||  SUB       .S1     A0, 1, A0                    ; 1 | decrement number of column blocks
||  LDDW      .D1     *A7, A31:A30                 ; 5 | load two 4-B inputs
||  ADD       .L2     B8, B5, B8                   ; 1 | update input pointer
||  LDDW      .D2     *B7, B31:B30                 ; 5 | load two 4-B inputs
; 7
LOOP:
    ADD       .L1     A7, A6, A7                   ; 1 | update input pointer
||  LDDW      .D1     *A8, A29:A28                 ; 5 | load two 4-B inputs
||  ADD       .L2     B7, B6, B7                   ; 1 | update input pointer
||  MV        .S2     A0, B0                       ; 1 | get number of column blocks
||  LDDW      .D2     *B8, B29:B28                 ; 5 | load two 4-B inputs
; 8
    ADD       .L1     A8, A6, A8                   ; 1 | update input pointer
||  MV        .S1     B4, A9                       ; 1 | pointer to output matrix
||  LDDW      .D1     *A7, A27:A26                 ; 5 | load two 4-B inputs
||  ADD       .L2     B8, B6, B8                   ; 1 | update input pointer
||  MV        .S2     B4, B9                       ; 1 | pointer to output matrix
||  LDDW      .D2     *B7, B27:B26                 ; 5 | load two 4-B inputs
; 9
    ADD       .L1     A7, A6, A7                   ; 1 | update input pointer
||  MV        .S1     B4, A10                      ; 1 | pointer to output matrix
||  LDDW      .D1     *A8, A25:A24                 ; 5 | load two 4-B inputs
||  ADD       .L2     B7, B6, B7                   ; 1 | update input pointer
||  MV        .S2     B4, B10                      ; 1 | pointer to output matrix
||  LDDW      .D2     *B8, B25:B24                 ; 5 | load two 4-B inputs
; 10
    ADD       .S1     A8, A6, A8                   ; 1 | update input pointer
||  LDDW      .D1     *A7, A23:A22                 ; 5 | load two 4-B inputs
||  ADD       .L2     B9, B6, B9                   ; 1 | update output pointer
||  ADD       .S2     B8, B6, B8                   ; 1 | update input pointer
||  LDDW      .D2     *B7, B23:B22                 ; 5 | load two 4-B inputs
; 11
    ADD       .L1     A10, A5, A10                 ; 1 | update output pointer
||  ADD       .S1     A7, A6, A7                   ; 1 | update input pointer
||  LDDW      .D1     *A8, A21:A20                 ; 5 | load two 4-B inputs
||  ADD       .L2     B9, B5, B10                  ; 1 | update output pointer
||  ADD       .S2     B7, B6, B7                   ; 1 | update input pointer
||  LDDW      .D2     *B8, B21:B20                 ; 5 | load two 4-B inputs
; 12
    MV        .L1     A31, A12                     ; 1 | temp
||  ADD       .S1     A8, A6, A8                   ; 1 | update input pointer
||  LDDW      .D1     *A7, A19:A18                 ; 5 | load two 4-B inputs
||  MV        .L2     B31, B12                     ; 1 | temp
||  ADD       .S2     B8, B6, B8                   ; 1 | update input pointer
||  LDDW      .D2     *B7, B19:B18                 ; 5 | load two 4-B inputs
; 13
    MV        .L1     A28, A31                     ; 1 | swap values
||  ADD       .S1     A7, A6, A7                   ; 1 | update input pointer
||  LDDW      .D1     *A8, A17:A16                 ; 5 | load two 4-B inputs
||  MV        .L2     B28, B31                     ; 1 | swap values
||  ADD       .S2     B7, B6, B7                   ; 1 | update input pointer
||  LDDW      .D2     *B8, B17:B16                 ; 5 | load two 4-B inputs
; 14
    MV        .L1     A12, A28                     ; 1 | swap values
||  MV        .S1     A27, A13                     ; 1 | temp
||  STDW      .D1     A31:A30, *A9                 ; 1 | store two 4-B outputs
||  MV        .L2     B12, B28                     ; 1 | swap values
||  MV        .S2     B27, B13                     ; 1 | temp
||  STDW      .D2     B31:B30, *B9                 ; 1 | store two 4-B outputs
; 15
    ADD       .L1     A4, A11, A4                  ; 1 | pointer to input matrix
||  MV        .S1     A24, A27                     ; 1 | swap values
||  STDW      .D1     A29:A28, *A10                ; 1 | store two 4-B outputs
||  ADD       .L2     B4, B6, B4                   ; 1 | update output pointer
||  MV        .S2     B24, B27                     ; 1 | swap values
||  STDW      .D2     B29:B28, *B10                ; 1 | store two 4-B outputs
; 16
    MV        .L1     A23, A12                     ; 1 | temp
||  MV        .S1     A13, A24                     ; 1 | swap values
||  STDW      .D1     A27:A26, *++A9[1]            ; 1 | store two 4-B outputs
||  MV        .L2     B23, B12                     ; 1 | temp
||  MV        .S2     B13, B24                     ; 1 | swap values
||  STDW      .D2     B27:B26, *++B9[1]            ; 1 | store two 4-B outputs
; 17
    MV        .L1     A20, A23                     ; 1 | swap values
||  [A0]B     .S1     LOOP                         ; 6 | if number of column blocks <> 0 go back to LOOP label
||  STDW      .D1     A25:A24, *++A10[1]           ; 1 | store two 4-B outputs
||  MV        .L2     B20, B23                     ; 1 | swap values
||  [!B0]B    .S2     B3                           ; 6 | if number of column blocks == 0 return from function
||  STDW      .D2     B25:B24, *++B10[1]           ; 1 | store two 4-B outputs
; 18
    MV        .L1     A12, A20                     ; 1 | swap values
||  MV        .S1     A19, A13                     ; 1 | temp
||  STDW      .D1     A23:A22, *++A9[1]            ; 1 | store two 4-B outputs
||  MV        .L2     B12, B20                     ; 1 | swap values
||  MV        .S2     B19, B13                     ; 1 | temp
||  STDW      .D2     B23:B22, *++B9[1]            ; 1 | store two 4-B outputs
; 19
    MV        .L1     A4, A7                       ; 1 | pointer to input matrix
||  MV        .S1     A16, A19                     ; 1 | swap values
||  STDW      .D1     A21:A20, *++A10[1]           ; 1 | store two 4-B outputs
||  ADD       .L2     B4, B6, B4                   ; 1 | update output pointer
||  MV        .S2     B16, B19                     ; 1 | swap values
||  STDW      .D2     B21:B20, *++B10[1]           ; 1 | store two 4-B outputs
; 20
    MV        .L1     A4, A8                       ; 1 | pointer to input matrix
||  MV        .S1     A13, A16                     ; 1 | swap values
||  STDW      .D1     A19:A18, *++A9[1]            ; 1 | store two 4-B outputs
||  MV        .L2     A4, B7                       ; 1 | pointer to input matrix
||  MV        .S2     B13, B16                     ; 1 | swap values
||  STDW      .D2     B19:B18, *++B9[1]            ; 1 | store two 4-B outputs
; 21
    STDW      .D1     A17:A16, *++A10[1]           ; 1 | store two 4-B outputs
||  ADD       .L2     B7, 8, B7                    ; 1 | update input pointer
||  ADD       .S2     B7, 8, B8                    ; 1 | pointer to input matrix
||  STDW      .D2     B17:B16, *++B10[1]           ; 1 | store two 4-B outputs
; 22
    ADD       .L1     A8, A5, A8                   ; 1 | update input pointer
||  SUB       .S1     A0, 1, A0                    ; 1 | decrement number of column blocks
||  LDDW      .D1     *A7, A31:A30                 ; 5 | load two 4-B inputs
||  ADD       .L2     B8, B5, B8                   ; 1 | update input pointer
||  LDDW      .D2     *B7, B31:B30                 ; 5 | load two 4-B inputs
