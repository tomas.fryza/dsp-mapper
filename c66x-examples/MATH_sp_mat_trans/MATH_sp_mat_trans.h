/**
 ***********************************************************************
 * @file MATH_sp_mat_trans.h
 *
 * @brief Functions for transposition of single-precision float matrix.
 * @author Tomas Fryza, Brno University of Technology, Czech Republic
 */


#ifndef MATH_SP_MAT_TRANS_H_INCLUDED
#define MATH_SP_MAT_TRANS_H_INCLUDED


/**
 ***********************************************************************
 * @ingroup MATH
 * @{
 * @defgroup MATH_sp_mat_trans MATH_sp_mat_trans
 * @brief Transposition of single-precision float matrix.
 * @{
 */


/**
 ***********************************************************************
 * @brief Transposition of single-precision square matrix of order 4.
 *
 * @param x - Pointer to input matrix
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 4.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 15<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_sp_mat_trans4(float *x, float *y);


/**
 ***********************************************************************
 * @brief Transposition of single-precision square matrix of order 8.
 *
 * @param x - Pointer to input matrix
 * @param y - Pointer to output matrix
 *
 * @par Assumptions:
 *    Number of rows and columns is equal to 8.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 38<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_sp_mat_trans8(float *x, float *y);


/**
 ***********************************************************************
 * @brief Transposition of single-precision square matrix of order N.
 *
 * @param x - Pointer to input matrix
 * @param y - Pointer to output matrix
 * @param N - Order of square matrix
 *
 * @par Assumptions:
 *    Number of rows and columns must be a multiple of 8 and >= 8.<br>
 *
 * @par Implementation notes:
 *    CPU cycles: 1/2*N*N + 10<br>
 *    Architecture: TMS320C66x<br>
 */
extern void MATH_sp_mat_trans(float *x, float *y, int N);


/** @} defgroup MATH_sp_mat_trans */
#endif /* MATH_SP_MAT_TRANS_H_INCLUDED */
