;;
;  MATH_sp_mat_trans4.s
;
    .global MATH_sp_mat_trans4
MATH_sp_mat_trans4:
; 1
    MV        .L1     A4, A1                       ; 1 | pointer to input matrix
||  MVKL      .S1     0x00000004, A6               ; 1 | N=4
||  MV        .D1     B4, A2                       ; 1 | pointer to output matrix
||  MV        .L2     A4, B1                       ; 1 | pointer to input matrix
||  MVKL      .S2     0x00000004, B6               ; 1 | N=4
||  MV        .D2     B3, B5                       ; 1 | move return address
; 2
    MV        .L1     A4, A0                       ; 1 | pointer to input matrix
||  MVKH      .S1     0x00000004, A6               ; 1 | N=4
||  MV        .D1     B4, A3                       ; 1 | pointer to output matrix
||  MV        .L2     A4, B0                       ; 1 | pointer to input matrix
||  MVKH      .S2     0x00000004, B6               ; 1 | N=4
||  MV        .D2     B4, B2                       ; 1 | pointer to output matrix
; 3
    SHL       .S1     A6, 3, A7                    ; 1 | 2*N*4
||  ADD       .L2     B0, 8, B0                    ; 1 | update input pointer
||  SHL       .S2     B6, 3, B7                    ; 1 | 2*N*4
||  MV        .D2     B4, B3                       ; 1 | pointer to output matrix
; 4
    SHL       .S1     A6, 2, A6                    ; 1 | 1*N*4
||  ADD       .L2     B1, 8, B1                    ; 1 | update input pointer
||  SHL       .S2     B6, 2, B6                    ; 1 | 1*N*4
; 5
    ADD       .L1     A1, A6, A1                   ; 1 | update input pointer
||  LDDW      .D1     *A0, A31:A30                 ; 5 | load two 4-B inputs
||  ADD       .L2     B1, B6, B1                   ; 1 | update input pointer
||  LDDW      .D2     *B0, B31:B30                 ; 5 | load two 4-B inputs
; 6
    ADD       .L1     A0, A7, A0                   ; 1 | update input pointer
||  LDDW      .D1     *A1, A29:A28                 ; 5 | load two 4-B inputs
||  ADD       .L2     B0, B7, B0                   ; 1 | update input pointer
||  LDDW      .D2     *B1, B29:B28                 ; 5 | load two 4-B inputs
; 7
    ADD       .L1     A1, A7, A1                   ; 1 | update input pointer
||  LDDW      .D1     *A0, A27:A26                 ; 5 | load two 4-B inputs
||  ADD       .L2     B1, B7, B1                   ; 1 | update input pointer
||  ADD       .S2     B2, B7, B2                   ; 1 | update output pointer
||  LDDW      .D2     *B0, B27:B26                 ; 5 | load two 4-B inputs
; 8
    ADD       .L1     A0, A7, A0                   ; 1 | update input pointer
||  ADD       .S1     A3, A6, A3                   ; 1 | update output pointer
||  LDDW      .D1     *A1, A25:A24                 ; 5 | load two 4-B inputs
||  ADD       .L2     B0, B7, B0                   ; 1 | update input pointer
||  ADD       .S2     B2, B6, B3                   ; 1 | update output pointer
||  LDDW      .D2     *B1, B25:B24                 ; 5 | load two 4-B inputs
; 9
    NOP
; 10
    MV        .L1     A31, A8                      ; 1 | temp
||  MV        .L2     B31, B8                      ; 1 | temp
||  B         .S2     B5                           ; 6 | return from function
; 11
    MV        .L1     A28, A31                     ; 1 | swap values
||  MV        .L2     B28, B31                     ; 1 | swap values
; 12
    MV        .L1     A8, A28                      ; 1 | swap values
||  MV        .S1     A27, A9                      ; 1 | temp
||  STDW      .D1     A31:A30, *A2                 ; 1 | store two 4-B outputs
||  MV        .L2     B8, B28                      ; 1 | swap values
||  MV        .S2     B27, B9                      ; 1 | temp
||  STDW      .D2     B31:B30, *B2                 ; 1 | store two 4-B outputs
; 13
    MV        .S1     A24, A27                     ; 1 | swap values
||  STDW      .D1     A29:A28, *A3                 ; 1 | store two 4-B outputs
||  MV        .S2     B24, B27                     ; 1 | swap values
||  STDW      .D2     B29:B28, *B3                 ; 1 | store two 4-B outputs
; 14
    MV        .S1     A9, A24                      ; 1 | swap values
||  STDW      .D1     A27:A26, *++A2[1]            ; 1 | store two 4-B outputs
||  MV        .S2     B9, B24                      ; 1 | swap values
||  STDW      .D2     B27:B26, *++B2[1]            ; 1 | store two 4-B outputs
; 15
    STDW      .D1     A25:A24, *++A3[1]            ; 1 | store two 4-B outputs
||  STDW      .D2     B25:B24, *++B3[1]            ; 1 | store two 4-B outputs
;
;
;;
;  CPU cycles:  15
;   .L1 :  9  ( 60.0 % )
;   .S1 :  8  ( 53.33 % )
;   .M1 :  0  ( 0.0 % )
;   .D1 :  10  ( 66.67 % )
;   .L2 :  11  ( 73.33 % )
;   .S2 :  10  ( 66.67 % )
;   .M2 :  0  ( 0.0 % )
;   .D2 :  11  ( 73.33 % )
