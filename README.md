# Software mapping tool
TODO

## Installation on GNU/Linux
```bash
# project requires the following packages to be installed correctly:
# GCC compiler
sudo apt install g++
# GNU make utility
sudo apt install make
# Documentation system for C, C++, Java, Python and other languages
sudo apt install doxygen
# Graph drawing tools
sudo apt install graphviz
```

## Use
```bash
# compiling the dspArchEdit tool only
make dsparchedit
# compiling the project, equivalent to make
make all
# deletes the compilation outputs
make clean
# create Doxygen documentation
make doc
```

```bash
# execute dspArchEdit tool
./dsparchedit
# execute dspArchEdit tool with loaded architecture from json file
./dsparchedit tms320c6678.json
```

## Application notes
### File Template Hierarchy
```bash
.
├── bin/
│   ├── dsparchedit
│   ├── dspmapping
│   ├── fft4_real.txt
│   └── tms320c6678.json
├── c66x-examples/
│   ├── MATH_dp_mat_mul/
│   ├── MATH_dp_mat_trans/
│   ├── MATH_dp_maxval/
│   ├── MATH_mat_trans/
│   ├── MATH_sp_mat_mul/
│   ├── MATH_sp_mat_trans/
│   └── MATH_sp_maxval/
├── LICENSE
├── Makefile
├── README.md
└── src/
    ├── Algorithm/
    ├── ArchEdit/
    ├── Architecture/
    ├── Common/
    ├── Doxyfile_dsparchedit
    ├── Doxyfile_dspmapping
    ├── DoxygenLayout.xml
    └── Mapping/
```
The source files `*.cpp`, header files `*.h` and doxygen config files are located in the `src/` folder. The compilation outputs (binary files `dsparchedit`, `dspmapping`, architecture json files `*.json` and algorithm description files `*.txt`) are located in the `bin/` folder. The c66x DSP examples are located in the `c66x-examples/` folder. The generated doxygen documention files are located in the `Doxygen/` folder.
