# Makefile for the DspMapper
# Author: Roman Mego

CC = g++
CFLAGS = -std=c++0x -DNDEBUG -O3 -Wall -c -fmessage-length=0
DOXY = doxygen


BIN_DIR = bin
OBJ_DIR = obj
DOC_DIR = Doxygen


COMMON_SOURCE += src/Algorithm/algorithm.cpp
COMMON_SOURCE += src/Architecture/architecture.cpp
COMMON_SOURCE += src/Architecture/datapath.cpp
COMMON_SOURCE += src/Architecture/crosspath.cpp
COMMON_SOURCE += src/Architecture/functionalunit.cpp
COMMON_SOURCE += src/Architecture/instruction.cpp
COMMON_SOURCE += src/Architecture/register.cpp
COMMON_SOURCE += src/Architecture/registerassign.cpp
COMMON_SOURCE += src/Architecture/registergroup.cpp
COMMON_SOURCE += src/Common/String/string.cpp
COMMON_SOURCE += src/Common/CmdLine/cmdline.cpp
COMMON_SOURCE += src/Common/Arguments/commandlineparser.cpp
COMMON_SOURCE += src/Common/Types/types.cpp
COMMON_SOURCE += src/Common/cJSON/cJSON.c
COMMON_SOURCE += src/Common/Version/version.c
COMMON_SOURCE += src/Common/Messages/message.cpp


MAPPING_SOURCE += ${COMMON_SOURCE}
MAPPING_SOURCE += src/Mapping/main.cpp
MAPPING_SOURCE += src/Mapping/mapping.cpp
MAPPING_SOURCE += src/Mapping/NodeMap/nodemap.cpp
MAPPING_SOURCE += src/Mapping/SignalMap/signalmap.cpp
MAPPING_SOURCE += src/Mapping/usage.cpp
MAPPING_SOURCE += src/Mapping/sorting.cpp
MAPPING_SOURCE += src/Common/FileInfo/fileinfo.cpp

MAPPING_OBJ := ${MAPPING_SOURCE:%.c=%.mp.o}
MAPPING_OBJ := ${MAPPING_OBJ:%.cpp=%.mp.o}
FULL_MAPPING_OBJ = $(addprefix $(OBJ_DIR)/,$(MAPPING_OBJ))


ARCHEDIT_SOURCE += ${COMMON_SOURCE}
ARCHEDIT_SOURCE += src/ArchEdit/main.cpp
ARCHEDIT_SOURCE += src/ArchEdit/global.cpp
ARCHEDIT_SOURCE += src/ArchEdit/add/add.cpp
ARCHEDIT_SOURCE += src/ArchEdit/remove/remove.cpp
ARCHEDIT_SOURCE += src/ArchEdit/set/set.cpp
ARCHEDIT_SOURCE += src/ArchEdit/set/crosspath/crosspath.cpp
ARCHEDIT_SOURCE += src/ArchEdit/set/functionalunit/functionalunit.cpp
ARCHEDIT_SOURCE += src/ArchEdit/set/instruction/instruction.cpp
ARCHEDIT_SOURCE += src/ArchEdit/view/view.cpp

ARCHEDIT_OBJ := ${ARCHEDIT_SOURCE:%.c=%.ae.o}
ARCHEDIT_OBJ := ${ARCHEDIT_OBJ:%.cpp=%.ae.o}
FULL_ARCHEDIT_OBJ = $(addprefix $(OBJ_DIR)/,$(ARCHEDIT_OBJ))


COMMON_INCLUDE += src/Common/Version

MAPPING_INCLUDE += ${COMMON_INCLUDE}
MAPPING_INCLUDE += src/Mapping

ARCHEDIT_INCLUDE += ${COMMON_INCLUDE}
ARCHEDIT_INCLUDE += src/ArchEdit


# rules to make object (*.o) files
$(OBJ_DIR)/%.ae.o: %.c
	mkdir -p $(dir $@ $<)
	$(CC) $(CFLAGS) $(addprefix -I, ${ARCHEDIT_INCLUDE}) $< -o $@

$(OBJ_DIR)/%.ae.o: %.cpp
	mkdir -p $(dir $@ $<)
	$(CC) $(CFLAGS) $(addprefix -I, ${ARCHEDIT_INCLUDE}) $< -o $@


$(OBJ_DIR)/%.mp.o: %.c
	mkdir -p $(dir $@ $<)
	$(CC) $(CFLAGS) $(addprefix -I, ${MAPPING_INCLUDE}) $< -o $@

$(OBJ_DIR)/%.mp.o: %.cpp
	mkdir -p $(dir $@ $<)
	$(CC) $(CFLAGS) $(addprefix -I, ${MAPPING_INCLUDE}) $< -o $@


# rule to make binary
all: dsparchedit dspmapping

dsparchedit: ${FULL_ARCHEDIT_OBJ}
	mkdir -p ${BIN_DIR}
	${CC} -o ${BIN_DIR}/dsparchedit $(FULL_ARCHEDIT_OBJ)

dspmapping: ${FULL_MAPPING_OBJ}
	@mkdir -p ${BIN_DIR}
	${CC} -o ${BIN_DIR}/dspmapping $(FULL_MAPPING_OBJ)

doc:
	@mkdir -p ${DOC_DIR}
	@(cd src && ${DOXY} Doxyfile_dsparchedit)
	@(cd src && ${DOXY} Doxyfile_dspmapping)

clean:
	rm -fr ${FULL_ARCHEDIT_OBJ}
	rm -fr ${FULL_MAPPING_OBJ}
	rm -fr ${DOC_DIR}
