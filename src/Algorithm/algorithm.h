/**
 * @file    algorithm.h
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Algorithm structure definition.
 */

#ifndef ALGORITHM_H_
#define ALGORITHM_H_

#include <iostream>
#include <list>
#include <vector>
#include "../Common/Types/types.h"

namespace Algorithm
{
    /**
     * Class describing algorithm.
     */
    class Algorithm
    {
    private:
        Types::DataType DefaultDataType;

    public:

        class Signal;

        class NodeInput {
            typedef union {
                Signal* signal;
                double constant;
                int index;
            } nodeinput_t;
        public:
            NodeInput();
            NodeInput(Signal* pSignal);
            NodeInput(double Constant);
            NodeInput(int Index);
            Types::NodeInputType Type;
            nodeinput_t Value;
        };

        /**
         * Class describing node in the algorithm.
         */
        class Node {
        public:
            Algorithm *Owner;
            Types::Operation Operation;         ///< Node operation.
            std::vector<NodeInput> InputSignal; ///< List of the input signals.
            Signal *OutputSignal;               ///< Output signal.

            int DefinitionLine;                 ///< Line where the node is defined.

            std::string GetNotation();
        };

        /**
         * Class describing signal.
         */
        class Signal {
        public:
            Algorithm *Owner;
            std::string Name;           ///< Name of the signal in the algorithm.
            Types::SignalType Type;     ///< Type of the signal.
            Types::DataType DataType;   ///< Data type representing the signal.

            int DefinitionLine;         ///< Line where the signal is defined.
        };

        /**
         * Class describing signal alias.
         */
        class SignalAlias {
        public:
            std::list<Signal*> Members;         ///< List of signals with the same value.
            Signal* GetMasterSignal();
        };

        std::list<Signal> Signals;              ///< List of signals in the algorithm.
        std::list<Node> Nodes;                  ///< List of nodes in the algorithm.
        std::list<SignalAlias> SignalAliases;   ///< List of signal aliases.
        std::string DefinitionFile;             ///< File where the algorithm is defined.

        int Load(std::string FilePath, Types::DataType DefaultDataType);
        int AddSignal(std::string SignalName, Types::DataType DataType, Types::SignalType SignalType, int DefinitionLine);
        Signal* GetSignal(std::string SignalName);
        Signal* GetSignal(Signal* pSignalAlias);
        int AddSignal(std::string Line, int DefinitionLine);
        int AddNode(std::string Line, int DefinitionLine);
        int AddAlias(std::string Line, int DefinitionLine);
        int AddConstant(std::string Line, int DefinitionLine);
    };
}

#endif /* ALGORITHM_H_ */
