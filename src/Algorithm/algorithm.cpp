/**
 * @file    algorithm.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Functions and methods for algorithm processing.
 */

#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <cctype>
#include "algorithm.h"
#include "../Common/String/string.h"
#include "../Common/Messages/message.h"

/**
 * @brief  Get part of string bordered by two characters.
 * @param  Str      Original string.
 * @param  cLeft    Left border character or 0.
 * @param  cRight   Right border character or 0.
 * @return Part of string without whitespace.
 */
std::string Parse(std::string Str, char cLeft, char cRight)
{
    int iLeft, iRight;

    if (cLeft != 0)
    {
        iLeft = Str.find_first_of(cLeft) + 1;
    }
    else
    {
        iLeft = 0;
    }

    if (cRight != 0)
    {
        iRight = Str.find_last_of(cRight);
    }
    else
    {
        iRight = Str.size();
    }

    return StringTrim(Str.substr(iLeft, iRight - iLeft));
}

// TODO
std::list<std::string> Parse(std::string String)
{
    std::list<std::string> strlist;
    std::stringstream strstream(String);

    while (strstream.good())
    {
        std::string strtmp;
        strstream >> strtmp;
        strlist.push_back(strtmp);
    }

    return strlist;
}

/**
 * @brief   Remove comment starting with # from line.
 * @param   Str Original line content.
 * @return  Line without comment, but containing whitespaces.
 */
std::string RemoveComment(std::string Str)
{
    int index = Str.find_first_of('#');
    return Str.substr(0, index);
}

/**
 * @brief  Load algorithm from the file.
 * @param  FilePath Path to file containing algorithm.
 * @param  DefaultDataType  Default data type for undefined signals.
 * @return 0 on success.
 */
int ::Algorithm::Algorithm::Load(std::string FilePath, Types::DataType DefaultDataType)
{
    std::ifstream file;
    std::string line;
    int lineCounter = 0;

    this->DefaultDataType = DefaultDataType;
    this->DefinitionFile.assign(FilePath);

    file.open(FilePath.c_str(), std::ios::in);

    /* Test if file exists */
    if (!file.is_open())
    {
        Error("Could not open the file \"" + FilePath + "\"");
    }

    /* Repeat until end of file is reached */
    while (!file.eof())
    {
        getline(file, line);                // Read single line from file
        lineCounter++;                      // Increase line counter

        /* Clean expression */
        line = RemoveComment(line);
        line = StringTrim(line);

        /* If expression is empty, skip current line */
        if (line.length() == 0)
        {
            continue;
        }

        size_t signEquals = line.find('=');
        size_t signParenthesesLeft = line.find('(');
        size_t signParenthesesRight = line.find(')');

        /* Assignments contains '=' */
        if (signEquals != std::string::npos)
        {
            /* Operation assignment */
            if (signParenthesesLeft != std::string::npos &&
                signParenthesesRight != std::string::npos &&
                signParenthesesLeft < signParenthesesRight)
            {
                this->AddNode(line, lineCounter);
            }
            /* Alias or constant assignment */
            else if (signParenthesesLeft == std::string::npos &&
                    signParenthesesRight == std::string::npos)
            {
                /* Constant assignment if there is number at the end */
                // TODO Valid checking of number
                if (isdigit(line.back()))
                {
                    this->AddConstant(line, lineCounter);
                }
                /* Alias */
                else
                {
                    this->AddAlias(line, lineCounter);
                }
            }
            /* Bad format */
            else
            {
                Error("Bad format", FilePath, lineCounter);
            }
        }
        /* Signal definition or operations without assignment */
        else
        {
            /* Signal definition */
            if (signParenthesesLeft == std::string::npos &&
                signParenthesesRight == std::string::npos)
            {
                this->AddSignal(line, lineCounter);
            }
            /* Operation */
            else if (signParenthesesLeft != std::string::npos &&
                     signParenthesesRight != std::string::npos &&
                     signParenthesesLeft < signParenthesesRight)
            {
                // TODO Operation without assignment
                this->AddNode(line, lineCounter);
            }
            /* Bad format */
            else
            {
                Error("Bad format", FilePath, lineCounter);
            }
        }
    }

    file.close();

    return 0;
}

/**
 * @brief  Add signal to algorithm.
 * @param  Line             Whole definition string.
 * @param  DefinitionLine   Line where the node is defined.
 * @return 0 on success.
 */
int ::Algorithm::Algorithm::AddSignal(std::string Line, int DefinitionLine)
{
    Signal newSignal;
    std::string signalName;
    std::list<std::string> lineList = Parse(Line);
    std::list<std::string>::iterator it = lineList.begin();
    Types::SignalType signalType = Types::GetSignalType(*it);
    Types::DataType sigDataType = this->DefaultDataType;

    it++;

    if (lineList.size() == 3)
    {
        sigDataType = Types::GetDataType(*it);
        it++;
    }
    else if (lineList.size() != 2)
    {
        Error("Syntax error", this->DefinitionFile, DefinitionLine);
    }

    signalName.assign(*it);

    /* Test if signal already exists */
    if (GetSignal(signalName))
    {
        Error("Signal \"" + signalName + "\" already exists", this->DefinitionFile, DefinitionLine);
    }

    /* Save values to temporary signal */
    newSignal.Owner = this;
    newSignal.Name = signalName;
    newSignal.Type = signalType;
    newSignal.DataType = sigDataType;
    newSignal.DefinitionLine = DefinitionLine;

    /* Copy temporary signal to list */
    this->Signals.push_back(newSignal);

    return 0;
}

/**
 * @brief  Get signal pointer.
 * @param  SignalName   Name of signal to find.
 * @return Pointer to signal or NULL.
 */
::Algorithm::Algorithm::Signal* ::Algorithm::Algorithm::GetSignal(std::string SignalName)
{
    std::list<Signal>::iterator it;

    /* Repeat for every signal in list */
    for (it = this->Signals.begin(); it != this->Signals.end(); ++it)
    {
    	/* If current signal name match */
        if (!it->Name.compare(SignalName))
        {
        	/* Return pointer to signal */
            return &*it;
        }
    }

    /* Return NULL if not found */
    return NULL;
}

::Algorithm::Algorithm::Signal* ::Algorithm::Algorithm::GetSignal(Algorithm::Signal* pSignalAlias)
{
    Signal* retVal = NULL;

    if (pSignalAlias->Type == Types::SignalType::ALIAS)
    {
        for (std::list<SignalAlias>::iterator itAliasGroup = this->SignalAliases.begin(); itAliasGroup != this->SignalAliases.end(); itAliasGroup++)
        {
            if (std::find(itAliasGroup->Members.begin(), itAliasGroup->Members.end(), pSignalAlias) != itAliasGroup->Members.end())
            {
                retVal = itAliasGroup->GetMasterSignal();
                break;
            }
        }
    }
    else
    {
        retVal = pSignalAlias;
    }

    return retVal;
}

/**
 * @brief  Add operation node to algorithm.
 * @param  Line             Whole operation string.
 * @param  DefinitionLine   Line where the node is defined.
 * @return 0 on success.
 */
int ::Algorithm::Algorithm::AddNode(std::string Line, int DefinitionLine)
{
    Node newNode;

    std::string operation;
    std::string out;
    std::stringstream in(Parse(Line, '(', ')'));
    std::string tmpIn;

    newNode.Owner = this;
    newNode.DefinitionLine = DefinitionLine;

    /* Process output signal */
    if (Line.find('=') == std::string::npos)
    {
        newNode.OutputSignal = NULL;
        operation = Parse(Line, 0, '(');
    }
    else
    {
        out = Parse(Line, 0, '=');
        newNode.OutputSignal = this->GetSignal(out);
        if (newNode.OutputSignal == NULL)
        {
            Error("Signal " + out + " not found.", this->DefinitionFile, DefinitionLine);
        }

        operation = Parse(Line, '=', '(');
    }

    /* Store operation type */
    newNode.Operation = Types::GetOperation(operation);
    if (newNode.Operation == Types::Operation::NONE)
    {
        Error("Unknown operation " + operation, DefinitionFile, DefinitionLine);
    }

    /* Separate input signals */
    // TODO
    int inIndex = 0;
    while (std::getline(in, tmpIn, ','))
    {
        NodeInput inSignal;
        tmpIn = StringTrim(tmpIn); // Get clean string for input argument of operation

        switch (Types::GetOperationInputType(newNode.Operation, inIndex))
        {
            case Types::NodeInputType::CONSTANT:
                // TODO Number checking
                inSignal = NodeInput(std::stod(tmpIn));
                break;
            case Types::NodeInputType::INDEX:
                // TODO Number checking
                inSignal = NodeInput(std::stoi(tmpIn));
                break;
            case Types::NodeInputType::POINTER:
                inSignal = NodeInput(this->GetSignal(tmpIn));
                if (inSignal.Value.signal == NULL)
                {
                    Error("Signal " + tmpIn + " not found.", this->DefinitionFile, DefinitionLine);
                }
                if (inSignal.Value.signal->DataType != Types::DataType::POINTER)
                {
                    Error("Signal " + tmpIn + " is not a pointer.");
                }
                break;
            default:
                inSignal = NodeInput(this->GetSignal(tmpIn));
                if (inSignal.Value.signal == NULL)
                {
                    Error("Signal " + tmpIn + " not found.", this->DefinitionFile, DefinitionLine);
                }
                if (inSignal.Value.signal->DataType == Types::DataType::POINTER)
                {
                    Error("Signal " + tmpIn + " is pointer.");
                }
                break;
        }

    	newNode.InputSignal.push_back(inSignal);
    	inIndex++;
    }

    /* Test if number of inputs corresponds to the operation */
    if (Types::GetOperationNumOfInputs(newNode.Operation) != newNode.InputSignal.size())
    {
    	Error("Expected number of input arguments is " + std::to_string(Types::GetOperationNumOfInputs(newNode.Operation)), this->DefinitionFile, DefinitionLine);
    }

    /* Test data type equality of the signals */
    if (newNode.InputSignal.size())
    {
        Types::DataType nodeDataType = Types::DataType::NONE;

        if (newNode.OutputSignal != NULL)
        {
            nodeDataType = newNode.OutputSignal->DataType;
        }

        for (std::vector<NodeInput>::iterator it = newNode.InputSignal.begin(); it != newNode.InputSignal.end(); it++)
        {
            if (it->Type != Types::NodeInputType::SIGNAL)
            {
                continue;
            }

            if (nodeDataType == Types::DataType::NONE)
            {
                nodeDataType = it->Value.signal->DataType;
            }

            if (it->Value.signal->DataType != nodeDataType)
            {
                Error("Data type of the arguments is not equal", this->DefinitionFile, DefinitionLine);
            }
        }
    }

    this->Nodes.push_back(newNode);

    return 0;
}

/**
 * @brief   Add signal alias.
 * @param   Line            Operation string from the file.
 * @param   DefinitionLine  Line where the alias is defined.
 * @return  0 on success.
 */
int ::Algorithm::Algorithm::AddAlias(std::string Line, int DefinitionLine)
{
    std::string target = Parse(Line, 0, '=');
    std::string source = Parse(Line, '=', 0);

    Signal* pTarget = this->GetSignal(target);
    Signal* pSource = this->GetSignal(source);

    if (pTarget == NULL)
    {
        Error("Undefined signal " + target, this->DefinitionFile, DefinitionLine);
    }
    else if (pSource == NULL)
    {
        Error("Undefined signal " + source, this->DefinitionFile, DefinitionLine);
    }

    pTarget->Type = Types::SignalType::ALIAS;

    // Find alias where the signals can be added
    for (std::list<SignalAlias>::iterator alias = this->SignalAliases.begin(); alias != this->SignalAliases.end(); alias++)
    {
        bool targetFound = std::find(alias->Members.begin(), alias->Members.end(), pTarget) != alias->Members.end();
        bool sourceFound = std::find(alias->Members.begin(), alias->Members.end(), pSource) != alias->Members.end();

        if (!targetFound && !sourceFound)
        {
            continue;
        }
        else if (targetFound && !sourceFound)
        {
            alias->Members.push_back(pSource);
        }
        else if (!targetFound && sourceFound)
        {
            alias->Members.push_back(pTarget);
        }
        else
        {
            Warning("Redundant signal alias.", this->DefinitionFile, DefinitionLine);
            return 0;
        }

        alias->Members.sort();

        for (std::list<SignalAlias>::iterator testAlias = this->SignalAliases.begin(); testAlias != this->SignalAliases.end(); testAlias++)
        {
            bool merge;

            if (testAlias == alias)
            {
                continue;
            }

            if (!targetFound)
            {
                merge = std::find(alias->Members.begin(), alias->Members.end(), pTarget) != alias->Members.end();
            }
            else
            {
                merge = std::find(alias->Members.begin(), alias->Members.end(), pTarget) != alias->Members.end();
            }

            if (!merge)
            {
                continue;
            }

            alias->Members.merge(testAlias->Members);
            this->SignalAliases.erase(testAlias);

            break;
        }

        return 0;
    }

    // If not found, add new alias group.
    SignalAlias newAlias;
    newAlias.Members.push_back(pTarget);
    newAlias.Members.push_back(pSource);
    newAlias.Members.sort();
    this->SignalAliases.push_back(newAlias);

    return 0;
}

// TODO
int ::Algorithm::Algorithm::AddConstant(std::string Line, int DefinitionLine)
{
    Node newNode;

    std::string target = Parse(Line, 0, '=');
    std::string inStr = Parse(Line, '=', 0);


    Signal* pTarget = this->GetSignal(target);
    NodeInput input(std::stod(inStr));

    newNode.Owner = this;
    newNode.Operation = Types::Operation::CONST;
    newNode.OutputSignal = pTarget;
    newNode.DefinitionLine = DefinitionLine;
    newNode.InputSignal.push_back(input);

    this->Nodes.push_back(newNode);

    return 0;
}

/**
 * @brief   Get the notation of the node.
 * @return  Notation of the node.
 */
std::string Algorithm::Algorithm::Node::GetNotation()
{
    std::string retVal;

    if (this->OutputSignal != NULL)
    {
        retVal.append(this->OutputSignal->Name);
        retVal.append(" = ");
    }

    if (this->Operation == Types::Operation::CONST)
    {
        std::ostringstream str;
        str << this->InputSignal.begin()->Value.constant;
        retVal.append(str.str());
    }
    else
    {
        retVal.append(Types::GetOperationStr(this->Operation));
        retVal.append("(");

        for (std::vector<NodeInput>::iterator it = this->InputSignal.begin(); it != this->InputSignal.end(); it++)
        {
            if (it->Type == Types::NodeInputType::INDEX)
            {
                std::ostringstream str;
                str << it->Value.index;
                retVal.append(str.str());
            }
            else
            {
                retVal.append(it->Value.signal->Name);
            }

            if (it !=  --this->InputSignal.end())
            {
                retVal.append(", ");
            }
        }

        retVal.append(")");
    }

    return retVal;
}

// TODO
::Algorithm::Algorithm::NodeInput::NodeInput()
{
    this->Type = Types::NodeInputType::NONE;
}

::Algorithm::Algorithm::NodeInput::NodeInput(Signal* pSignal)
{
    if (pSignal->DataType == Types::DataType::POINTER)
    {
        this->Type = Types::NodeInputType::POINTER;
    }
    else
    {
        this->Type = Types::NodeInputType::SIGNAL;
    }
    this->Value.signal = pSignal;
}

::Algorithm::Algorithm::NodeInput::NodeInput(double Constant)
{
    this->Type = Types::NodeInputType::CONSTANT;
    this->Value.constant = Constant;
}

::Algorithm::Algorithm::NodeInput::NodeInput(int Index)
{
    this->Type = Types::NodeInputType::INDEX;
    this->Value.index = Index;
}

::Algorithm::Algorithm::Signal* ::Algorithm::Algorithm::SignalAlias::GetMasterSignal()
{
    Signal* retVal = NULL;

    for (std::list<Signal*>::iterator itSig = this->Members.begin(); itSig != this->Members.end(); itSig++)
    {
        if ((*itSig)->Type != Types::SignalType::ALIAS)
        {
            retVal = *itSig;
            break;
        }
    }

    return retVal;
}
