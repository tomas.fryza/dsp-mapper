/**
 ***********************************************************************
 * @file    cmdline.h
 * @author  Roman Mego
 * @date    8.10.2016
 * @brief   Command line class definition.
 */


#ifndef CMDLINE_H_
#define CMDLINE_H_

#include <string>
#include <list>


/**
 ***********************************************************************
 * Command line class
 */
class CommandLine {
public:
    typedef int (*CommandFunction_t)(int argc, char *argv[]);   ///< Type definition for the function call pointer

    /**
     * Class describing single command from the command line
     */
    class Command {
    public:
        std::string Cmd;            ///< Command
        CommandFunction_t Call;     ///< Pointer to the called function
        std::string Description;    ///< Description of the command
        std::string Help;           ///< Help text for the command
    };

    void AddCommand(std::string Cmd, CommandFunction_t Call, std::string Description, std::string Help);
    int LineProcess(std::string line);
    int ArgProcess(int argc, char* argv[]);
    void ShowHelp();
    void ShowHelp(std::string Cmd);

private:
    std::list<Command> CommandList; ///< List of available commands
};

#endif /* CMDLINE_H_ */
