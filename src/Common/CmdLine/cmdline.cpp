/**
 ***********************************************************************
 * @file    cmdline.cpp
 * @author  Roman Mego
 * @date    28.11.2017
 * @brief   Command line class methods.
 */


#include <cstdlib>
#include <iostream>
#include <iomanip>
#include "cmdline.h"


/**
 ***********************************************************************
 * @brief   Add executable command to the command line processing.
 * @param   Cmd         Command entered in command line.
 * @param   Call        Function call.
 * @param   Description Description in the list of commands.
 * @param   Help        Help text for the command.
 */
void CommandLine::AddCommand(std::string Cmd, CommandFunction_t Call, std::string Description, std::string Help)
{
    Command tmpCmd;

    tmpCmd.Cmd = Cmd;
    tmpCmd.Call = Call;
    tmpCmd.Description = Description;
    tmpCmd.Help = Help;

    this->CommandList.push_back(tmpCmd);
}


/**
 ***********************************************************************
 * @brief   Process the string entered in the command line.
 * @param   line    String to process.
 * @return  Return value from the called function.
 */
int CommandLine::LineProcess(std::string line)
{
    int retVal;
    int argc;
    char** argv;
    std::list<std::string> cmdList;
    size_t posRight;
    size_t posLeft = 0;
    int i = 0;

    while (true)
    {
        if (line.at(posLeft) == '\"')
        {
            posRight = line.find_first_of('\"', posLeft + 1);

            if (posRight == std::string::npos)
            {
                return -1;
            }

            cmdList.push_back(line.substr(posLeft + 1, posRight - posLeft - 1));
        }
        else
        {
            posRight = line.find_first_of(' ', posLeft);

            if (posRight == std::string::npos)
            {
                cmdList.push_back(line.substr(posLeft));
                break;
            }

            cmdList.push_back(line.substr(posLeft, posRight - posLeft));
        }

        if (posRight + 1 < line.size())
        {
            posLeft = line.find_first_not_of(' ', posRight + 1);
            if (posLeft == std::string::npos)
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    argc = cmdList.size();
    argv = (char**)malloc(argc * sizeof(char*));

    for (std::list<std::string>::iterator it = cmdList.begin(); it != cmdList.end(); it++)
    {
        argv[i] = (char*)it->c_str();
        i++;
    }

    retVal = this->ArgProcess(argc, argv);

    free(argv);

    return retVal;
}


/**
 ***********************************************************************
 * @brief   Execute command from parsed arguments.
 * @param   argc    Number of arguments.
 * @param   argv    Argument array.
 * @return  Return value from called function.
 */
int CommandLine::ArgProcess(int argc, char* argv[])
{
    std::string cmd(argv[0]);

    if ((cmd.compare("help") == 0) || (cmd.compare("?") == 0))
    {
        if (argc == 1)
        {
            this->ShowHelp();
            return 0;
        }
        else if (argc == 2)
        {
            this->ShowHelp(argv[1]);
            return 0;
        }
    }

    for (std::list<Command>::iterator it = this->CommandList.begin(); it != this->CommandList.end(); it++)
    {
        if (cmd.compare(it->Cmd) == 0)
        {
            return it->Call(argc, argv);
        }
    }

    std::cout << "\033[31mUnknown command \"" << cmd << "\"\033[0m" << std::endl;
    std::cout << "Use \"help\" or \"?\" to list available commands" << std::endl;

    return -1;
}


/**
 ***********************************************************************
 * @brief   Print list of all available commands.
 */
void CommandLine::ShowHelp()
{
    unsigned int cmdWidth = 0;

    std::cout << "Available commands:" << std::endl;

    for (std::list<Command>::iterator it = CommandList.begin(); it != CommandList.end(); it++)
    {
        if (it->Cmd.size() > cmdWidth)
        {
            cmdWidth = it->Cmd.size();
        }
    }

    for (std::list<Command>::iterator it = CommandList.begin(); it != CommandList.end(); it++)
    {
        std::cout << "  " << std::setw(cmdWidth) << std::left << it->Cmd << " " << it->Description << std::endl;
    }
}


/**
 ***********************************************************************
 * @brief   Show help for the command.
 * @param   Cmd Command to show the help.
 */
void CommandLine::ShowHelp(std::string Cmd)
{
    for (std::list<Command>::iterator it = CommandList.begin(); it != CommandList.end(); it++)
    {
        if (it->Cmd.compare(Cmd) == 0)
        {
            std::cout << it->Description << std::endl;
            std::cout << " " << std::endl;
            std::cout << it->Help << std::endl;
            return;
        }
    }

    std::cout << "\033[31mCommand " << Cmd << " not found.\033[0m" << std::endl;
    std::cout << "Use \"help\" or \"?\" to list available commands" << std::endl;
}
