/**
 * @file    types.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Data types.
 */

#include "types.h"
#include <algorithm>

using namespace std;

Types::operation_table_t Types::operationTable[] = {
    { Types::Operation::ABS,      "abs",      1,    true,  { NodeInputType::SIGNAL } },
    { Types::Operation::ADD,      "add",      2,    true,  { NodeInputType::SIGNAL,  NodeInputType::SIGNAL } },
    { Types::Operation::CONST,    "const",    1,    true,  { NodeInputType::CONSTANT } },
    { Types::Operation::CONSTH,   "consth",   1,    true,  { NodeInputType::CONSTANT } },
    { Types::Operation::CONSTL,   "constl",   1,    true,  { NodeInputType::CONSTANT } },
    { Types::Operation::DIV,      "div",      2,    true,  { NodeInputType::SIGNAL,  NodeInputType::SIGNAL } },
    { Types::Operation::MPY,      "mpy",      2,    true,  { NodeInputType::SIGNAL,  NodeInputType::SIGNAL } },
    { Types::Operation::SUB,      "sub",      2,    true,  { NodeInputType::SIGNAL,  NodeInputType::SIGNAL } },
    { Types::Operation::LOADMEM,  "load",     2,    true,  { NodeInputType::POINTER, NodeInputType::INDEX } },
    { Types::Operation::STOREMEM, "store",    3,    false, { NodeInputType::SIGNAL,  NodeInputType::POINTER, NodeInputType::INDEX } },
    { Types::Operation::MOVE,     "move",     1,    true,  { NodeInputType::SIGNAL }  },
    { Types::Operation::NONE,     0,          0,    false }
};

Types::datatype_table_t Types::dataTypeTable[] = {
    { Types::DataType::INT32,    "int32",    4,  REPR_INTEGER },
    { Types::DataType::INT64,    "int64",    8,  REPR_INTEGER },
    { Types::DataType::FLOAT,    "float",    4,  REPR_FLOAT },
    { Types::DataType::DOUBLE,   "double",   8,  REPR_FLOAT },
    { Types::DataType::POINTER,  "pointer",  0,  REPR_INTEGER },
    { Types::DataType::NONE,     0,          0,  REPR_NONE }
};

Types::signaltype_table_t Types::signalTypeTable[] = {
    { Types::SignalType::SIGNAL, "signal" },
    { Types::SignalType::INPUT,  "input" },
    { Types::SignalType::OUTPUT, "output" },
    { Types::SignalType::NONE,   0 }
};

/**
 * @brief   Get operation from string.
 * @param   operation   String representing the operation.
 * @return  Operation.
 */
Types::Operation Types::GetOperation(string operation)
{
	std::transform(operation.begin(), operation.end(), operation.begin(), ::tolower);
    for (operation_table_t *pOperation = operationTable; pOperation->value != Operation::NONE; pOperation++)
    {
        if (!operation.compare(pOperation->format))
        {
            return pOperation->value;
        }
    }
    return Operation::NONE;
}

/**
 * @brief   Get number of inputs of the operation.
 * @param   operation   Operation to get number of inputs.
 * @return  Number of inputs or -1 on error.
 */
std::size_t Types::GetOperationNumOfInputs(Operation operation)
{
    for (operation_table_t *pOperation = operationTable; pOperation->value != Operation::NONE; pOperation++)
    {
        if (pOperation->value == operation)
        {
            return pOperation->numOfInputParams;
        }
    }
    return -1;
}

/**
 * @brief   Find string representing the operation.
 * @param   operation   Operation to find.
 * @return  String representing operation.
 */
std::string Types::GetOperationStr(Operation operation)
{
    operation_table_t *pOperation;
    for (pOperation = operationTable; pOperation->value != Operation::NONE; pOperation++)
    {
        if (pOperation->value == operation)
        {
            return pOperation->format;
        }
    }
    return "";
}

Types::NodeInputType Types::GetOperationInputType(Types::Operation operation, int inputIndex)
{
    operation_table_t *pOperation;
    for (pOperation = operationTable; pOperation->value != Operation::NONE; pOperation++)
    {
        if (pOperation->value == operation)
        {
            return pOperation->inputType[inputIndex];
        }
    }
    return NodeInputType::NONE;
}

/**
 * @brief   Get data type from string.
 * @param   data    String identifying data type.
 * @return  Data type.
 */
Types::DataType Types::GetDataType(string data)
{
	std::transform(data.begin(), data.end(), data.begin(), ::tolower);
    for (datatype_table_t *pDataType = dataTypeTable; pDataType->value != DataType::NONE; pDataType++)
    {
        if (data.compare(pDataType->format) == 0)
        {
            return pDataType->value;
        }
    }
    return DataType::NONE;
}

/**
 * @brief   Get data type with specified data representation.
 * @param   Bytes           Number of bytes in representation.
 * @param   Representation  Number representation.
 * @return  Data type.
 */
Types::DataType Types::GetDataType(size_t Bytes, representation_t Representation)
{
    for (datatype_table_t *pDataType = dataTypeTable; pDataType->value != DataType::NONE; pDataType++)
    {
        if (pDataType->bytes == Bytes && pDataType->representetion == Representation)
        {
            return pDataType->value;
        }
    }

    return DataType::NONE;
}

/**
 * @brief
 * @param   dataType
 * @return
 */
std::string Types::GetDataTypeStr(DataType dataType)
{
    for (datatype_table_t *pDataType = dataTypeTable; pDataType->value != DataType::NONE; pDataType++)
    {
        if (dataType == pDataType->value)
        {
            return pDataType->format;
        }
    }
    return NULL;
}

size_t Types::GetDataTypeBytes(DataType dataType)
{
    for (datatype_table_t *pDataType = dataTypeTable; pDataType->value != DataType::NONE; pDataType++)
    {
        if (dataType == pDataType->value)
        {
            return pDataType->bytes;
        }
    }
    return 0;
}

/**
 *
 * @param signalType
 * @return
 */
Types::SignalType Types::GetSignalType(std::string signalType)
{
    std::transform(signalType.begin(), signalType.end(), signalType.begin(), ::tolower);

    for (signaltype_table_t *pSignalType = signalTypeTable; pSignalType->value != SignalType::NONE; pSignalType++)
    {
        if (signalType.compare(pSignalType->format) == 0)
        {
            return pSignalType->value;
        }
    }

    return SignalType::NONE;
}

int Types::SetRegisterWidth(size_t Width)
{
    for (datatype_table_t *pDataType = dataTypeTable; pDataType->value != DataType::NONE; pDataType++)
    {
        if (pDataType->value == Types::DataType::POINTER)
        {
            pDataType->bytes = Width;
            return 0;
        }
    }
    return -1;
}
