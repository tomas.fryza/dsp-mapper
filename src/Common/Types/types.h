/**
 * @file    types.h
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Data types header file.
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <string>

class Types {
public:
    /**
     * Enumeration for the instruction identification
     */
    enum class Operation {
        NONE = 0,   ///< No operation
        ABS,        ///< Absolute value
        ADD,        ///< Addition
        DIV,        ///< Division
        CONST,      ///< Load constant
        CONSTH,     ///< Load constant high part
        CONSTL,     ///< Load constant low part
        MPY,        ///< Multiplication
        SUB,        ///< Subtraction
        LOADMEM,    ///< Load from memory
        STOREMEM,   ///< Store to memory
        MOVE        ///< Move registers
    };

    /**
     * Enumeration for the data type identification
     */
    enum class DataType {
        NONE = 0,   ///< Undefined data type
        INT8,       ///< 8-bit integer
        INT16,      ///< 16-bit integer
        INT32,      ///< 32-bit integer
        INT64,      ///< 64-bit integer
        FLOAT,      ///< Single-precision float
        DOUBLE,     ///< Double-precision float
        POINTER     ///< Pointer
    };

    /**
     * Enumeration for number representation
     */
    typedef enum {
        REPR_NONE = 0,  ///< Undefined data representation
        REPR_INTEGER,   ///< Integer representation
        REPR_FLOAT      ///< Floating-point representation
    } representation_t;

    /**
     * Enumeration for the data type identification
     */
    enum class SignalType {
        NONE = 0,       ///< Undefined signal type
        SIGNAL,         ///< General signal
        INPUT,          ///< Input signal
        OUTPUT,         ///< Output signal
        ALIAS           ///< Signal alias
    };

    enum class NodeInputType {
        NONE,
        SIGNAL,
        POINTER,
        CONSTANT,
        INDEX
    };

    static Operation GetOperation(std::string operation);
    static std::size_t GetOperationNumOfInputs(Operation operation);
    static std::string GetOperationStr(Operation operation);
    static NodeInputType GetOperationInputType(Operation operation, int inputIndex);
    static DataType GetDataType(std::string data);
    static DataType GetDataType(size_t NumBytes, representation_t Representation);
    static std::string GetDataTypeStr(DataType dataType);
    static size_t GetDataTypeBytes(DataType dataType);
    static SignalType GetSignalType(std::string signalType);
    static int SetRegisterWidth(size_t Width);

    /**
     * Table record for the operation description
     */
    typedef struct {
        Types::Operation value;     ///< Enumeration value
        const char* format;         ///< String for operation identification
        int numOfInputParams;       ///< Number of input parameters
        bool hasOutput;             ///< Result is stored in output
        NodeInputType inputType[3]; ///< Type of operation input
    } operation_table_t;

    /**
     * Table record for the data type description
     */
    typedef struct {
    	Types::DataType value;              ///< Enumeration value
        const char* format;                 ///< String for data type identification
        size_t bytes;                       ///< Number of bytes
        representation_t representetion;    ///< Number representation
    } datatype_table_t;

    /**
     * Table record for the signal types
     */
    typedef struct {
        Types::SignalType value;    ///< Enumeration value
        const char* format;         ///< String for signal type identification
    } signaltype_table_t;

    static operation_table_t operationTable[];      ///< Table with available operations
    static datatype_table_t dataTypeTable[];       ///< Table with available data types
    static signaltype_table_t signalTypeTable[];    ///< Table with available signal types
};

#endif /* TYPES_H_ */
