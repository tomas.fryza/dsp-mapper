/**
 * @file    version.h
 * @author  Roman Mego
 * @date    5.11.2015
 */

#ifndef VERSION_H_
#define VERSION_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

typedef struct {
    const uint8_t major;
    const uint8_t minor;
    const uint16_t build;
    const uint16_t revision;
} version_t;

version_t Version(void);

#ifdef __cplusplus
}
#endif

#endif /* VERSION_H_ */
