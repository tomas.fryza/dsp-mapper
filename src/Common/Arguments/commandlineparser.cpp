/**
 * @file    commandlineparser.cpp
 * @author  Roman Mego
 * @date    29.9.2015
 * @brief   Command line argument parser.
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "commandlineparser.h"

/**
 * @brief   Command line option constructor.
 * @param   ShortCommand    Short variant of the command starting with '-'.
 * @param   LongCommand     Long variant of the command starting with '--'.
 * @param   Description     Help description for the command.
 */
CommandLineParser::CommandLineOption::CommandLineOption(char ShortCommand, std::string LongCommand, std::string Description)
{
    this->LongCommand = LongCommand;
    this->ShortCommand = ShortCommand;
    this->Description = Description;
}

/**
 * @brief   Add option to the parser.
 * @param   Option  Reference to the option to add.
 */
void CommandLineParser::AddOption(CommandLineOption & Option)
{
    this->Options.push_back(&Option);
}

/**
 * @brief   Add positional argument to the parser.
 * @param   Name        Name of the argument to be displayed in usage.
 * @param   Description Description to display in the help.
 */
void CommandLineParser::AddPositionalArgument(std::string Name, std::string Description)
{
    PositionalArgument tmpArg;

    tmpArg.Name.assign(Name);
    tmpArg.Description.assign(Description);

    this->Positional.push_back(tmpArg);
}

/**
 * @brief   Show help.
 */
void CommandLineParser::ShowHelp()
{
    unsigned int optionWidth = 0;

    std::cout << this->AppCmd;

    for (std::list<PositionalArgument>::iterator it = this->Positional.begin(); it != this->Positional.end(); it++)
    {
        std::cout << "  " << it->Name;
        if (it->Name.size() > optionWidth)
        {
            optionWidth = it->Name.size();
        }
    }

    if (Options.size() > 0)
    {
        std::cout << " [-options]";

        for (std::list<CommandLineOption*>::iterator it = this->Options.begin(); it != this->Options.end(); it++)
        {
            if ((*it)->LongCommand.size() > optionWidth)
            {
                optionWidth = (*it)->LongCommand.size();
            }
        }
    }

    std::cout << std::endl;

    if (Positional.size() > 0)
    {
        std::cout << std::endl << "Arguments:" << std::endl;

        for (std::list<PositionalArgument>::iterator it = this->Positional.begin(); it != this->Positional.end(); it++)
        {
            std::cout << "  " <<  std::setw(optionWidth + 7) << std::left << it->Name << it->Description << std::endl;
        }
    }

    if (Options.size() > 0)
    {
        std::cout << std::endl << "Options:" << std::endl;
    }

    for (std::list<CommandLineOption*>::iterator it = this->Options.begin(); it != this->Options.end(); it++)
    {
        std::cout << "  -" << (*it)->ShortCommand << " --" << std::setw(optionWidth) << std::left << (*it)->LongCommand << "  " << (*it)->Description << std::endl;
    }

    exit(0);
}

/**
 * @brief   Parse the arguments.
 * @param   argc    Number of arguments.
 * @param   argv    Argument array.
 */
void CommandLineParser::Parse(int argc, char* argv[])
{
    bool parsePositional = true;

    this->AppCmd.assign(argv[0]);

    if (argc > 1)
    {
        std::string tmpHelpArg(argv[1]);
        if (tmpHelpArg.find("-h") == 0 ||
            tmpHelpArg.find("--help") == 0)
        {
            this->ShowHelp();
        }
    }

    for (int n = 1; n < argc; n++)
    {
        size_t startPos;
        std::string tmpArg(argv[n]);

        if (tmpArg.find("-") == 0)
        {
            startPos = 1;
        }
        else if (tmpArg.find("--") == 0)
        {
            startPos = 2;
        }
        else
        {
            startPos = 0;
        }

        if (startPos != 0)
        {
            bool knownOption = false;
            parsePositional = false;

            for (std::list<CommandLineOption*>::iterator it = this->Options.begin(); it != this->Options.end(); it++)
            {
                std::string tmpOpt;
                if (startPos == 1)
                {
                    tmpOpt = (*it)->ShortCommand;
                }
                else
                {
                    tmpOpt = (*it)->LongCommand;
                }

                if (tmpArg.find(tmpOpt) == startPos)
                {
                    ParsedArgument tmpParsed;
                    tmpParsed.Option = (*it);
                    tmpParsed.IsSet = true;

                    this->ParsedOptions.push_back(tmpParsed);
                    knownOption = true;

                    break;
                }
            }

            if (!knownOption)
            {
                this->ShowHelp();
            }
        }
        else if (!parsePositional)
        {
            this->ParsedOptions.back().Value = tmpArg;
            this->ParsedOptions.back().Values.push_back(tmpArg);
        }
        else
        {
            this->ParsedPositions.push_back(tmpArg);
        }
    }
}

/**
 * @brief   Get the status of the option.
 * @param   Option  Reference to the option.
 * @return  TRUE if option was set, FALSE otherwise.
 */
bool CommandLineParser::IsSet(CommandLineOption & Option)
{
    for (std::list<ParsedArgument>::iterator it = this->ParsedOptions.begin(); it != this->ParsedOptions.end(); it++)
    {
        if (it->Option == &Option)
        {
            return it->IsSet;
        }
    }

    return false;
}

/**
 * @brief   Get the value of the option.
 * @param   Option  Reference to the option.
 * @return  Value passed with the option.
 */
std::string CommandLineParser::Value(CommandLineOption & Option)
{
    for (std::list<ParsedArgument>::iterator it = this->ParsedOptions.begin(); it != this->ParsedOptions.end(); it++)
    {
        if (it->Option == &Option)
        {
            return it->Value;
        }
    }

    return std::string();
}

/**
 * @brief   Get the values list of the option.
 * @param   Option  Reference to the option.
 * @return  List of value passed with the option.
 */
std::list<std::string> CommandLineParser::Values(CommandLineOption & Option)
{
    for (std::list<ParsedArgument>::iterator it = this->ParsedOptions.begin(); it != this->ParsedOptions.end(); it++)
    {
        if (it->Option == &Option)
        {
            return it->Values;
        }
    }

    return std::list<std::string>();
}

/**
 * @brief   Get the position argument value.
 * @param   Position    Position of the argument.
 * @return  Value of the positional argument.
 */
std::string CommandLineParser::Value(int Position)
{
    std::list<std::string>::iterator it = ParsedPositions.begin();
    std::advance(it, Position);
    return *it;
}

/**
 * @brief   Get the list of the positional arguments.
 * @return  List of values passed in the positional arguments.
 */
std::list<std::string> CommandLineParser::Values()
{
    return this->ParsedPositions;
}

/**
 * @brief   Constructor for the parsed argument class.
 */
CommandLineParser::ParsedArgument::ParsedArgument()
{
    this->Value.clear();
    this->IsSet = false;
    this->Option = NULL;
}
