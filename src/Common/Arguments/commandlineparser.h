/**
 * @file    commandlineparser.h
 * @author  Roman Mego
 * @date    29.9.2015
 * @brief   Command line argument parser class definition.
 */

#ifndef COMMANDLINEPARSER_H_
#define COMMANDLINEPARSER_H_

#include <string>
#include <list>

/**
 * Command line parser for argc, argv arguments
 */
class CommandLineParser {
public:
    class CommandLineOption {
    public:
        char ShortCommand;          ///< Short variant of option
        std::string LongCommand;    ///< Long variant of option
        std::string Description;    ///< Description of the option
        CommandLineOption(char ShortCommand, std::string LongCommand, std::string Description);
    };

    void AddOption(CommandLineOption & Option);
    void AddPositionalArgument(std::string Name, std::string Description);
    void Parse(int argc, char* argv[]);
    bool IsSet(CommandLineOption & Option);
    std::string Value(CommandLineOption & Option);
    std::list<std::string> Values(CommandLineOption & Option);
    std::string Value(int Position);
    std::list<std::string> Values();
    void ShowHelp();

private:
    /**
     * Class describing parsed option from arguments
     */
    class ParsedArgument {
    public:
        CommandLineOption* Option;      ///< Reference to the recognized option
        bool IsSet;                     ///< Value indicating that the option was set in arguments
        std::string Value;              ///< Value set in the option
        std::list<std::string> Values;  ///< List of values set in the option
        ParsedArgument();
    };

    /**
     * Class describing positional argument
     */
    class PositionalArgument {
    public:
        std::string Name;               ///< Name of the positional argument
        std::string Description;        ///< Description of the positional argument
    };

    std::string AppCmd;

    std::list<PositionalArgument> Positional;   ///< List of possible positional arguments
    std::list<CommandLineOption*> Options;      ///< List of possible options

    std::list<std::string> ParsedPositions;     ///< Parsed positional arguments
    std::list<ParsedArgument> ParsedOptions;    ///< Parsed options
};

#endif /* COMMANDLINEPARSER_H_ */
