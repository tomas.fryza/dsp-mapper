/**
 * @file    fileinfo.cpp
 * @author  Roman Mego
 * @date    22.9.2016
 * @brief   Functions for retrieving informations from file name.
 */

#include "fileinfo.h"

/**
 * @brief   Gets extension of the file.
 * @param   FileName    Path to the file.
 * @return  Extension of the file.
 */
std::string FileInfo::GetExtension(std::string FileName)
{
    size_t dotIndex = FileName.find_last_of('.');
    size_t slashIndex = FileName.find_last_of("/\\");

    if ((dotIndex == std::string::npos) ||
        (slashIndex != std::string::npos &&
        slashIndex > dotIndex))
    {
        return "";
    }
    else
    {
        return FileName.substr(dotIndex);
    }
}

/**
 * @brief   Gets base name of the file.
 * @param   FileName    Path to the file.
 * @return  Base name of the file.
 */
std::string FileInfo::GetBaseFileName(std::string FileName)
{
    size_t dotIndex = FileName.find_last_of('.');
    size_t slashIndex = FileName.find_last_of("/\\");

    if (dotIndex != std::string::npos &&
        slashIndex != std::string::npos)
    {
        if (dotIndex > slashIndex)
        {
            return FileName.substr(slashIndex + 1, dotIndex - slashIndex - 1);
        }
        else
        {
            return FileName.substr(slashIndex + 1);
        }
    }
    else if (dotIndex == std::string::npos &&
             slashIndex != std::string::npos)
    {
        return FileName.substr(slashIndex + 1);
    }
    else if (dotIndex != std::string::npos &&
             slashIndex == std::string::npos)
    {
        return FileName.substr(0, dotIndex);
    }
    else
    {
        return FileName;
    }
}

/**
 * @brief   Gets directory from the file name.
 * @param   FileName    Path to the file.
 * @return  Directory to the file.
 */
std::string FileInfo::GetDirectory(std::string FileName)
{
    size_t slashIndex = FileName.find_last_of("/\\");

    if (slashIndex == std::string::npos)
    {
        return "";
    }
    else
    {
        return FileName.substr(0, slashIndex + 1);
    }
}
