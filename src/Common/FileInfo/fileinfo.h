/**
 * @file    fileinfo.h
 * @author  Roman Mego
 * @date    22.9.2016
 * @brief   Header file for retrieving informations from file name.
 */

#ifndef FILEINFO_H_
#define FILEINFO_H_

#include <string>

class FileInfo
{
public:
    static std::string GetExtension(std::string FileName);
    static std::string GetBaseFileName(std::string FileName);
    static std::string GetDirectory(std::string FileName);
};


#endif /* FILEINFO_H_ */
