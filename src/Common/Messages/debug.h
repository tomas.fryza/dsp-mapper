/**
 ***********************************************************************
 * @file    debug.h
 * @author  Roman Mego
 * @date    20.5.2015
 * @brief   Debug print macros.
 */


#ifndef DEBUG_H_
#define DEBUG_H_


#if defined(DEBUG) && defined(NDEBUG)
#error "Preprocessor symbols DEBUG and NDEBUG can not be defined at the same time."
#endif

#if !defined(DEBUG) && !defined(NDEBUG)
#   error "Preprocessor DEBUG or NDEBUG not defined."
#endif

#if defined(DEBUG)
#   if defined(__cplusplus)
#       include <iostream>
#       define DBG_PRINT(message)       std::cout << "DEBUG " << __FILE__ << ":" << __LINE__ << ": " << message << std::endl
#   else
#       include <stdio.h>
#       define DBG_PRINT(fmt, ...)  fprintf(stdout, "DEBUG %s:%d: " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#   endif
#else
#   define DBG_PRINT(fmt, ...)
#endif

#endif /* DEBUG_H_ */
