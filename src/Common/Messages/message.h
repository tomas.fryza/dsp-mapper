/**
 ***********************************************************************
 * @file    message.h
 * @author  Roman Mego
 * @date    31.12.2016
 * @brief   Message print macros.
 */


#ifndef MESSAGE_H_
#define MESSAGE_H_


#include <iostream>
#include <cstdlib>

#include "debug.h"

void Error(std::string Message);
void Error(std::string Message, std::string File);
void Error(std::string Message, std::string File, int Line);

void Warning(std::string Message);
void Warning(std::string Message, std::string File);
void Warning(std::string Message, std::string File, int Line);

#endif /* MESSAGE_H_ */
