/**
 ***********************************************************************
 * @file    message.cpp
 * @author  Roman Mego
 * @date    2.12.2017
 * @brief   Message output functions.
 */


#include <iostream>
#include <cstdlib>
#include <cassert>


/**
 ***********************************************************************
 * @brief   Shows error message.
 * @param   Message Message to show.
 */
void Error(std::string Message)
{
    std::cout << "\033[31mERROR: " << Message << "\033[0m" << std::endl;
    assert(false);
    std::exit(EXIT_FAILURE);
}


/**
 ***********************************************************************
 * @brief   Shows error message with file name where error occurred.
 * @param   Message Message to show.
 * @param   File    File name.
 */
void Error(std::string Message, std::string File)
{
    std::cout << "\033[31m" << File << ": ERROR: " << Message << "\033[0m" << std::endl;
    assert(false);
    std::exit(EXIT_FAILURE);
}


/**
 ***********************************************************************
 * @brief   Shows error message with file name and line where error occurred.
 * @param   Message Message to show.
 * @param   File    File name.
 * @param   Line    Line number.
 */
void Error(std::string Message, std::string File, int Line)
{
    std::cout << "\033[31m" << File << ":" << Line << ": ERROR: " << Message << "\033[0m" << std::endl;
    assert(false);
    std::exit(EXIT_FAILURE);
}


/**
 ***********************************************************************
 * @brief   Shows warning message.
 * @param   Message Message to show.
 */
void Warning(std::string Message)
{
    std::cout << "WARNING: " << Message << std::endl;
}


/**
 ***********************************************************************
 * @brief   Shows warning message with file name.
 * @param   Message Message to show.
 * @param   File    File name.
 */
void Warning(std::string Message, std::string File)
{
    std::cout << File << ": WARNING: " << Message << std::endl;
}


/**
 ***********************************************************************
 * @brief   Shows warning message with file name and line number.
 * @param   Message Message to show.
 * @param   File    File name.
 * @param   Line    Line number.
 */
void Warning(std::string Message, std::string File, int Line)
{
    std::cout << File << ":" << Line << ": WARNING: " << Message << std::endl;
}
