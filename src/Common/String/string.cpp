/**
 * @file    string.cpp
 * @author  Roman Mego
 * @date    20.5.2015
 * @brief   String utilities.
 */

#include "./string.h"

std::string whitespaces = " \t\f\v\n\r";

/**
 * @brief   Remove whitespaces from string on the left side.
 * @param   input   String to trim.
 * @return  Trimmed string from left.
 */
std::string StringTrimLeft(std::string input)
{
    return input.erase(0, input.find_first_not_of(whitespaces));
}

/**
 * @brief   Remove whitespaces from string on the right side.
 * @param   input   String to trim.
 * @return  Trimmed string from right.
 */
std::string StringTrimRight(std::string input)
{
    return input.erase(input.find_last_not_of(whitespaces) + 1, input.size());
}

/**
 * @brief   Remove whitespaces from string on the both sides.
 * @param   input   String to trim.
 * @return  Trimmed string
 */
std::string StringTrim(std::string input)
{
    return StringTrimLeft(StringTrimRight(input));
}

/**
 * @brief   Replace part of the string with new content.
 * @param   str         String where to find and replace substring.
 * @param   oldSubStr   Substring to replace.
 * @param   newSubStr   New content to insert.
 * @return  String with replaced content.
 */
std::string StringReplace(std::string str, std::string oldSubStr, std::string newSubStr)
{
    size_t pos = str.find(oldSubStr);

    if (pos != std::string::npos)
    {
        str.replace(pos, oldSubStr.size(), newSubStr);
    }

    return str;
}
