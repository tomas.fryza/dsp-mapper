/**
 * @file    string.h
 * @author  Roman Mego
 * @date    20.5.2015
 * @brief   String utilities header file.
 */

#ifndef STRING_H_
#define STRING_H_

#include <string>

std::string StringTrimLeft(std::string input);
std::string StringTrimRight(std::string input);
std::string StringTrim(std::string input);
std::string StringReplace(std::string original, std::string oldSubStr, std::string newSubStr);

#endif /* STRING_H_ */
