/**
 ***********************************************************************
 * @file    view.cpp
 * @author  Roman Mego
 * @date    22.11.2017
 * @brief   Functions for viewing architecture parts.
 */


#include "../../Common/CmdLine/cmdline.h"
#include "../global.h"
#include "view.h"


/**
 ***********************************************************************
 * @brief   View root console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int view(int argc, char *argv[])
{
    CommandLine cmdLine;

    if (argc == 1)
    {
        std::cout << "Architecture: " << arch.GetName() << std::endl;
        std::cout << "Data paths:   ";
        for (std::size_t n = 0; n < arch.GetDataPathCount(); n++)
        {
            std::cout << arch.GetDataPath(n)->GetName() << " ";
        }
        std::cout << std::endl;

        std::cout << "Cross paths:  ";
        for (std::size_t n = 0; n < arch.GetCrossPathCount(); n++)
        {
            std::cout << arch.GetCrossPath(n)->GetName() << " ";
        }
        std::cout << std::endl;

        std::cout << "Instructions: ";

//TODO print instructions in alphabetic order
        for (std::size_t n = 0; n < arch.GetInstructionCount(); n++)
        {
            std::cout << arch.GetInstruction(n)->GetName() << " ";
        }
        std::cout << std::endl;

        std::cout << " " << std::endl;
        std::cout << "Use \"help view\" to see detailed help for architecture overview" << std::endl;

        return 0;
    }

    cmdLine.AddCommand("datapath", view_datapath, "Usage: view datapath [data path name]", "Show the overview of the data path.");
    cmdLine.AddCommand("crosspath", view_crosspath, "Usage: view crosspath [cross path name]", "Show the overview of the cross path.");
    cmdLine.AddCommand("instruction", view_instruction, "Usage: view instruction [instruction name]", "Show the overview of the instruction.");
    cmdLine.AddCommand("functionalunit", view_functionalunit, "Usage: view [functional unit name]", "Show the overview of the functional unit.");
    cmdLine.AddCommand("reggroup", view_reggroup, "Usage: view reggroup [data path name] [register group name]", "Show the overview of the register group in the data path.");

    return cmdLine.ArgProcess(argc - 1, &argv[1]);
}


/**
 ***********************************************************************
 * @brief   Print data path detail to the console.
 * @param   DataPathName    Name of data path to print.
 */
void view_datapath_print(std::string DataPathName)
{
    Architecture::DataPath* dataPath = arch.GetDataPath(DataPathName);
    if (dataPath == NULL)
    {
        std::cout << "\033[31mData path " << DataPathName << " not found\033[0m\r\n";
        return;
    }
    std::cout << "Data path:        " << dataPath->GetName() << std::endl;

    std::cout << "Functional units: ";
    for (std::size_t n = 0; n < dataPath->GetFunctionalUnitCount(); n++)
    {
        std::cout << dataPath->GetFunctionalUnit(n)->GetName() << " ";
    }
    std::cout << std::endl;

    std::cout << "Registers:        ";
    for (std::size_t n = 0; n < dataPath->GetRegisterCount(); n++)
    {
        std::cout << dataPath->GetRegister(n)->GetName() << " ";
    }
    std::cout << std::endl;

    std::cout << "Register groups:  ";
    for (std::size_t n = 0; n < dataPath->GetRegisterGroupCount(); n++)
    {
        std::cout << dataPath->GetRegisterGroup(n)->GetName() << " ";
    }
    std::cout << std::endl;

    std::cout << "Data types:       ";
    Types::datatype_table_t *pDataType = Types::dataTypeTable;
    while (pDataType->format)
    {
        Architecture::RegisterGroup *group = dataPath->GetRegisterGroup(pDataType->value);
        std::cout << pDataType->format << ":";
        if (group == NULL)
        {
            std::cout << "NULL";
        }
        else
        {
            std::cout << group->GetName();
        }
        std::cout << " ";
        pDataType++;
    }
    std::cout << std::endl;
}


/**
 ***********************************************************************
 * @brief   View data path console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int view_datapath(int argc, char *argv[])
{
    if (argc == 1)
    {
        for (std::size_t n = 0; n < arch.GetDataPathCount(); n++)
        {
            view_datapath_print(arch.GetDataPath(n)->GetName());
        }
    }
    else
    {
        for (int n = 1; n < argc; n++)
        {
            view_datapath_print(argv[n]);
        }
    }
    return 0;
}


/**
 ***********************************************************************
 * @brief   Print cross path detail to the console.
 * @param   CrossPathName    Name of cross path to print.
 */
void view_crosspath_print(std::string CrossPathName)
{
    Architecture::CrossPath* crossPath = arch.GetCrossPath(CrossPathName);
    if (crossPath == NULL)
    {
        std::cout << "\033[31mCross path " << CrossPathName << " not found\033[0m" << std::endl;
        return;
    }

    std::cout << "Cross path:       " << crossPath->GetName() << std::endl;
    if (crossPath->GetSource() == NULL)
    {
        std::cout << "Source data path: NONE" << std::endl;
    }
    else
    {
        std::cout << "Source data path: " << crossPath->GetSource()->GetName() << std::endl;
    }
    std::cout << "Width:            " << crossPath->GetMaxWidth() << std::endl;
    std::cout << "Target operands:  " << crossPath->GetNumOfOperands() << std::endl;
}


/**
 ***********************************************************************
 * @brief   View cross path console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int view_crosspath(int argc, char *argv[])
{
    if (argc == 1)
    {
        for (std::size_t n = 0; n < arch.GetCrossPathCount(); n++)
        {
            view_crosspath_print(arch.GetCrossPath(n)->GetName());
        }
    }
    else
    {
        for (int n = 1; n < argc; n++)
        {
            view_crosspath_print(argv[n]);
        }
    }
    return 0;
}


/**
 ***********************************************************************
 * @brief   Print functional unit detail to the console.
 * @param   FunctionalUnitName  Name of the functional unit to print.
 */
void view_functionalunit_print(std::string FunctionalUnitName)
{
    Architecture::FunctionalUnit* unit = arch.GetFunctionalUnit(FunctionalUnitName);
    if (unit == NULL)
    {
        std::cout << "\033[31mFunctional unit " + FunctionalUnitName + " not found\033[0m" << std::endl;
        return;
    }

    std::cout << "Functional unit: " << unit->GetName() << std::endl;

    std::cout << "Cross path connection: ";
    for (size_t n = 0; n < unit->GetCrossPathInputCount(); n++)
    {
        Architecture::CrossPath* crosspath = unit->GetCrossPathInput(n);
        if (crosspath != NULL)
        {
            std::cout << crosspath->GetName();
        }
        else
        {
            std::cout << "none";
        }
        std::cout << " ";
    }
    std::cout << std::endl;
}


/**
 ***********************************************************************
 * @brief   View functional unit console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int view_functionalunit(int argc, char *argv[])
{
    if (argc == 1)
    {
        for (std::size_t n = 0; n < arch.GetDataPathCount(); n++)
        {
            Architecture::DataPath* dataPath = arch.GetDataPath(n);
            for (std::size_t i = 0; i < dataPath->GetFunctionalUnitCount(); i++)
            {
                view_functionalunit_print(dataPath->GetFunctionalUnit(i)->GetName());
            }
        }
    }
    else
    {
        for (int n = 1; n < argc; n++)
        {
            view_functionalunit_print(argv[n]);
        }
    }
    return 0;
}


/**
 ***********************************************************************
 * @brief   Print register group detail to the console.
 * @param   regGroup    Pointer to the register group to show.
 */
void view_reggroup_print(Architecture::RegisterGroup *regGroup)
{
    std::cout << "Data path:      " << regGroup->GetParentDataPath()->GetName() << std::endl;
    std::cout << "Register group: " << regGroup->GetName() << std::endl;

    for (std::size_t n = 0; n < regGroup->GetAssignmentCount(); n++)
    {
        Architecture::RegisterAssign *regAssign = regGroup->GetAssignment(n);
        std::cout << regAssign->GetName() << " ( ";
        for (std::size_t m = 0; m < regAssign->GetRegisterCount(); m++)
        {
            std::cout << regAssign->GetRegister(m)->GetName() << " ";
        }
        std::cout << ")" << std::endl;
    }
}


/**
 ***********************************************************************
 * @brief   View register group details console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int view_reggroup(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Can't find the data path " << argv[1] << "." << std::endl;
        return -1;
    }

    Architecture::RegisterGroup *group = path->GetRegisterGroup(argv[2]);
    if (group == NULL)
    {
        std::cout << "Can't find the register group " << argv[2] << "." << std::endl;
        return -1;
    }

    view_reggroup_print(group);

    return 0;
}


/**
 ***********************************************************************
 * @brief   Print instruction details to the console.
 * @param   InstructionName Name of the instruction to print.
 */
void view_instruction_print(std::string InstructionName)
{
    /* Convert given instruction name to uppercase */
    for(unsigned int i = 0; i < InstructionName.length(); i++)
        InstructionName[i] = std::toupper(InstructionName[i]);

    std::cout << " " << std::endl;
    Architecture::Instruction* instr = arch.GetInstruction(InstructionName);
    if (instr == NULL)
    {
        std::cout << "\033[31mInstruction " << InstructionName << " not found\033[0m" << std::endl;
        return;
    }

    std::cout << "Instruction:     " << instr->GetName() << std::endl;
    std::cout << "Format:          " << instr->GetFormat() << std::endl;
    std::cout << "Operation:       " << Types::GetOperationStr(instr->GetOperation()) << std::endl;
    std::cout << "Read cycles:     " << instr->GetReadCycles() << std::endl;
    std::cout << "Write cycles:    " << instr->GetWriteCycles() << std::endl;
    std::cout << "Total cycles:    " << instr->GetTotalCycles() << std::endl;

    std::cout << "Supported units: ";
    for (std::size_t n = 0; n < instr->GetFunctionalUnitCount(); n++)
    {
        std::cout << instr->GetFunctionalUnit(n)->GetName() << " ";
    }
    std::cout << std::endl;

    std::cout << "Data type:       ";
    for (std::size_t n = 0; n < instr->GetDataTypeCount(); n++)
    {
        std::cout << Types::GetDataTypeStr(instr->GetDataType(n)) << " ";
    }
    std::cout << std::endl;
}


/**
 ***********************************************************************
 * @brief   View instruction details console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int view_instruction(int argc, char *argv[])
{
    if (argc == 1)
    {
        for (std::size_t n = 0; n < arch.GetInstructionCount(); n++)
        {
            view_instruction_print(arch.GetInstruction(n)->GetName());
        }
    }
    else
    {
        for (int n = 1; n < argc; n++)
        {
            view_instruction_print(argv[n]);
        }
    }
    return 0;
}
