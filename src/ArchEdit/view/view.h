/**
 ***********************************************************************
 * @file    view.h
 * @author  Roman Mego
 * @date    3.5.2016
 * @brief   Header file for viewing architecture parts.
 */


#ifndef VIEW_H_
#define VIEW_H_


int view(int argc, char *argv[]);
int view_datapath(int argc, char *argv[]);
int view_crosspath(int argc, char *argv[]);
int view_functionalunit(int argc, char *argv[]);
int view_reggroup(int argc, char *argv[]);
int view_instruction(int argc, char *argv[]);

#endif /* VIEW_H_ */
