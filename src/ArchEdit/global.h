/**
 ***********************************************************************
 * @file    global.h
 * @author  Roman Mego
 * @date    10.9.2014
 * @brief   Header file for the global variables
 */


#ifndef GLOBAL_H_
#define GLOBAL_H_


#include "../Architecture/architecture.h"

extern Architecture arch;

#endif /* GLOBAL_H_ */
