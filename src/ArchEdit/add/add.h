/**
 ***********************************************************************
 * @file    add.h
 * @author  Roman Mego
 * @date    3.5.2016
 * @brief   Header file for adding architecture parts.
 */


#ifndef ADD_H_
#define ADD_H_


int add(int argc, char *argv[]);
int add_datapath(int argc, char *argv[]);
int add_crosspath(int argc, char *argv[]);
int add_functionalunit(int argc, char *argv[]);
int add_instruction(int argc, char *argv[]);
int add_register(int argc, char *argv[]);
int add_registergroup(int argc, char *argv[]);
int add_registergroupitem(int argc, char *argv[]);
int add_regassign(int argc, char *argv[]);

#endif /* ADD_H_ */
