/**
 * @file    add.cpp
 * @author  Roman Mego
 * @date    3.5.2016
 * @brief   Functions for adding architecture parts.
 */

#include "../../Common/CmdLine/cmdline.h"
#include "../global.h"
#include "add.h"

/**
 * @brief   Add root console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int add(int argc, char *argv[])
{
    CommandLine cmdLine;

    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    cmdLine.AddCommand("datapath",       add_datapath,          "Add data path to the architecture.",   "add datapath [data path name]");
    cmdLine.AddCommand("crosspath",      add_crosspath,         "Add cross path to the architecture.",  "add crosspath [cross path name]");
    cmdLine.AddCommand("functionalunit", add_functionalunit,    "Add functional unit to data path.",    "add functionalunit [data path name] [functional unit name]");
    cmdLine.AddCommand("instruction",    add_instruction,       "Add instruction.",                     "add instruction [instruction name]");
    cmdLine.AddCommand("register",       add_register,          "Add register to data path.",           "add register [data path name] [register]");
    cmdLine.AddCommand("reggroup",       add_registergroup,     "Add register group to data path.",     "add reggroup [data path name] [register group name]");
    cmdLine.AddCommand("reggroupitem",   add_registergroupitem, "Add item to register group.",          "add reggroupitem [data path name] [register group item]");
    cmdLine.AddCommand("regassign",      add_regassign,         "Add register assignment.",             "add regassign [data path name] [register group name] [register group item] [register list to assign...]");

    return cmdLine.ArgProcess(argc - 1, &argv[1]);
}

/**
 * @brief   Add data path console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int add_datapath(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    for (int n = 1; n < argc; n++)
    {
        if (arch.AddDataPath(argv[n]) == NULL)
        {
            std::cout << "Can't add data path " << argv[n] << "." << std::endl;
        }
        else
        {
            std::cout << "Data path " << argv[n] << " added." << std::endl;
        }
    }

    return 0;
}

/**
 * @brief   Add cross path console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int add_crosspath(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    for (int n = 1; n < argc; n++)
    {
        if (arch.AddCrossPath(argv[n]) == NULL)
        {
            std::cout << "Can't add cross path " << argv[n] << "." << std::endl;
        }
        else
        {
            std::cout << "Cross path " << argv[n] << " added." << std::endl;
        }
    }

    return 0;
}

/**
 * @brief   Add functional unit console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int add_functionalunit(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    for (int n = 2; n < argc; n++)
    {
        if (path->AddFunctionalUnit(argv[n]) == NULL)
        {
            std::cout << "Can't add functional unit " << argv[n] << "." << std::endl;
        }
        else
        {
            std::cout << "Functional unit " << argv[n] << " added to data path " << path->GetName() << "." << std::endl;
        }
    }

    return 0;
}

/**
 * @brief   Add instruction console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int add_instruction(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    for (int n = 1; n < argc; n++)
    {
        if (arch.AddInstruction(argv[n]) == NULL)
        {
            std::cout << "Can't add instruction " << argv[n] << "." << std::endl;
        }
        else
        {
            std::cout << "Instruction " << argv[n] << " added." << std::endl;
        }
    }

    return 0;
}

/**
 * @brief   Add register console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int add_register(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    for (int n = 2; n < argc; n++)
    {
        if (path->AddRegister(argv[n]) == NULL)
        {
            std::cout << "Can't add register " << argv[n] << "." << std::endl;
        }
        else
        {
            std::cout << "Register " << argv[n] << " added to data path " << path->GetName() << "." << std::endl;
        }
    }

    return 0;
}

/**
 * @brief   Add register group console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int add_registergroup(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    for (int n = 2; n < argc; n++)
    {
        if (path->AddRegisterGroup(argv[n]) == NULL)
        {
            std::cout << "Can't add register group " << argv[n] << "." << std::endl;
        }
        else
        {
            std::cout << "Register group " << argv[n] << " added to data path " << path->GetName() << "." << std::endl;
        }
    }

    return 0;
}

/**
 * @brief   Add register group item console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int add_registergroupitem(int argc, char *argv[])
{
    if (argc < 4)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    Architecture::RegisterGroup *group = path->GetRegisterGroup(argv[2]);
    if (group == NULL)
    {
        std::cout << "Register group " << argv[2] << " not found in data path " << path->GetName() << "." << std::endl;
        return -1;
    }

    for (int n = 3; n < argc; n++)
    {
        if (group->AddAssignment(argv[n]) == NULL)
        {
            std::cout << "Can't add register group item " << argv[n] << "." << std::endl;
        }
        else
        {
            std::cout << "Register group item " << argv[n] << " added to register group " << group->GetName() << "." << std::endl;
        }
    }

    return 0;
}

/**
 * @brief   Add register assignment.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int add_regassign(int argc, char *argv[])
{
    if (argc < 5)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    Architecture::RegisterGroup *group = path->GetRegisterGroup(argv[2]);
    if (group == NULL)
    {
        std::cout << "Register group " << argv[2] << " not found in data path " << path->GetName() << "." << std::endl;
        return -1;
    }

    Architecture::RegisterAssign *assign = group->GetAssignment(argv[3]);
    if (assign == NULL)
    {
        std::cout << "Assignment " << argv[3] << " not found." << std::endl;
        return -1;
    }

    for (int n = 4; n < argc; n++)
    {
        if (assign->AddRegister(argv[n]) == NULL)
        {
            std::cout << "Can't add register " << argv[n] << " to assignment " << assign->GetName() << "." << std::endl;
        }
        else
        {
            std::cout << "Register " << argv[n] << " added to assignment " << assign->GetName() << "." << std::endl;
        }
    }

    return 0;
}
