/**
 ***********************************************************************
 * @file    global.cpp
 * @author  Roman Mego
 * @date    10.9.2014
 * @brief   Global variables
 */

#include "../Architecture/architecture.h"

Architecture arch;  ///< Architecture description
