/**
 ***********************************************************************
 * @file    main.cpp
 * @author  Roman Mego
 * @date    22.11.2017
 * @brief   Main file of dspArchEdit application
 */


#include <cstdlib>
#include <iostream>
#include <string>
#include "../Common/CmdLine/cmdline.h"
#include "../Common/Arguments/commandlineparser.h"
#include "../Common/Version/version.h"
#include "global.h"
#include "view/view.h"
#include "add/add.h"
#include "set/set.h"
#include "remove/remove.h"

std::string fileName;


/**
 ***********************************************************************
 * @brief   Loads the architecture structure from the file.
 * @param   argc    Number of arguments from the console.
 * @param   argv    Array of arguments from the console.
 * @return  0 on success.
 */
int load(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        std::cout << "For usage, see \"help load\"" << std::endl;
        return -1;
    }

    if (arch.Load(argv[1]) == 0)
    {
        fileName.assign(argv[1]);
        std::cout << "Architecture has been loaded." << std::endl;
        std::cout << "Use \"view\" to show the overview of the architecture" << std::endl;
        return 0;
    }
    return -1;
}


/**
 ***********************************************************************
 * @brief   Save describing structure into the file.
 * @param   argc    Number of arguments from the console.
 * @param   argv    Array of arguments from the console.
 * @return  0 on success.
 */
int save(int argc, char *argv[])
{
    std::string tmpFileName;

    switch (argc)
    {
        case 1:
            if (fileName.empty())
            {
                std::cout << "Please, enter the file name: ";
                std::getline(std::cin, tmpFileName);
            }
            else
            {
                tmpFileName.assign(fileName);
            }
            break;
        case 2:
            tmpFileName.assign(argv[1]);
            break;
        default:
            std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
            std::cout << "For usage, see \"help save\"" << std::endl;
            return -1;
    }

    if (arch.Save(tmpFileName) == 0)
    {
        fileName.assign(tmpFileName);
        std::cout << "Architecture has been saved." << std::endl;
        return 0;
    }
    else
    {
        std::cout << "\033[31mError, architecture has not been saved\033[0m" << std::endl;
        return 0;
    }
    return -1;
}


/**
 ***********************************************************************
 * @brief   Renames the architecture in the defining structure.
 * @param   argc    Number of arguments from the console.
 * @param   argv    Array of arguments from the console.
 * @return  0 on success.
 */
int rename(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        std::cout << "For usage, see \"help rename\"" << std::endl;
        return -1;
    }
    arch.SetName(argv[1]);
    return 0;
}


/**
 ***********************************************************************
 * @brief   Terminates the application.
 * @param   argc    Number of arguments from the console.
 * @param   argv    Array of arguments from the console.
 * @return  Should not return back to the application.
 */
int exit(int argc, char *argv[])
{
    std::exit(EXIT_SUCCESS);
    return -1;
}


/**
 ***********************************************************************
 * @brief   Main function called at the start of the application.
 * @param   argc    Number of arguments from the console.
 * @param   argv    Array of arguments from the console.
 * @return  0 on success.
 */
int main(int argc, char *argv[])
{
    CommandLine cmdLine;
    CommandLineParser parser;
    parser.AddPositionalArgument("[file]", "Path to the architecture file.");

    std::cout << " " << std::endl;
    std::cout << "\033[1mDSP Mapper - Architecture Editor\033[0m" << std::endl;
    std::cout << "Version: " << (int)Version().major << "." << (int)Version().minor << "." << Version().build << "." << Version().revision << std::endl;
    std::cout << " " << std::endl;
    std::cout << "Use \"help\" or \"?\" to list available commands" << std::endl;
    std::cout << "Use \"help command\" or \"? command\" to see detailed help" << std::endl;

    parser.Parse(argc, argv);

    if (parser.Values().size() == 1)
    {
        if (arch.Load(parser.Value(0)) == 0)
        {
            fileName.assign(parser.Value(0));
        }
    }
    else if (parser.Values().size() > 1)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        parser.ShowHelp();
    }

    cmdLine.AddCommand("load", load, "Load architecture from json file.", "Usage:\r\n    load [file.json]\r\n\r\nExample:\r\n    load tms320c6678.json");
    cmdLine.AddCommand("view", view, "Show the overview of the architecture.", "Usage:\r\n    view [resource] [...]\r\n\r\nFor resource list, see \"view help\". If no resource is entered, command will show overview of the architecture.\r\n\r\nExamples:\r\n    view\r\n    view help\r\n    view instruction\r\n    view instruction ADD\r\n    view instruction ADD SUB");
    cmdLine.AddCommand("add", add, "Add resource to the architecture.", "Usage:\r\n    add [resource] [...]\r\n\r\nFor resource list, see \"add help\".\r\n\r\nExample:\r\n    add help\r\n    add instruction SHL");
    cmdLine.AddCommand("set", set, "Set parameter of the resource.", "Usage:\r\n    set [resource] [...]\r\n\r\nFor resource list, see \"set help\".");
    cmdLine.AddCommand("remove", rem, "Remove resource from the architecture.", "Usage:\r\n    remove [resource] [...]\r\n\r\nFor resource list, see \"remove help\".");
    cmdLine.AddCommand("rename", rename, "Rename the architecture.", "Usage:\r\n    rename [name]");
    cmdLine.AddCommand("save", save, "Save architecture to file.", "Usage:\r\n    save [file]");
    cmdLine.AddCommand("exit", exit, "Terminate the application.", "Usage:\r\n    exit");

    while (1)
    {
        std::string line;
        std::cout << " " << std::endl;
        std::cout << "\033[1m" << arch.GetName() << ">>\033[0m ";
        std::getline(std::cin, line);
        cmdLine.LineProcess(line);
    }

    return 0;
}
