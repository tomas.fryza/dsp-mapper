/**
 ***********************************************************************
 * @file    set.h
 * @author  Roman Mego
 * @date    30.11.2016
 * @brief   Header file for parameter functions implementation.
 */


#ifndef SOURCE_ARCHEDIT_SET_SET_H_
#define SOURCE_ARCHEDIT_SET_SET_H_


int set(int argc, char *argv[]);
int set_crosspath(int argc, char *argv[]);
int set_datatype(int argc, char *argv[]);
int set_functionalunit(int argc, char *argv[]);
int set_instruction(int argc, char *argv[]);
int set_regwidth(int argc, char *argv[]);

#endif /* SOURCE_ARCHEDIT_SET_SET_H_ */
