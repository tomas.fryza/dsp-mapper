/**
 ***********************************************************************
 * @file    instruction.h
 * @author  Roman Mego
 * @date    1.12.2016
 * @brief   Header file for setting instruction parameters.
 */


#ifndef INSTRUCTION_H_
#define INSTRUCTION_H_


int set_instruction_format(int argc, char *argv[]);
int set_instruction_operation(int argc, char *argv[]);
int set_instruction_readcycles(int argc, char *argv[]);
int set_instruction_writecycles(int argc, char *argv[]);
int set_instruction_totalcycles(int argc, char *argv[]);
int set_instruction_unit(int argc, char *argv[]);
int set_instruction_datatype(int argc, char *argv[]);

#endif /* INSTRUCTION_H_ */
