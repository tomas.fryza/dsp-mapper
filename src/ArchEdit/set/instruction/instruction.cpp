/**
 ***********************************************************************
 * @file    instruction.cpp
 * @author  Roman Mego
 * @date    21.11.2017
 * @brief   Functions for setting instruction parameters.
 */


#include "../../global.h"
#include <cstdlib>
#include "instruction.h"


/**
 ***********************************************************************
 * @brief   Set instruction format console function
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_instruction_format(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        std::cout << "For available commands, see \"set instruction help\"" << std::endl;
        return -1;
    }

    Architecture::Instruction *inst = arch.GetInstruction(argv[1]);
    if (inst == NULL)
    {
        std::cout << "\033[31mInstruction " << argv[1] << " not found\033[0m" << std::endl;
        return -1;
    }

    inst->SetFormat(argv[2]);

    return 0;
}


/**
 ***********************************************************************
 * @brief   Set instruction operation console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_instruction_operation(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        return -1;
    }

    Architecture::Instruction *inst = arch.GetInstruction(argv[1]);
    if (inst == NULL)
    {
        std::cout << "\033[31mInstruction " << argv[1] << " not found\033[0m" << std::endl;
        return -1;
    }

    if (inst->SetOperation(argv[2]))
    {
        std::cout << "\033[31mCan't set operation " << argv[2] << "\033[0m" << std::endl;
        return -1;
    }

    return 0;
}


/**
 ***********************************************************************
 * @brief   Set instruction read cycles console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_instruction_readcycles(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        return -1;
    }

    Architecture::Instruction *inst = arch.GetInstruction(argv[1]);
    if (inst == NULL)
    {
        std::cout << "\033[31mInstruction " << argv[1] << " not found\033[0m" << std::endl;
        return -1;
    }

    inst->SetReadCycles(atoi(argv[2]));

    return 0;
}


/**
 ***********************************************************************
 * @brief   Set instruction write cycles console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_instruction_writecycles(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        return -1;
    }

    Architecture::Instruction *inst = arch.GetInstruction(argv[1]);
    if (inst == NULL)
    {
        std::cout << "\033[31mInstruction " << argv[1] << " not found\033[0m" << std::endl;
        return -1;
    }

    inst->SetWriteCycles(atoi(argv[2]));

    return 0;
}


/**
 ***********************************************************************
 * @brief   Set instruction total cycles console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_instruction_totalcycles(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        return -1;
    }

    Architecture::Instruction *inst = arch.GetInstruction(argv[1]);
    if (inst == NULL)
    {
        std::cout << "\033[31mInstruction " << argv[1] << " not found\033[0m" << std::endl;
        return -1;
    }

    inst->SetTotalCycles(atoi(argv[2]));

    return 0;
}


/**
 ***********************************************************************
 * @brief   Set supported functional unit to instruction console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_instruction_unit(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        return -1;
    }

    Architecture::Instruction *inst = arch.GetInstruction(argv[1]);
    if (inst == NULL)
    {
        std::cout << "\033[31mInstruction " << argv[1] << " not found\033[0m" << std::endl;
        return -1;
    }

    inst->RemoveFunctionalUnit();

    for (int n = 2; n < argc; n++)
    {
        if (inst->AddFunctionalUnit(argv[n]) == NULL)
        {
            std::cout << "\033[31mCan't add functional unit " << argv[n] << " support\033[0m" << std::endl;
        }
        else
        {
            std::cout << "Functional unit " << argv[n] << " support added to instruction " << inst->GetName() << "." << std::endl;
        }
    }

    return 0;
}


/**
 ***********************************************************************
 * @brief   Set supported data type to instruction console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_instruction_datatype(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "\033[31mInvalid number of arguments\033[0m" << std::endl;
        return -1;
    }

    Architecture::Instruction *inst = arch.GetInstruction(argv[1]);
    if (inst == NULL)
    {
        std::cout << "\033[31mInstruction " << argv[1] << " not found\033[0m" << std::endl;
        return -1;
    }

    inst->RemoveDataType();

    for (int n = 2; n < argc; n++)
    {
        if (inst->AddDataType(argv[n]) != 0)
        {
            std::cout << "\033[31mCan't add data type " << argv[n] << " support\033[0m" << std::endl;
        }
        else
        {
            std::cout << "Data type " << argv[n] << " support added to instruction " << inst->GetName() << "." << std::endl;
        }
    }

    return 0;
}
