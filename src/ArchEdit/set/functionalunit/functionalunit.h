/**
 ***********************************************************************
 * @file    functionalunit.h
 * @author  Roman Mego
 * @date    1.12.2016
 * @brief   Header file for setting functional units parameters.
 */


#ifndef FUNCTIONALUNIT_H_
#define FUNCTIONALUNIT_H_


int set_functionalunit_crosspath(int argc, char *argv[]);

#endif /* FUNCTIONALUNIT_H_ */
