/**
 ***********************************************************************
 * @file    functionalunit.cpp
 * @author  Roman Mego
 * @date    1.11.2017
 * @brief   Console functions for setting functional units parameters.
 */


#include "functionalunit.h"
#include "../../global.h"
#include <cstdlib>


/**
 ***********************************************************************
 * @brief   Functional unit crosspath parameter console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_functionalunit_crosspath(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::FunctionalUnit* unit = arch.GetFunctionalUnit(argv[1]);
    if (unit == NULL)
    {
        std::cout << "Cannot find functional unit '" << argv[1] << "'";
    }

    unit->CleanCrossPath();

    std::list<Architecture::CrossPath*> crossPathList;

    for (int n = 2; n < argc; n++)
    {
        Architecture::CrossPath* crossPath = NULL;
        if (std::string(argv[n]).compare("NONE") != 0)
        {
            crossPath = arch.GetCrossPath(argv[n]);
            if (crossPath == NULL)
            {
                std::cout << "Cannot find cross path '" << argv[n] << "'.";
                return -1;
            }
        }
        crossPathList.push_back(crossPath);
    }

    unit->SetCrossPathInput(crossPathList);

    return 0;
}
