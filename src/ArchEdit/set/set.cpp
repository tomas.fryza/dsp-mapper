/**
 ***********************************************************************
 * @file    set.cpp
 * @author  Roman Mego
 * @date    29.10.2017
 * @brief   Set parameter functions implementation.
 */


#include "../../Common/CmdLine/cmdline.h"
#include "../global.h"
#include "set.h"

#include "crosspath/crosspath.h"
#include "functionalunit/functionalunit.h"
#include "instruction/instruction.h"


/**
 ***********************************************************************
 * @brief   Set root console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set(int argc, char *argv[])
{
    CommandLine cmdLine;

    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    cmdLine.AddCommand("datatype", set_datatype, "set datatype [data path] [data type] [register group]", "Set data type representation.");
    cmdLine.AddCommand("functionalunit", set_functionalunit, "set functionalunit [parameter] [...]", "Set functional unit parameter.");
    cmdLine.AddCommand("instruction", set_instruction, "set instruction [parameter] [instruction name] [operation]", "Set instruction parameter.");
    cmdLine.AddCommand("registerwidth", set_regwidth, "set regwidth [data path] [bytes]", "Set register width.");

    return cmdLine.ArgProcess(argc - 1, &argv[1]);
}


/**
 ***********************************************************************
 * @brief   Cross path parameter root console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int crosspathparam(int argc, char *argv[])
{
    CommandLine cmdLine;

    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    cmdLine.AddCommand("source", set_crosspath_source, "set crosspath source [cross path name] [source data path name]", "Set source of the cross path.");
    cmdLine.AddCommand("width", set_crosspath_width, "set crosspath width [cross path name] [width in number of registers]", "Set width of the cross path.");
    cmdLine.AddCommand("operands", set_crosspath_operands, "set crosspath operands [cross path name] [max. number of target operands]", "Set number of target operands of the cross path.");

    return cmdLine.ArgProcess(argc - 1, &argv[1]);
}


/**
 ***********************************************************************
 * @brief   Data type parameter console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_datatype(int argc, char *argv[])
{
    if (argc != 4)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath* path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Cannot find data path \"" << argv[1] << "\"" << std::endl;
        return -1;
    }

    Types::DataType type = Types::GetDataType(argv[2]);
    if (type == Types::DataType::NONE)
    {
        std::cout << "Cannot find data type \"" << argv[2] << "\"" << std::endl;
        return -1;
    }

    Architecture::RegisterGroup* registers = path->GetRegisterGroup(argv[3]);
    if (registers == NULL)
    {
        std::cout << "Cannot find register group \"" << argv[3] << "\"" << std::endl;
        return -1;
    }

    return path->ConnectDataTypeToGroup(type, registers);
}


/**
 ***********************************************************************
 * @brief   Functional unit parameter console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_functionalunit(int argc, char *argv[])
{
    CommandLine cmdLine;

    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    cmdLine.AddCommand("crosspath", set_functionalunit_crosspath, "set functionalunit crosspath [functional unit] [cross path list...]", "Set cross path input of the functional unit.");

    return cmdLine.ArgProcess(argc - 1, &argv[1]);
}


/**
 ***********************************************************************
 * @brief   Instruction parameter console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_instruction(int argc, char *argv[])
{
    CommandLine cmdLine;

    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    cmdLine.AddCommand("format", set_instruction_format, "set instruction format [instruction name] [format string]", "Set format of the instruction.");
    cmdLine.AddCommand("operation", set_instruction_operation, "set instruction operation [instruction name] [operation]", "Set operation of the instruction.");
    cmdLine.AddCommand("readcycles", set_instruction_readcycles, "set instruction readcycles [instruction name] [cycles]", "Set read cycles of the instruction.");
    cmdLine.AddCommand("writecycles", set_instruction_writecycles, "set instruction writecycles [instruction name] [cycles]", "Set read cycles of the instruction.");
    cmdLine.AddCommand("totalcycles", set_instruction_totalcycles, "set instruction total cycles [instruction name] [cycles]", "Set read cycles of the instruction.");
    cmdLine.AddCommand("unit", set_instruction_unit, "set instruction unit [instruction name] [unit name]", "Set functional unit support.");
    cmdLine.AddCommand("datatype", set_instruction_datatype, "set instruction datatype [instruction] [data type]", "Set data type support.");

    return cmdLine.ArgProcess(argc - 1, &argv[1]);
}


/**
 ***********************************************************************
 * @brief   Register width parameter console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_regwidth(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath* path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Cannot find data path \"" << argv[1] << "\"" << std::endl;
        return -1;
    }

    return Types::SetRegisterWidth(std::atoi(argv[2]));
}
