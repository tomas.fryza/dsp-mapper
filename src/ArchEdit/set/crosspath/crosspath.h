/**
 ***********************************************************************
 * @file    crosspath.h
 * @author  Roman Mego
 * @date    1.12.2016
 * @brief   Header file for setting cross path parameters.
 */


#ifndef CROSSPATH_H_
#define CROSSPATH_H_


int set_crosspath_source(int argc, char *argv[]);
int set_crosspath_width(int argc, char *argv[]);
int set_crosspath_operands(int argc, char *argv[]);

#endif /* CROSSPATH_H_ */
