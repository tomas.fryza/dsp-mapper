/**
 ***********************************************************************
 * @file    crosspath.cpp
 * @author  Roman Mego
 * @date    1.12.2016
 * @brief   Functions for setting cross path parameters.
 */


#include "crosspath.h"
#include "../../global.h"
#include <cstdlib>


/**
 ***********************************************************************
 * @brief   Set cross path source console function
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_crosspath_source(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::CrossPath *crossPath = arch.GetCrossPath(argv[1]);
    if (crossPath == NULL)
    {
        std::cout << "Cross path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    if (crossPath->SetSource(argv[2]))
    {
        std::cout << "Can't set data path " << argv[2] << " as source to cross path " << argv[1] << std::endl;
    }

    std::cout << "Data path " << argv[2] << " set as source to cross path " << argv[1] << std::endl;

    return 0;
}


/**
 ***********************************************************************
 * @brief   Set cross path width console function
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_crosspath_width(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::CrossPath *crossPath = arch.GetCrossPath(argv[1]);
    if (crossPath == NULL)
    {
        std::cout << "Cross path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    crossPath->SetMaxWidth(atoi(argv[2]));

    return 0;
}


/**
 ***********************************************************************
 * @brief   Set cross path number of target operands console function
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int set_crosspath_operands(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::CrossPath *crossPath = arch.GetCrossPath(argv[1]);
    if (crossPath == NULL)
    {
        std::cout << "Cross path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    crossPath->SetNumOfOperands(atoi(argv[2]));

    return 0;
}
