/**
 ***********************************************************************
 * @file    remove.h
 * @author  Roman Mego
 * @date    28.11.2016
 * @brief   Header file for remove architecture parts.
 */


#ifndef REMOVE_H_
#define REMOVE_H_


int rem(int argc, char *argv[]);
int rem_datapath(int argc, char *argv[]);
int rem_crosspath(int argc, char *argv[]);
int rem_functionalunit(int argc, char *argv[]);
int rem_instruction(int argc, char *argv[]);
int rem_register(int argc, char *argv[]);
int rem_reggroup(int argc, char *argv[]);
int rem_reggroupitem(int argc, char *argv[]);
int rem_regassign(int argc, char *argv[]);

#endif /* REMOVE_H_ */
