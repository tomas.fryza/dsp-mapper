/**
 * @file    remove.cpp
 * @author  Roman Mego
 * @date    28.11.2016
 * @brief   Functions for remove architecture parts.
 */

#include "../../Common/CmdLine/cmdline.h"
#include "../global.h"
#include "remove.h"

/**
 * @brief   Remove root console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int rem(int argc, char *argv[])
{
    CommandLine cmdLine;

    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    cmdLine.AddCommand("datapath",       rem_datapath,       "Remove data path from the architecture.",   "remove datapath [datapath list...]");
    cmdLine.AddCommand("functionalunit", rem_functionalunit, "Remove functional unit from data path.",    "remove functionalunit [data path name] [functional unit list...]");
    cmdLine.AddCommand("instruction",    rem_instruction,    "Remove instruction from the architecture.", "rempve instruction [instruction name...]");
    cmdLine.AddCommand("register",       rem_register,       "Remove register from data path.",           "remove register [data path name] [register list...]");
    cmdLine.AddCommand("reggroup",       rem_reggroup,       "Remove register group from data path.",     "remove reggroup [data path name] [register group list...]");
    cmdLine.AddCommand("reggroupitem",   rem_reggroupitem,   "Remove item from register group.",          "remove reggroupitem [data path name] [register group] [group item list...]");
    cmdLine.AddCommand("regassign",      rem_regassign,      "Remove register from the assignment.",      "remove regassign [data path] [register group] [group item] [register list...]");

    return cmdLine.ArgProcess(argc - 1, &argv[1]);
}

/**
 * @brief   Remove data path console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int rem_datapath(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    for (int n = 1; n < argc; n++)
    {
        if (arch.RemoveDataPath(argv[n]) != 0)
        {
            std::cout << "Can't remove data path " << argv[n] << "." << std::endl;
            return -1;
        }
        else
        {
            std::cout << "Data path " << argv[n] << " removed." << std::endl;
        }
    }

    return 0;
}

/**
 * @brief   Remove cross path console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int rem_crosspath(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    for (int n = 1; n < argc; n++)
    {
        if (arch.RemoveCrossPath(argv[n]) != 0)
        {
            std::cout << "Can't remove cross path " << argv[n] << "." << std::endl;
            return -1;
        }
        else
        {
            std::cout << "Cross path " << argv[n] << " removed." << std::endl;
        }
    }

    return 0;
}

/**
 * @brief   Remove functional unit console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int rem_functionalunit(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    for (int n = 2; n < argc; n++)
    {
        if (path->RemoveFunctionalUnit(argv[n]) != 0)
        {
            std::cout << "Can't remove functional unit " << argv[n] << "." << std::endl;
            return -1;
        }
        else
        {
            std::cout << "Functional unit " << argv[n] << " removed from data path " << path->GetName() << "." << std::endl;
        }
    }
    return 0;
}

/**
 * @brief   Remove instruction console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int rem_instruction(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    for (int n = 1; n < argc; n++)
    {
        arch.RemoveInstruction(argv[n]);
    }

    return 0;
}

/**
 * @brief   Remove register console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int rem_register(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    for (int n = 2; n < argc; n++)
    {
        if (path->RemoveRegister(argv[n]) != 0)
        {
            std::cout << "Can't remove register " << argv[n] << "." << std::endl;
            return -1;
        }
        else
        {
            std::cout << "Register " << argv[n] << " removed from data path " << path->GetName() << "." << std::endl;
        }
    }
    return 0;
}

/**
 * @brief   Remove register group console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int rem_reggroup(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    for (int n = 2; n < argc; n++)
    {
        if (path->RemoveRegisterGroup(argv[n]) != 0)
        {
            std::cout << "Can't remove register group " << argv[n] << "." << std::endl;
            return -1;
        }
        else
        {
            std::cout << "Register group " << argv[n] << " removed from data path " << path->GetName() << "." << std::endl;
        }
    }
    return 0;
}

/**
 * @brief   Remove register group item console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int rem_reggroupitem(int argc, char *argv[])
{
    if (argc < 4)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    Architecture::RegisterGroup *group = path->GetRegisterGroup(argv[2]);
    if (group == NULL)
    {
        std::cout << "Register group " << argv[2] << " not found in data path " << path->GetName() << "." << std::endl;
        return -1;
    }

    for (int n = 3; n < argc; n++)
    {
        if (group->RemoveAssignment(argv[n]) != 0)
        {
            std::cout << "Can't remove register group item " << argv[n] << "." << std::endl;
            return -1;
        }
        else
        {
            std::cout << "Register group item " << argv[n] << " removed from register group " << group->GetName() << "." << std::endl;
        }
    }
    return 0;
}

/**
 * @brief   Remove register assignment console function.
 * @param   argc    Number of arguments.
 * @param   argv    Pointer to the argument array.
 * @return  0 on success.
 */
int rem_regassign(int argc, char *argv[])
{
    if (argc < 5)
    {
        std::cout << "Invalid number of arguments." << std::endl;
        return -1;
    }

    Architecture::DataPath *path = arch.GetDataPath(argv[1]);
    if (path == NULL)
    {
        std::cout << "Data path " << argv[1] << " not found." << std::endl;
        return -1;
    }

    Architecture::RegisterGroup *group = path->GetRegisterGroup(argv[2]);
    if (group == NULL)
    {
        std::cout << "Register group " << argv[2] << " not found in data path " << path->GetName() << "." << std::endl;
        return -1;
    }

    Architecture::RegisterAssign *assign = group->GetAssignment(argv[3]);
    if (assign == NULL)
    {
        std::cout << "Assignment " << argv[3] << " not found." << std::endl;
        return -1;
    }

    for (int n = 4; n < argc; n++)
    {
        if (assign->RemoveRegister(argv[n]) != 0)
        {
            std::cout << "Can't remove register group " << argv[n] << " from the assignment." << std::endl;
            return -1;
        }
        else
        {
            std::cout << "Register group" << argv[n] << " removed from assignment " << assign->GetName() << "." << std::endl;
        }
    }
    return 0;
}
