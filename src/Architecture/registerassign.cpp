/**
 ***********************************************************************
 * @file    registerassign.cpp
 * @author  Roman Mego
 * @date    4.5.2015
 * @brief   Methods for register assignment class.
 */


#include "architecture.h"


/**
 ***********************************************************************
 * @brief   Constructor.
 * @param   Parent  Pointer to the parent register group.
 * @param   Name    Name of the assignment.
 */
Architecture::RegisterAssign::RegisterAssign(Architecture::RegisterGroup *Parent, std::string Name)
{
    this->Name.assign(Name);
    this->Parent = Parent;
}


/**
 ***********************************************************************
 * @brief   Get name of the register assignment.
 * @return  Name of the register assignment.
 */
std::string Architecture::RegisterAssign::GetName()
{
    return this->Name;
}


/**
 ***********************************************************************
 * @brief   Add register to the assignment.
 * @param   RegisterName    Name of the register to add.
 * @return  Pointer to the added register.
 */
Architecture::Register* Architecture::RegisterAssign::AddRegister(std::string RegisterName)
{
    if (this->GetRegister(RegisterName) != NULL) return NULL;

    Register* pReg = this->Parent->GetParentDataPath()->GetParentArchitecture()->GetRegister(RegisterName);

    if (pReg == NULL) return NULL;

    this->Registers.push_back(pReg);
    return pReg;
}


/**
 ***********************************************************************
 * @brief   Remove register from the assignment.
 * @param   RegisterName    Name of the register to remove.
 * @return  0 on success.
 */
int Architecture::RegisterAssign::RemoveRegister(std::string RegisterName)
{
    Architecture::Register *pReg = this->GetRegister(RegisterName);
    if (pReg == NULL) return -1;
    this->Registers.remove(pReg);
    return 0;
}


/**
 ***********************************************************************
 * @brief   Remove register from the assignment.
 * @param   RegisterPtr Pointer to the register to remove.
 * @return  0 on success.
 */
int Architecture::RegisterAssign::RemoveRegister(Architecture::Register *RegisterPtr)
{
    this->Registers.remove(RegisterPtr);
    return 0;
}


/**
 ***********************************************************************
 * @brief   Get register from the assignment.
 * @param   RegisterName    Name of the register to get.
 * @return  Pointer to the register.
 */
Architecture::Register* Architecture::RegisterAssign::GetRegister(std::string RegisterName)
{
    for (std::list<Register*>::iterator reg = this->Registers.begin(); reg != this->Registers.end(); reg++)
    {
        if (!RegisterName.compare((*reg)->GetName()))
        {
            return *reg;
        }
    }
    return NULL;
}


/**
 ***********************************************************************
 * @brief   Get register from the assignment.
 * @param   Index   Index of the register in the assignment.
 * @return  Pointer to the register.
 */
Architecture::Register* Architecture::RegisterAssign::GetRegister(std::size_t Index)
{
    if (Index >= this->GetRegisterCount()) return NULL;
    std::list<Architecture::Register*>::iterator it = this->Registers.begin();
    std::advance(it, Index);
    return *it;
}


/**
 ***********************************************************************
 * @brief   Get number of the registers in the assignment.
 * @return  Number of the registers in the assignment.
 */
std::size_t Architecture::RegisterAssign::GetRegisterCount()
{
    return this->Registers.size();
}
