/**
 ***********************************************************************
 * @file    architecture.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Methods for Architecture class.
 */


#include <fstream>
#include <string>
#include <cstring>
#include "architecture.h"
#include "../Common/Messages/message.h"
#include "../Common/cJSON/cJSON.h"
#include "../Common/Messages/debug.h"


/**
 ***********************************************************************
 * @brief Destructor
 */
Architecture::~Architecture()
{
    while (this->GetInstructionCount() > 0)
    {
        this->RemoveInstruction(this->GetInstruction(0)->GetName());
    }

    while (this->GetDataPathCount() > 0)
    {
        this->RemoveDataPath(this->GetDataPath(0)->GetName());
    }
}


/**
 ***********************************************************************
 * @brief  Load architecture from JSON file.
 * @param  FilePath Path to JSON file.
 * @return 0 on success.
 */
int Architecture::Load(std::string FilePath)
{
    std::ifstream file;
    std::string FileContent;
    cJSON *jsonStruct;

    this->Name.clear();
    this->DataPaths.clear();
    this->CrossPaths.clear();
    this->Instructions.clear();

    file.open(FilePath.c_str(), std::ios::in);

    /* Check if file exists */
    if (!file.is_open())
    {
        Error("Could not open the file \"" + FilePath + "\"");
    }

    /* Read file content */
    FileContent.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());

    /* Close file */
    file.close();

    /* Parse JSON file */
    jsonStruct = cJSON_Parse(FileContent.c_str());

    /* Name of the architecture */
    cJSON *jsonName = cJSON_GetObjectItem(jsonStruct, "name");              // Find name
    if (jsonName == NULL) Error("Could not find the architecture name");    // Name object not found
    this->Name.assign(jsonName->valuestring);                               // Copy name from JSON to structure

    /* Load register width */
    cJSON *jsonRegisterWidth = cJSON_GetObjectItem(jsonStruct, "regwidth");
    if (jsonRegisterWidth == NULL) Error("Can't get register width");
    if (Types::SetRegisterWidth(jsonRegisterWidth->valueint)) Error("Can't set register width");

    /* Add data paths without parameters */
    cJSON *jsonDataPathArray = cJSON_GetObjectItem(jsonStruct, "dataPath");
    if (jsonDataPathArray == NULL) Error("Could not find data paths in JSON file.");    // DataPath object not found

    /* Repeat for every datapath in architecture */
    for (int n = 0; n < cJSON_GetArraySize(jsonDataPathArray); n++)
    {
        cJSON *jsonDataPath = cJSON_GetArrayItem(jsonDataPathArray, n);         // Get item in data path array
        if (jsonDataPath == NULL) Error("Cannot get the data path " + std::to_string(n) + " from JSON file.");

        /* Get name of data path */
        cJSON *jsonDataPathName = cJSON_GetObjectItem(jsonDataPath, "name");    // Get name object
        if (jsonDataPathName == NULL) Error("Cannot find the name of the data path " + std::to_string(n));

        Architecture::DataPath* dataPath = this->AddDataPath(jsonDataPathName->valuestring);
        if (dataPath == NULL) Error("Cannot add data path '" + std::string(jsonDataPathName->valuestring) + "'.");
    }

    /* Add cross paths without parameters */
    cJSON *jsonCrossPathArray = cJSON_GetObjectItem(jsonStruct, "crossPath");
    if (jsonCrossPathArray == NULL) Error("Cannot find cross paths in JSON file.");

    /* Repeat for every cross path */
    for (int n = 0; n < cJSON_GetArraySize(jsonCrossPathArray); n++)
    {
        cJSON *jsonCrossPath = cJSON_GetArrayItem(jsonCrossPathArray, n);
        if (jsonCrossPath == NULL) Error("Cannot get the cross path " + std::to_string(n) + " from JSON file.");

        cJSON *jsonCrossPathName = cJSON_GetObjectItem(jsonCrossPath, "name");
        if (jsonCrossPathName == NULL) Error("Cannot get name of the cross path " + std::to_string(n));

        Architecture::CrossPath* crossPath = this->AddCrossPath(jsonCrossPathName->valuestring);
        if (crossPath == NULL) Error("Cannot add cross path " + std::string(jsonCrossPathName->valuestring) + " to architecture.");
    }

    /* Set parameters of the data paths */
    for (int n = 0; n < cJSON_GetArraySize(jsonDataPathArray); n++)
    {
        cJSON *jsonDataPath = cJSON_GetArrayItem(jsonDataPathArray, n);         // Get item in data path array
        if (jsonDataPath == NULL) Error("Cannot get the data path " + std::to_string(n) + " from JSON file.");

        /* Get name of data path */
        cJSON *jsonDataPathName = cJSON_GetObjectItem(jsonDataPath, "name");    // Get name object
        if (jsonDataPathName == NULL) Error("Cannot find the name of the data path " + std::to_string(n));

        /* Get data path from architecture */
        Architecture::DataPath* pDataPath = this->GetDataPath(jsonDataPathName->valuestring);
        if (pDataPath == NULL) Error("Cannot find data path '" + std::string(jsonDataPathName->valuestring) + "'");

        /* Functional Units */
        cJSON *jsonDataPathUnitArray = cJSON_GetObjectItem(jsonDataPath, "unit");   // Get data path unit array
        if (jsonDataPathUnitArray == NULL) Error("Can't find functional units of the data path " + std::to_string(n));

        /* Repeat for each unit in data path */
        for (int m = 0; m < cJSON_GetArraySize(jsonDataPathUnitArray); m++)
        {
            cJSON *jsonDataPathUnit = cJSON_GetArrayItem(jsonDataPathUnitArray, m); // Get unit from array
            if (jsonDataPathUnit == NULL) Error("Cannot get the functional unit " + std::to_string(m) + " of the data path " + pDataPath->GetName());

            std::string unitName = cJSON_GetObjectItem(jsonDataPathUnit, "name")->valuestring;
            std::list<Architecture::CrossPath*> crossPathList;
            Architecture::FunctionalUnit* unit = pDataPath->AddFunctionalUnit(unitName);

            if (unit == NULL) Error("Can't add functional unit \"" + unitName + "\" (probably multiply defined)");

            cJSON *jsonDataPathUnitCrossPathArray = cJSON_GetObjectItem(jsonDataPathUnit, "crosspath");
            for (int i = 0; i < cJSON_GetArraySize(jsonDataPathUnitCrossPathArray); i++)
            {
                std::string crossPathName = cJSON_GetArrayItem(jsonDataPathUnitCrossPathArray, i)->valuestring;
                Architecture::CrossPath* crossPath = NULL;

                if (crossPathName.compare("NONE") != 0)
                {
                    crossPath = this->GetCrossPath(crossPathName);
                    if (crossPath == NULL) Error("Can't find cross path " + crossPathName + " for functional unit " + unitName + ".");
                }

                crossPathList.push_back(crossPath);
            }

            unit->SetCrossPathInput(crossPathList);
        }

        /* Load registers */
        cJSON *jsonDataPathRegisterArray = cJSON_GetObjectItem(jsonDataPath, "register");
        if (jsonDataPathRegisterArray == NULL) Error("Can't find the registers of the data path " + std::to_string(n));

        /* For every register */
        for (int m = 0; m < cJSON_GetArraySize(jsonDataPathRegisterArray); m++)
        {
            cJSON *jsonDataPathRegister = cJSON_GetArrayItem(jsonDataPathRegisterArray, m);
            if (jsonDataPathRegister == NULL) Error("Can't get the register " + std::to_string(m) + " of the data path " + std::to_string(n));

            if (pDataPath->AddRegister(jsonDataPathRegister->valuestring) == NULL) Error("Can't add register \"" + std::string(jsonDataPathRegister->valuestring) + "\" (probably multiply defined)");
        }

        /* Load register groups */
        cJSON *jsonDataPathRegGroupArray = cJSON_GetObjectItem(jsonDataPath, "reggroup");
        if (jsonDataPathRegGroupArray == NULL) Error("Can't find register groups of the data path " + std::to_string(n));

        /* For every register group in data path */
        for (int m = 0; m < cJSON_GetArraySize(jsonDataPathRegGroupArray); m++)
        {
            cJSON *jsonDataPathRegGroup = cJSON_GetArrayItem(jsonDataPathRegGroupArray, m);
            if (jsonDataPathRegGroup == NULL) Error("Can't get the register group " + std::to_string(m) + " of the data path " + std::to_string(n));

            cJSON *jsonDataPathRegGroupName = cJSON_GetObjectItem(jsonDataPathRegGroup, "name");
            if (jsonDataPathRegGroupName == NULL) Error("Can't find the name of the register group " + std::to_string(m) + " of the data path " + std::to_string(n));

            cJSON *jsonDataPathRegGroupAssignArr = cJSON_GetObjectItem(jsonDataPathRegGroup, "assign");
            if (jsonDataPathRegGroupAssignArr == NULL) Error("Can't find the assignment of the register group " + std::to_string(m) + "of the data path " + std::to_string(n));

            RegisterGroup *pRegGroup = pDataPath->AddRegisterGroup(jsonDataPathRegGroupName->valuestring);
            if (pRegGroup == NULL) Error("Can't add register group \"" + std::string(jsonDataPathRegGroupName->valuestring) + "\" (probably multiply defined)");

            /* Repeat for every assignment in register group */
            for (int i = 0; i < cJSON_GetArraySize(jsonDataPathRegGroupAssignArr); i++)
            {
                cJSON *jsonDataPathRegGroupAssign = cJSON_GetArrayItem(jsonDataPathRegGroupAssignArr, i);
                if (jsonDataPathRegGroupAssign == NULL) Error("Can't get the assignment " + std::to_string(i) + " of the assignment group " + std::to_string(m) + " in the data path " + std::to_string(n));

                cJSON *jsonDataPathRegGroupAssignLabel = cJSON_GetObjectItem(jsonDataPathRegGroupAssign, "label");
                if (jsonDataPathRegGroupAssignLabel == NULL) Error("Can't find the label of the assignment " + std::to_string(i) + " of the assignment group " + std::to_string(m) + " in the data path " + std::to_string(n));

                cJSON *jsonDataPathRegGroupAssignRegArr = cJSON_GetObjectItem(jsonDataPathRegGroupAssign, "regs");
                if (jsonDataPathRegGroupAssignRegArr == NULL) Error("Can't find the registers in the assignment " + std::to_string(i) + " of the assignment group " + std::to_string(m) + " in the data path " + std::to_string(n));

                RegisterAssign *pAssign = pRegGroup->AddAssignment(jsonDataPathRegGroupAssignLabel->valuestring);
                if (pAssign == NULL) Error("Can't add register assignment \"" + std::string(jsonDataPathRegGroupAssignLabel->valuestring) + "\" (probably multiply defined)");

                /* For every register in assignment */
                for (int j = 0; j < cJSON_GetArraySize(jsonDataPathRegGroupAssignRegArr); j++)
                {
                    cJSON *jsonDataPathRegGroupAssignReg = cJSON_GetArrayItem(jsonDataPathRegGroupAssignRegArr, j);
                    if (jsonDataPathRegGroupAssignReg == NULL) Error("Can't get the register " + std::to_string(j) + " from assignment " + std::to_string(i) + " of the assignment group " + std::to_string(m) + " in the data path " + std::to_string(n));

                    if (pAssign->AddRegister(jsonDataPathRegGroupAssignReg->valuestring) == NULL) Error("Can't add register \"" + std::string(jsonDataPathRegGroupName->valuestring) + "\" to assignment (probably multiply defined or does not exist)");
                }
            }
        }

        /* Data types */
        cJSON *jsonDataPathDataType = cJSON_GetObjectItem(jsonDataPath, "datatype");
        if (jsonDataPathDataType == NULL) Error("Can't find data type assignments of the data path " + std::to_string(n));

        /* Connect data types to register groups */
        for (Types::datatype_table_t *pDataType = Types::dataTypeTable; pDataType->value != Types::DataType::NONE; pDataType++)
        {
            cJSON *jsonDataPathDataTypeVal = cJSON_GetObjectItem(jsonDataPathDataType, pDataType->format);
            if (jsonDataPathDataTypeVal != NULL)
            {
                pDataPath->ConnectDataTypeToGroup(pDataType->value, jsonDataPathDataTypeVal->valuestring);
            }
        }

    }

    /* Set parameters of the cross paths */
    for (int n = 0; n < cJSON_GetArraySize(jsonCrossPathArray); n++)
    {
        cJSON *jsonCrossPath = cJSON_GetArrayItem(jsonCrossPathArray, n);
        if (jsonCrossPath == NULL) Error("Cant't get the cross path " + std::to_string(n) + " from the list");

        cJSON *jsonCrossPathName = cJSON_GetObjectItem(jsonCrossPath, "name");
        if (jsonCrossPathName == NULL) Error("Can't find the name of the cross path " + std::to_string(n));

        CrossPath* pCrossPath = this->GetCrossPath(jsonCrossPathName->valuestring);
        if (pCrossPath == NULL) Error("Can't add the cross path " + std::to_string(n));

        cJSON *jsonCrossPathSource = cJSON_GetObjectItem(jsonCrossPath, "source");
        if (jsonCrossPathSource == NULL) Error("Can't get the source data path of the cross path " + std::to_string(n));
        if (pCrossPath->SetSource(jsonCrossPathSource->valuestring)) Error("Can't set source for cross path " + std::to_string(n));

        cJSON *jsonCrossPathWidth = cJSON_GetObjectItem(jsonCrossPath, "width");
        if (jsonCrossPathWidth == NULL) Error("Can't get the maximum width of the cross path " + std::to_string(n));
        if (pCrossPath->SetMaxWidth(jsonCrossPathWidth->valueint)) Error("Can't set width of the cross path " + std::to_string(n));

        cJSON *jsonCrossPathOperands = cJSON_GetObjectItem(jsonCrossPath, "operands");
        if (jsonCrossPathOperands == NULL) Error("Can't get the number of target operands of the cross path " + std::to_string(n));
        if (pCrossPath->SetNumOfOperands(jsonCrossPathOperands->valueint)) Error("Can't set the number of target operands for cross path " + std::to_string(n));
    }

    /* Instructions */
    cJSON *jsonInstructionArray = cJSON_GetObjectItem(jsonStruct, "instruction");
    if (jsonInstructionArray == NULL) Error("Can't find the instruction list");

    /* For every instruction */
    for (int n = 0; n < cJSON_GetArraySize(jsonInstructionArray); n++)
    {
        cJSON *jsonInstruction = cJSON_GetArrayItem(jsonInstructionArray, n);
        if (jsonInstruction == NULL) Error("Can't get the instruction " + std::to_string(n) + " from the list");

        cJSON *jsonInstructionName = cJSON_GetObjectItem(jsonInstruction, "name");
        if (jsonInstructionName == NULL) Error("Can't find the name of the instruction " + std::to_string(n));

        Instruction *pInstruction = this->AddInstruction(jsonInstructionName->valuestring);
        if (pInstruction == NULL) Error("");

        /* Instruction format */
        cJSON *jsonInstructionFormat = cJSON_GetObjectItem(jsonInstruction, "format");
        if (jsonInstructionFormat == NULL) Error("Can't find the format of the instruction " + std::to_string(n));
        pInstruction->SetFormat(jsonInstructionFormat->valuestring);

        /* Instruction operation type */
        cJSON *jsonInstructionOperation = cJSON_GetObjectItem(jsonInstruction, "type");
        if (jsonInstructionOperation == NULL) Error("Can't find the operation of the instruction " + std::to_string(n));
        if (pInstruction->SetOperation(jsonInstructionOperation->valuestring) != 0) Warning("Unsupported operation \"" + std::string(jsonInstructionOperation->valuestring) + "\" of instruction \"" + pInstruction->GetName() + "\"");

        /* Instruction read cycles */
        cJSON *jsonInstructionReadCycles = cJSON_GetObjectItem(jsonInstruction, "read");
        if (jsonInstructionReadCycles == NULL) Error("Can't find the number of read cycles of the instruction " + std::to_string(n));
        pInstruction->SetReadCycles(jsonInstructionReadCycles->valueint);

        /* Instruction write cycles */
        cJSON *jsonInstructionWriteCycles = cJSON_GetObjectItem(jsonInstruction, "write");
        if (jsonInstructionWriteCycles == NULL) Error("Can't find the number of write cycles of the instruction " + std::to_string(n));
        pInstruction->SetWriteCycles(jsonInstructionWriteCycles->valueint);

        /* Instruction total cycles */
        cJSON *jsonInstructionTotalCycles = cJSON_GetObjectItem(jsonInstruction, "total");
        if (jsonInstructionTotalCycles == NULL) Error("Can't find the number of total cycles of the instruction " + std::to_string(n));
        pInstruction->SetTotalCycles(jsonInstructionTotalCycles->valueint);

        /* Instruction data types */
        cJSON *jsonInstructionDataTypeArray = cJSON_GetObjectItem(jsonInstruction, "data");
        if (jsonInstruction == NULL) Error("Can't find supported data types of the instruction " + std::to_string(n));

        /* For every data type */
        for (int m = 0; m < cJSON_GetArraySize(jsonInstructionDataTypeArray); m++)
        {
            cJSON *jsonInstructionDataType = cJSON_GetArrayItem(jsonInstructionDataTypeArray, m);
            if (jsonInstructionDataType == NULL) Error("Can't get supported data type " + std::to_string(m) + " of the instruction " + std::to_string(n));
            if (pInstruction->AddDataType(jsonInstructionDataType->valuestring) != 0) Error("Can't add data type " + std::string(jsonInstructionDataType->valuestring) + " to instruction " + pInstruction->GetName());
        }

        /* Functional units */
        cJSON *jsonInstructionUnitArray = cJSON_GetObjectItem(jsonInstruction, "units");
        if (jsonInstructionUnitArray == NULL) Error("Can't find supported functional units of the instruction " + std::to_string(n));

        /* For every supported functional unit */
        for (int m = 0; m < cJSON_GetArraySize(jsonInstructionUnitArray); m++)
        {
            cJSON *jsonInstructionUnit = cJSON_GetArrayItem(jsonInstructionUnitArray, m);
            if (jsonInstructionUnit == NULL) Error("Can't get supported functional unit " + std::to_string(m) + " of the instruction " + std::to_string(n));
            if (pInstruction->AddFunctionalUnit(jsonInstructionUnit->valuestring) == NULL) Error("Can't add functional unit " + std::string(jsonInstructionUnit->valuestring) + " to instruction " + pInstruction->GetName());
        }
    }

    cJSON_Delete(jsonStruct);

    return 0;
}


/**
 ***********************************************************************
 * @brief  Save architecture structure to JSON file.
 * @param  FilePath File path.
 * @return 0 on success.
 */
int Architecture::Save(std::string FilePath)
{
    cJSON *jsonStruct = cJSON_CreateObject();
    cJSON_AddItemToObject(jsonStruct, "name", cJSON_CreateString(this->GetName().c_str()));

    cJSON *jsonDataPathArray = cJSON_CreateArray();
    for (std::size_t n = 0; n < this->GetDataPathCount(); n++)
    {
        Architecture::DataPath *dataPath = this->GetDataPath(n);

        cJSON *jsonDataPath = cJSON_CreateObject();
        cJSON_AddItemToObject(jsonDataPath, "name", cJSON_CreateString(dataPath->GetName().c_str()));

        cJSON *jsonDataPathUnitArray = cJSON_CreateArray();
        for (std::size_t m = 0; m < dataPath->GetFunctionalUnitCount(); m++)
        {
            cJSON *jsonDataPathFunctionalUnit = cJSON_CreateObject();
            cJSON *jsonDataPathFunctionalUnitCrossPathArray = cJSON_CreateArray();

            cJSON_AddItemToObject(jsonDataPathFunctionalUnit, "name", cJSON_CreateString(dataPath->GetFunctionalUnit(m)->GetName().c_str()));

            for (std::size_t i = 0; i < dataPath->GetFunctionalUnit(m)->GetCrossPathInputCount(); i++)
            {
                CrossPath* crossPath = dataPath->GetFunctionalUnit(m)->GetCrossPathInput(i);
                if (crossPath == NULL)
                {
                    cJSON_AddItemToArray(jsonDataPathFunctionalUnitCrossPathArray, cJSON_CreateString("NONE"));
                }
                else
                {
                    cJSON_AddItemToArray(jsonDataPathFunctionalUnitCrossPathArray, cJSON_CreateString(crossPath->GetName().c_str()));
                }
            }

            cJSON_AddItemToObject(jsonDataPathFunctionalUnit, "crosspath", jsonDataPathFunctionalUnitCrossPathArray);

            cJSON_AddItemToArray(jsonDataPathUnitArray, jsonDataPathFunctionalUnit);
        }
        cJSON_AddItemToObject(jsonDataPath, "unit", jsonDataPathUnitArray);

        cJSON *jsonDataPathRegisterArray = cJSON_CreateArray();
        for (std::size_t m = 0; m < dataPath->GetRegisterCount(); m++)
        {
            cJSON_AddItemToArray(jsonDataPathRegisterArray, cJSON_CreateString(dataPath->GetRegister(m)->GetName().c_str()));
        }
        cJSON_AddItemToObject(jsonDataPath, "register", jsonDataPathRegisterArray);

        cJSON *jsonDataPathRegGroupArray = cJSON_CreateArray();
        for (std::size_t m = 0; m < dataPath->GetRegisterGroupCount(); m++)
        {
            Architecture::RegisterGroup *group = dataPath->GetRegisterGroup(m);
            cJSON *jsonDataPathRegGroup = cJSON_CreateObject();
            cJSON_AddItemToObject(jsonDataPathRegGroup, "name", cJSON_CreateString(group->GetName().c_str()));

            cJSON *jsonRegAssignArray = cJSON_CreateArray();
            for (std::size_t i = 0; i < group->GetAssignmentCount(); i++)
            {
                Architecture::RegisterAssign *regAssign = group->GetAssignment(i);
                cJSON *jsonRegAssign = cJSON_CreateObject();

                cJSON_AddItemToObject(jsonRegAssign, "label", cJSON_CreateString(regAssign->GetName().c_str()));

                cJSON *jsonAssignedRegsArray = cJSON_CreateArray();
                for (std::size_t j = 0; j < regAssign->GetRegisterCount(); j++)
                {
                    cJSON_AddItemToArray(jsonAssignedRegsArray, cJSON_CreateString(regAssign->GetRegister(j)->GetName().c_str()));
                }
                cJSON_AddItemToObject(jsonRegAssign, "regs", jsonAssignedRegsArray);

                cJSON_AddItemToArray(jsonRegAssignArray, jsonRegAssign);
            }
            cJSON_AddItemToObject(jsonDataPathRegGroup, "assign", jsonRegAssignArray);

            cJSON_AddItemToArray(jsonDataPathRegGroupArray, jsonDataPathRegGroup);
        }
        cJSON_AddItemToObject(jsonDataPath, "reggroup", jsonDataPathRegGroupArray);

        cJSON *jsonDataType = cJSON_CreateObject();
        Types::datatype_table_t *pDataType = Types::dataTypeTable;
        while (pDataType->value != Types::DataType::NONE)
        {
            Architecture::RegisterGroup *regGroup = dataPath->GetRegisterGroup(pDataType->value);
            if (regGroup != NULL)
            {
                cJSON_AddItemToObject(jsonDataType, pDataType->format, cJSON_CreateString(regGroup->GetName().c_str()));
            }
            pDataType++;
        }
        cJSON_AddItemToObject(jsonDataPath, "datatype", jsonDataType);

        cJSON_AddItemToArray(jsonDataPathArray, jsonDataPath);
    }
    cJSON_AddItemToObject(jsonStruct, "dataPath", jsonDataPathArray);

    cJSON *jsonCrossPathArray = cJSON_CreateArray();
    for (std::size_t n = 0; n < this->GetCrossPathCount(); n++)
    {
        Architecture::CrossPath* crossPath = this->GetCrossPath(n);
        if (crossPath->GetSource() == NULL)
        {
            Warning("Cross path " + crossPath->GetName() + " skipped, no valid data path found.");
            continue;
        }

        cJSON *jsonCrossPath = cJSON_CreateObject();

        cJSON_AddItemToObject(jsonCrossPath, "name", cJSON_CreateString(crossPath->GetName().c_str()));
        cJSON_AddItemToObject(jsonCrossPath, "source", cJSON_CreateString(crossPath->GetSource()->GetName().c_str()));
        cJSON_AddItemToObject(jsonCrossPath, "width", cJSON_CreateNumber(crossPath->GetMaxWidth()));
        cJSON_AddItemToObject(jsonCrossPath, "operands", cJSON_CreateNumber(crossPath->GetNumOfOperands()));

        cJSON_AddItemToArray(jsonCrossPathArray, jsonCrossPath);
    }
    cJSON_AddItemToObject(jsonStruct, "crossPath", jsonCrossPathArray);

    cJSON *jsonInstructionArray = cJSON_CreateArray();
    for (std::size_t n = 0; n < this->GetInstructionCount(); n++)
    {
        Architecture::Instruction *instruction = this->GetInstruction(n);
        cJSON *jsonInstruction = cJSON_CreateObject();

        cJSON_AddItemToObject(jsonInstruction, "name", cJSON_CreateString(instruction->GetName().c_str()));
        cJSON_AddItemToObject(jsonInstruction, "format", cJSON_CreateString(instruction->GetFormat().c_str()));
        cJSON_AddItemToObject(jsonInstruction, "type", cJSON_CreateString(Types::GetOperationStr(instruction->GetOperation()).c_str()));

        cJSON *dataTypeArray = cJSON_CreateArray();
        for (std::size_t m = 0; m < instruction->GetDataTypeCount(); m++)
        {
            cJSON_AddItemToArray(dataTypeArray, cJSON_CreateString(Types::GetDataTypeStr(instruction->GetDataType(m)).c_str()));
        }
        cJSON_AddItemToObject(jsonInstruction, "data", dataTypeArray);

        cJSON *unitsArray = cJSON_CreateArray();
        for (std::size_t m = 0; m < instruction->GetFunctionalUnitCount(); m++)
        {
            cJSON_AddItemToArray(unitsArray, cJSON_CreateString(instruction->GetFunctionalUnit(m)->GetName().c_str()));
        }
        cJSON_AddItemToObject(jsonInstruction, "units", unitsArray);

        cJSON_AddItemToObject(jsonInstruction, "read", cJSON_CreateNumber(instruction->GetReadCycles()));
        cJSON_AddItemToObject(jsonInstruction, "write", cJSON_CreateNumber(instruction->GetWriteCycles()));
        cJSON_AddItemToObject(jsonInstruction, "total", cJSON_CreateNumber(instruction->GetTotalCycles()));

        cJSON_AddItemToArray(jsonInstructionArray, jsonInstruction);
    }
    cJSON_AddItemToObject(jsonStruct, "instruction", jsonInstructionArray);

    std::ofstream file;
    file.open(FilePath.c_str(), std::ios::out);
    if (!file.is_open())
    {
        return -1;
    }

    file << cJSON_Print(jsonStruct);
    file.close();

    return 0;
}


/**
 ***********************************************************************
 * @brief  Set name of the architecture.
 * @param  Name New name of the architecture.
 * @return 0 on success.
 */
int Architecture::SetName(std::string Name)
{
    this->Name.assign(Name);
    return 0;
}


/**
 ***********************************************************************
 * @brief  Get name of the architecture.
 * @return Name of the architecture.
 */
std::string Architecture::GetName()
{
    return this->Name;
}


/**
 ***********************************************************************
 * @brief  Add data path to the architecture.
 * @param  DataPathName Name of the new data path.
 * @return Pointer to the new data path or NULL.
 */
Architecture::DataPath* Architecture::AddDataPath(std::string DataPathName)
{
    /* Data path with this name exists */
    if (this->GetDataPath(DataPathName) != NULL) return NULL;

    this->DataPaths.push_back(DataPath(this, DataPathName));
    DataPath *pPath = &*(--this->DataPaths.end());

    return pPath;
}


/**
 ***********************************************************************
 * @brief  Remove data path from the architecture.
 * @param  DataPathName Name of the data path to remove.
 * @return 0 on success.
 */
int Architecture::RemoveDataPath(std::string DataPathName)
{
    for (std::list<Architecture::CrossPath>::iterator it = this->CrossPaths.begin(); it != this->CrossPaths.end(); it++)
    {
        if (!DataPathName.compare(it->GetSource()->GetName()))
        {
            this->CrossPaths.erase(it);
            break;
        }
    }

    for (std::list<Architecture::DataPath>::iterator it = this->DataPaths.begin(); it != this->DataPaths.end(); it++)
    {
        if (!DataPathName.compare(it->GetName()))
        {
            this->DataPaths.erase(it);
            return 0;
        }
    }
    return -1;
}


/**
 ***********************************************************************
 * @brief  Get number of the data paths in the architecture.
 * @return Number of data paths in the architecture.
 */
std::size_t Architecture::GetDataPathCount()
{
    return this->DataPaths.size();
}

Architecture::DataPath* Architecture::GetDataPath(std::string DataPathName)
{
    for (std::list<DataPath>::iterator path = this->DataPaths.begin(); path != this->DataPaths.end(); path++)
    {
        if (!DataPathName.compare(path->GetName()))
        {
            return &*path;
        }
    }
    return NULL;
}


/**
 ***********************************************************************
 * @brief  Get data path.
 * @param  Index    Index of the data path in the list.
 * @return Pointer to the data path or NULL.
 */
Architecture::DataPath* Architecture::GetDataPath(std::size_t Index)
{
    if (Index >= this->GetDataPathCount()) return NULL;
    std::list<Architecture::DataPath>::iterator it = this->DataPaths.begin();
    std::advance(it, Index);
    return &*it;
}


/**
 ***********************************************************************
 * @brief   Add cross path into the architecture.
 * @param   CrossPathName   Name of the cross path.
 * @return  Reference to the new cross path.
 */
Architecture::CrossPath* Architecture::AddCrossPath(std::string CrossPathName)
{
    if (this->GetCrossPath(CrossPathName) != NULL) return NULL;

    this->CrossPaths.push_back(CrossPath(this, CrossPathName));
    CrossPath* pCrossPath = &*(--this->CrossPaths.end());

    return pCrossPath;
}


/**
 ***********************************************************************
 * @brief   Remove cross path from the list.
 * @param   CrossPathName   Name of cross path to remove.
 * @return  0 on success.
 */
int Architecture::RemoveCrossPath(std::string CrossPathName)
{
    for (std::list<Architecture::CrossPath>::iterator it = this->CrossPaths.begin(); it != this->CrossPaths.end(); it++)
    {
        if (!CrossPathName.compare(it->GetName()))
        {
            this->CrossPaths.erase(it);
            return 0;
        }
    }
    return -1;
}


/**
 ***********************************************************************
 * @brief   Get number of items in cross path list.
 * @return  Number of cross paths.
 */
std::size_t Architecture::GetCrossPathCount()
{
    return this->CrossPaths.size();
}


/**
 ***********************************************************************
 * @brief   Get cross path.
 * @param   CrossPathName   Name of the cross path to get.
 * @return  Reference to the cross path.
 */
Architecture::CrossPath* Architecture::GetCrossPath(std::string CrossPathName)
{
    for (std::list<Architecture::CrossPath>::iterator it = this->CrossPaths.begin(); it != this->CrossPaths.end(); it++)
    {
        if (!CrossPathName.compare(it->GetName()))
        {
            return &*it;
        }
    }

    return NULL;
}


/**
 ***********************************************************************
 * @brief   Get cross path.
 * @param   Index   Position of the cross path in the list.
 * @return  Reference to the cross path.
 */
Architecture::CrossPath* Architecture::GetCrossPath(std::size_t Index)
{
    if (Index >= this->GetCrossPathCount()) return NULL;
    std::list<Architecture::CrossPath>::iterator it = this->CrossPaths.begin();
    std::advance(it, Index);
    return &*it;
}


/**
 ***********************************************************************
 * @brief  Get functional unit.
 * @param  UnitName Name of the functional unit.
 * @return Pointer to the functional unit or NULL.
 */
Architecture::FunctionalUnit* Architecture::GetFunctionalUnit(std::string UnitName)
{

    for (std::list<DataPath>::iterator path = this->DataPaths.begin(); path != this->DataPaths.end(); path++)
    {
        for (std::size_t m = 0; m < path->GetFunctionalUnitCount(); m++)
        {
            FunctionalUnit* unit = path->GetFunctionalUnit(m);
            if (!UnitName.compare(unit->GetName()))
            {
                return &*unit;
            }
        }
    }

    return NULL;
}


/**
 ***********************************************************************
 * @brief  Get register.
 * @param  RegisterName Name of the register.
 * @return Pointer to the register or NULL.
 */
Architecture::Register* Architecture::GetRegister(std::string RegisterName)
{
    for (std::list<DataPath>::iterator path = this->DataPaths.begin(); path != this->DataPaths.end(); path++)
    {
        for (std::size_t n = 0; n < path->GetRegisterCount(); n++)
        {
            Register* reg = path->GetRegister(n);
            if (!RegisterName.compare(reg->GetName()))
            {
                return reg;
            }
        }
    }
    return NULL;
}


/**
 ***********************************************************************
 * @brief  Add instruction to the architecture.
 * @param  InstructionName  Name of the instruction.
 * @return Pointer to the new instruction or NULL.
 */
Architecture::Instruction* Architecture::AddInstruction(std::string InstructionName)
{
    if (this->GetInstruction(InstructionName) != NULL) return NULL;

    this->Instructions.push_back(Instruction(this, InstructionName));
    Instruction* pInstr = &*(--this->Instructions.end());

    return pInstr;
}


/**
 ***********************************************************************
 * @brief  Remove instruction from the architecture.
 * @param  InstructionName  Name if the instruction.
 * @return 0 on success.
 */
int Architecture::RemoveInstruction(std::string InstructionName)
{
    for (std::list<Architecture::Instruction>::iterator it = this->Instructions.begin(); it != this->Instructions.end(); it++)
    {
        if (!InstructionName.compare(it->GetName()))
        {
            this->Instructions.erase(it);
            return 0;
        }
    }
    return -1;
}


/**
 ***********************************************************************
 * @brief  Get instruction.
 * @param  InstructionName  Name of the instruction.
 * @return Pointer to the instruction or NULL.
 */
Architecture::Instruction* Architecture::GetInstruction(std::string InstructionName)
{
    for (std::list<Instruction>::iterator instruction = this->Instructions.begin(); instruction != this->Instructions.end(); instruction++)
    {
        if (!InstructionName.compare(instruction->GetName()))
        {
            return &*instruction;
        }
    }
    return NULL;
}


/**
 ***********************************************************************
 * @brief  Get instruction.
 * @param  Index    Index of the instruction in the list.
 * @return Pointer to the instruction or NULL.
 */
Architecture::Instruction* Architecture::GetInstruction(std::size_t Index)
{
    if (Index >= this->GetInstructionCount()) return NULL;
    std::list<Architecture::Instruction>::iterator it = this->Instructions.begin();
    std::advance(it, Index);
    return &*it;
}


/**
 ***********************************************************************
 * @brief   Get list of instruction with selected attributes.
 * @param   Operation   Operation performed by instruction.
 * @return  List of available instructions.
 */
std::vector<Architecture::Instruction*> Architecture::GetInstruction(Types::Operation Operation)
{
    std::vector<Instruction*> retVal;

    for (std::list<Instruction>::iterator instruction = this->Instructions.begin(); instruction != this->Instructions.end(); instruction++)
    {
        if (instruction->GetOperation() == Operation)
        {
            retVal.push_back(&*instruction);
        }
    }

    return retVal;
}


/**
 ***********************************************************************
 * @brief   Get list of instruction with selected attributes.
 * @param   Operation   Operation performed by instruction.
 * @param   DataType    Supported data type by instruction.
 * @return  List of available instructions.
 */
std::vector<Architecture::Instruction*> Architecture::GetInstruction(Types::Operation Operation, Types::DataType DataType)
{
    std::vector<Instruction*> retVal;

    for (std::list<Instruction>::iterator instruction = this->Instructions.begin(); instruction != this->Instructions.end(); instruction++)
    {
        if (instruction->GetOperation() == Operation &&
            instruction->GetDataTypeSupport(DataType))
        {
            retVal.push_back(&*instruction);
        }
    }

    return retVal;
}


/**
 ***********************************************************************
 * @brief  Get number of the instructions in the architecture description.
 * @return Number of the instructions.
 */
std::size_t Architecture::GetInstructionCount()
{
    return this->Instructions.size();
}
