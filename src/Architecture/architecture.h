/**
 ***********************************************************************
 * @file    architecture.h
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Description of the architecture class.
 */

#ifndef ARCHITECTURE_H_
#define ARCHITECTURE_H_

#include <iostream>
#include <list>
#include <vector>
#include <map>
#include "../Common/Types/types.h"


class Architecture {
public:
    class DataPath;
    class CrossPath;
    class RegisterGroup;
    class Instruction;

private:
    std::string  Name;                      ///< Name of the architecture
    std::list<DataPath> DataPaths;          ///< List of data paths in the core
    std::list<CrossPath> CrossPaths;        ///< List of cross paths in the core
    std::list<Instruction> Instructions;    ///< List of supported instructions

public:
    /**
     * Class describing functional unit
     */
    class FunctionalUnit {
    private:
        std::string  Name;                      ///< Name of the functional unit
        DataPath *Parent;                       ///< Reference to the parent data path
        std::list<CrossPath*> CrossPathInput;   ///< List of input cross path connections
    public:
        FunctionalUnit(DataPath* Parent, std::string Name);
        ~FunctionalUnit();
        std::string GetName();
        Architecture::DataPath* GetParentDataPath();
        std::size_t GetCrossPathInputCount();
        Architecture::CrossPath* GetCrossPathInput(size_t Index);
        int SetCrossPathInput(std::list<CrossPath*> CrossPaths);
        int CleanCrossPath();
        int CleanCrossPath(size_t Index);
    };

    /**
     * Class describing register
     */
    class Register {
    private:
        std::string  Name;  ///< Name of the register
        DataPath *Parent;   ///< Reference to the parent data path
    public:
        Register(DataPath* Parent, std::string Name);
        ~Register();
        std::string GetName();
    };

    /**
     * Class describing register assignment
     */
    class RegisterAssign {
    private:
        std::string Name;               ///< Name of the register assignment
        RegisterGroup *Parent;          ///< Reference to the parent register group
        std::list<Register*> Registers; ///< List of references to the assigned registers
    public:
        RegisterAssign(RegisterGroup* Parent, std::string Name);
        std::string GetName();
        Register* AddRegister(std::string RegisterName);
        int RemoveRegister(std::string RegisterName);
        int RemoveRegister(Register *RegisterPtr);
        Register* GetRegister(std::string RegisterName);
        Register* GetRegister(std::size_t Index);
        std::size_t GetRegisterCount();
    };

    /**
     * Class describing register group
     */
    class RegisterGroup {
    private:
        std::string Name;                       ///< Name of the register group
        DataPath* Parent;                       ///< Reference to the parent data path
        std::list<RegisterAssign> Assignments;  ///< List of register assignments in the register group
    public:
        RegisterGroup(DataPath* Parent, std::string Name);
        ~RegisterGroup();
        std::string GetName();
        DataPath* GetParentDataPath();
        RegisterAssign* AddAssignment(std::string AssignmentName);
        int RemoveAssignment(std::string AssignmentName);
        RegisterAssign* GetAssignment(std::string AssignmentName);
        RegisterAssign* GetAssignment(std::size_t Index);
        std::size_t GetAssignmentCount();
    };

    /**
     * Class describing data path
     */
    class DataPath {
    private:
        Architecture* Parent;                           ///< Reference to the parent architecture
        std::string  Name;                              ///< Name of the data path
        std::list<FunctionalUnit> FunctionalUnits;      ///< List of functional units in the data path
        std::list<Register> Registers;                  ///< List of registers in the data path
        std::list<RegisterGroup> RegisterGroups;        ///< List of register groups in the data path
        std::map<std::string, RegisterGroup*> DataType; ///< Mapping of the register groups to the data type

    public:
        DataPath(Architecture *Parent, std::string Name);
        ~DataPath();
        Architecture* GetParentArchitecture();
        std::string GetName();
        FunctionalUnit* AddFunctionalUnit(std::string FunctionalUnitName);
        int RemoveFunctionalUnit(std::string FunctionalUnitName);
        FunctionalUnit* GetFunctionalUnit(std::size_t Index);
        std::size_t GetFunctionalUnitCount();
        Register* AddRegister(std::string RegisterName);
        int RemoveRegister(std::string RegisterName);
        Register* GetRegister(std::size_t Index);
        std::size_t GetRegisterCount();
        RegisterGroup* AddRegisterGroup(std::string GroupName);
        int RemoveRegisterGroup(std::string GroupName);
        std::size_t GetRegisterGroupCount();
        RegisterGroup* GetRegisterGroup(std::string GroupName);
        RegisterGroup* GetRegisterGroup(std::size_t Index);
        int ConnectDataTypeToGroup(Types::DataType DataType, std::string GroupName);
        int ConnectDataTypeToGroup(Types::DataType DataType, RegisterGroup* GroupName);
        int RemoveDataType(Types::DataType DataType);
        RegisterGroup* GetRegisterGroup(Types::DataType DataType);
    };

    /**
     * Class describing cross path
     */
    class CrossPath {
    private:
        Architecture* Parent;       ///< Reference to the parent architecture
        std::string Name;           ///< Name of the cross path
        DataPath* Source;           ///< Reference to the source data path
        std::size_t MaxWidth;       ///< Maximum transferred data width (number of registers)
        std::size_t NumOfOperands;  ///< Maximum number of target FU at once
    public:
        CrossPath(Architecture* Parent, std::string Name);
        ~CrossPath();
        std::string GetName();
        int SetSource(DataPath* Source);
        int SetSource(std::string DataPathName);
        DataPath* GetSource();
        int SetMaxWidth(std::size_t MaxWidth);
        size_t GetMaxWidth();
        int SetNumOfOperands(size_t NumOfOperands);
        std::size_t GetNumOfOperands();
    };

    /**
     * Class describing instruction
     */
    class Instruction {
    private:
        Architecture *Parent;                       ///< Reference to the parent architecture
        std::string Name;                           ///< Instruction name
        std::string Format;                         ///< Instruction format
        Types::Operation Operation;                 ///< Instruction operation
        int ReadCycles;                             ///< Number of cycles needed to read instruction and data
        int WriteCycles;                            ///< Number of cycles needed to write data
        int TotalCycles;                            ///< Total cycles needed to execute instruction
        std::list<Types::DataType> DataType;        ///< List of supported data type by the instruction
        std::list<FunctionalUnit*> FunctionalUnits; ///< List of references to the functional units which supports instruction

    public:
        Instruction(Architecture *Parent, std::string Name);
        std::string GetName();
        int SetFormat(std::string Format);
        std::string GetFormat();
        int SetOperation(Types::Operation Operation);
        int SetOperation(std::string Operation);
        Types::Operation GetOperation();
        int SetReadCycles(int Cycles);
        int GetReadCycles();
        int SetWriteCycles(int Cycles);
        int GetWriteCycles();
        int SetTotalCycles(int Cycles);
        int GetTotalCycles();
        Architecture::FunctionalUnit* AddFunctionalUnit(std::string FunctionalUnitName);
        int RemoveFunctionalUnit(std::string FunctionalUnitName);
        int RemoveFunctionalUnit(FunctionalUnit *FunctionalUnitPtr);
        int RemoveFunctionalUnit();
        Architecture::FunctionalUnit* GetFunctionalUnit(std::size_t Index);
        bool GetFunctionalUnitSupport(std::string FunctionalUnitName);
        bool GetFunctionalUnitSupport(Architecture::FunctionalUnit *FunctionalUnitPtr);
        std::size_t GetFunctionalUnitCount();
        int AddDataType(Types::DataType DataType);
        int AddDataType(std::string DataType);
        int RemoveDataType(std::string DataType);
        int RemoveDataType();
        Types::DataType GetDataType(std::size_t Index);
        bool GetDataTypeSupport(Types::DataType DataType);
        std::size_t GetDataTypeCount();
    };

    ~Architecture();
    int Load(std::string FilePath);
    int Save(std::string FilePath);
    int SetName(std::string Name);
    std::string GetName();
    DataPath* AddDataPath(std::string DataPathName);
    int RemoveDataPath(std::string DataPathName);
    size_t GetDataPathCount();
    DataPath* GetDataPath(std::string DataPathName);
    DataPath* GetDataPath(std::size_t Index);
    CrossPath* AddCrossPath(std::string CrossPathName);
    int RemoveCrossPath(std::string CrossPathName);
    std::size_t GetCrossPathCount();
    CrossPath* GetCrossPath(std::string CrossPathName);
    CrossPath* GetCrossPath(std::size_t Index);
    FunctionalUnit* GetFunctionalUnit(std::string UnitName);
    Register* GetRegister(std::string RegisterName);

    Instruction* AddInstruction(std::string InstructionName);
    int RemoveInstruction(std::string InstructionName);
    Instruction* GetInstruction(std::string InstructionName);
    Instruction* GetInstruction(std::size_t Index);
    std::vector<Instruction*> GetInstruction(Types::Operation Operation);
    std::vector<Instruction*> GetInstruction(Types::Operation Operation, Types::DataType DataType);
    std::size_t GetInstructionCount();
};

#endif /* ARCHITECTURE_H_ */
