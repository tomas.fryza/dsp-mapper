/**
 ***********************************************************************
 * @file    instruction.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Methods for Instruction class.
 */


#include "architecture.h"


/**
 ***********************************************************************
 * @brief Constructor.
 * @param Parent    Pointer to parent architecture structure.
 * @param Name      Name of the instruction.
 */
Architecture::Instruction::Instruction(Architecture *Parent, std::string Name)
{
    this->Parent = Parent;
    this->Name.assign(Name);

    this->Operation = Types::Operation::NONE;
    this->ReadCycles = -1;
    this->WriteCycles = -1;
    this->TotalCycles = -1;
}


/**
 ***********************************************************************
 * @brief  Get name of the instruction.
 * @return Name of the instruction.
 */
std::string Architecture::Instruction::GetName()
{
    return this->Name;
}


/**
 ***********************************************************************
 * @brief  Set format of the instruction.
 * @param  Format   Format of the instruction.
 * @return 0 on success.
 */
int Architecture::Instruction::SetFormat(std::string Format)
{
    this->Format.assign(Format);
    return 0;
}


/**
 ***********************************************************************
 * @brief  Get format of the instruction.
 * @return Format of the instruction.
 */
std::string Architecture::Instruction::GetFormat()
{
    return this->Format;
}


/**
 ***********************************************************************
 * @brief  Set instruction operation.
 * @param  Operation    Operation.
 * @return 0 on success.
 */
int Architecture::Instruction::SetOperation(Types::Operation Operation)
{
    this->Operation = Operation;
    return 0;
}


/**
 ***********************************************************************
 * @brief  Set instruction operation.
 * @param  Operation    Operation string.
 * @return 0 on success.
 */
int Architecture::Instruction::SetOperation(std::string Operation)
{
    Types::Operation operation = Types::GetOperation(Operation);
    this->Operation = operation;
    if (operation == Types::Operation::NONE) return -1;
    return 0;
}


/**
 ***********************************************************************
 * @brief  Get operation of the instruction.
 * @return Operation.
 */
Types::Operation Architecture::Instruction::GetOperation()
{
    return this->Operation;
}


/**
 ***********************************************************************
 * @brief  Set number of cycles needed to read input data from registers.
 * @param  Cycles   Number of cycles.
 * @return 0 on success.
 */
int Architecture::Instruction::SetReadCycles(int Cycles)
{
    this->ReadCycles = Cycles;
    return 0;
}


/**
 ***********************************************************************
 * @brief  Get number of cycles needed to read input data registers.
 * @return Number of cycles.
 */
int Architecture::Instruction::GetReadCycles()
{
    return this->ReadCycles;
}


/**
 ***********************************************************************
 * @brief  Set number of cycles needed to store output data to registers.
 * @param  Cycles   Number of cycles.
 * @return 0 on success.
 */
int Architecture::Instruction::SetWriteCycles(int Cycles)
{
    this->WriteCycles = Cycles;
    return 0;
}


/**
 ***********************************************************************
 * @brief  Get number of cycles needed to store output data to registers.
 * @return Number of cycles.
 */
int Architecture::Instruction::GetWriteCycles()
{
    return this->WriteCycles;
}

/**
 ***********************************************************************
 * @brief  Set number of cycles needed to execute the instruction.
 * @param  Cycles   Number of cycles.
 * @return 0 on success.
 */
int Architecture::Instruction::SetTotalCycles(int Cycles)
{
    this->TotalCycles = Cycles;
    return 0;
}


/**
 ***********************************************************************
 * @brief  Set number of cycles needed to execute the instruction.
 * @return Number of cycles.
 */
int Architecture::Instruction::GetTotalCycles()
{
    return this->TotalCycles;
}


/**
 ***********************************************************************
 * @brief  Add support for the functional unit.
 * @param  FunctionalUnitName   Name of the functional unit.
 * @return Pointer to the functional unit or NULL.
 */
Architecture::FunctionalUnit* Architecture::Instruction::AddFunctionalUnit(std::string FunctionalUnitName)
{
    FunctionalUnit *pUnit = this->Parent->GetFunctionalUnit(FunctionalUnitName);
    if (pUnit == NULL || this->GetFunctionalUnitSupport(pUnit)) return NULL;
    this->FunctionalUnits.push_back(pUnit);
    return pUnit;
}


/**
 ***********************************************************************
 * @brief  Remove support for the functional unit.
 * @param  FunctionalUnitName   Name of the functional unit.
 * @return 0 on success.
 */
int Architecture::Instruction::RemoveFunctionalUnit(std::string FunctionalUnitName)
{
    if (this->GetFunctionalUnitSupport(FunctionalUnitName) == false) return -1;
    Architecture::FunctionalUnit *pUnit = this->Parent->GetFunctionalUnit(FunctionalUnitName);
    if (pUnit == NULL) return -1;
    this->FunctionalUnits.remove(pUnit);
    return 0;
}


/**
 ***********************************************************************
 * @brief  Remove support for the functional unit.
 * @param  FunctionalUnitPtr    Pointer to the functional unit.
 * @return 0 on success.
 */
int Architecture::Instruction::RemoveFunctionalUnit(Architecture::FunctionalUnit *FunctionalUnitPtr)
{
    if (this->GetFunctionalUnitSupport(FunctionalUnitPtr) == false) return -1;
    this->FunctionalUnits.remove(FunctionalUnitPtr);
    return 0;
}


/**
 ***********************************************************************
 * @brief   Remove all functional unit support.
 * @return  0 on success.
 */
int Architecture::Instruction::RemoveFunctionalUnit()
{
    this->FunctionalUnits.clear();
    return 0;
}


/**
 ***********************************************************************
 * @brief   Get supported functional unit by index.
 * @param   Index   Index of the supported functional unit.
 * @return  Pointer to the supported functional unit.
 */
Architecture::FunctionalUnit* Architecture::Instruction::GetFunctionalUnit(std::size_t Index)
{
    if (Index >= this->GetFunctionalUnitCount()) return NULL;
    std::list<FunctionalUnit*>::iterator it = this->FunctionalUnits.begin();
    std::advance(it, Index);
    return *it;
}


/**
 ***********************************************************************
 * @brief   Check the instruction support by the functional unit.
 * @param   FunctionalUnitName  Name of the functional unit.
 * @return  TRUE if instruction is supported by the functional unit.
 */
bool Architecture::Instruction::GetFunctionalUnitSupport(std::string FunctionalUnitName)
{
    for (std::list<FunctionalUnit*>::iterator unit = this->FunctionalUnits.begin(); unit != this->FunctionalUnits.end(); unit++)
    {
        if (!FunctionalUnitName.compare((*unit)->GetName()))
        {
            return true;
        }
    }
    return false;
}


/**
 ***********************************************************************
 * @brief   Check the instruction support by the functional unit.
 * @param   FunctionalUnitPtr   Pointer to the functional unit.
 * @return  TRUE if instruction is supported by the functional unit.
 */
bool Architecture::Instruction::GetFunctionalUnitSupport(Architecture::FunctionalUnit *FunctionalUnitPtr)
{
    for (std::list<FunctionalUnit*>::iterator unit = this->FunctionalUnits.begin(); unit != this->FunctionalUnits.end(); unit++)
    {
        if (FunctionalUnitPtr == *unit)
        {
            return true;
        }
    }
    return false;
}


/**
 ***********************************************************************
 * @brief   Get the number of supported functional units.
 * @return  Number of supported functional units.
 */
std::size_t Architecture::Instruction::GetFunctionalUnitCount()
{
    return this->FunctionalUnits.size();
}


/**
 ***********************************************************************
 * @brief   Add supported data type to the instruction.
 * @param   DataType    Data type to add.
 * @return  0 on success.
 */
int Architecture::Instruction::AddDataType(Types::DataType DataType)
{
    if (this->GetDataTypeSupport(DataType)) return -1;
    this->DataType.push_back(DataType);
    return 0;
}


/**
 ***********************************************************************
 * @brief   Add supported data type to the instruction.
 * @param   DataType    Data type to add.
 * @return  0 on success.
 */
int Architecture::Instruction::AddDataType(std::string DataType)
{
    Types::DataType dataType = Types::GetDataType(DataType);
    if (dataType == Types::DataType::NONE || this->GetDataTypeSupport(dataType)) return -1;
    this->DataType.push_back(dataType);
    return 0;
}


/**
 ***********************************************************************
 * @brief   Remove data type from the support of the instruction.
 * @param   DataType    Data type to remove.
 * @return  0 on success.
 */
int Architecture::Instruction::RemoveDataType(std::string DataType)
{
    Types::DataType dataType = Types::GetDataType(DataType);
    if (dataType == Types::DataType::NONE || this->GetDataTypeSupport(dataType)) return -1;
    this->DataType.remove(dataType);
    return 0;
}


/**
 ***********************************************************************
 * @brief   Remove all data types from the support of the instruction.
 * @return  0 on success.
 */
int Architecture::Instruction::RemoveDataType()
{
    this->DataType.clear();
    return 0;
}


/**
 ***********************************************************************
 * @brief   Get supported data type.
 * @param   Index   Index of the supported data type.
 * @return  Supported data type at desired index.
 */
Types::DataType Architecture::Instruction::GetDataType(std::size_t Index)
{
    if (Index >= this->GetDataTypeCount()) return Types::DataType::NONE;
    std::list<Types::DataType>::iterator it = this->DataType.begin();
    std::advance(it, Index);
    return *it;
}


/**
 ***********************************************************************
 * @brief   Check the support of the data type.
 * @param   DataType    Data type to check.
 * @return  TRUE if data type is supported.
 */
bool Architecture::Instruction::GetDataTypeSupport(Types::DataType DataType)
{
    for (std::list<Types::DataType>::iterator dataType = this->DataType.begin(); dataType != this->DataType.end(); dataType++)
    {
        if (DataType == *dataType)
        {
            return true;
        }
    }
    return false;
}


/**
 ***********************************************************************
 * @brief   Get number of supported data types.
 * @return  Number of supported data types.
 */
std::size_t Architecture::Instruction::GetDataTypeCount()
{
    return this->DataType.size();
}
