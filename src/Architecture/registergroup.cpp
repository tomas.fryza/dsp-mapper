/**
 ***********************************************************************
 * @file    registergroup.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Methods for register group class.
 */


#include "architecture.h"


/**
 ***********************************************************************
 * @brief   Constructor.
 * @param   Parent  Pointer to the parent data path.
 * @param   Name    Name of the new register group.
 */
Architecture::RegisterGroup::RegisterGroup(Architecture::DataPath *Parent, std::string Name)
{
    this->Name.assign(Name);
    this->Parent = Parent;
}


/**
 ***********************************************************************
 * @brief   Destructor.
 */
Architecture::RegisterGroup::~RegisterGroup()
{
    Types::datatype_table_t *pType = Types::dataTypeTable;
    while (pType->value != Types::DataType::NONE)
    {
        Architecture::RegisterGroup *pGroup = this->Parent->GetRegisterGroup(pType->value);
        if (pGroup != NULL)
        {
            this->Parent->RemoveDataType(pType->value);
        }
        pType++;
    }
}


/**
 ***********************************************************************
 * @brief   Get name of the register group.
 * @return  Name of the register group.
 */
std::string Architecture::RegisterGroup::GetName()
{
    return this->Name;
}


/**
 ***********************************************************************
 * @brief   Get parent data path.
 * @return  Pointer to the parent data path.
 */
Architecture::DataPath* Architecture::RegisterGroup::GetParentDataPath()
{
    return this->Parent;
}


/**
 ***********************************************************************
 * @brief   Add assignment to the register group.
 * @param   AssignmentName  Assignment name.
 * @return  Pointer to the new assignment.
 */
Architecture::RegisterAssign* Architecture::RegisterGroup::AddAssignment(std::string AssignmentName)
{
    if (this->GetAssignment(AssignmentName) != NULL) return NULL;

    this->Assignments.push_back(RegisterAssign(this, AssignmentName));
    RegisterAssign* pAssign = &*(--this->Assignments.end());

    return pAssign;
}


/**
 ***********************************************************************
 * @brief   Remove assignment from the register group.
 * @param   AssignmentName  Name of the assignment to remove.
 * @return  0 on success.
 */
int Architecture::RegisterGroup::RemoveAssignment(std::string AssignmentName)
{
    for (std::list<Architecture::RegisterAssign>::iterator it = this->Assignments.begin(); it != this->Assignments.end(); it++)
    {
        if (!AssignmentName.compare(it->GetName()))
        {
            this->Assignments.erase(it);
            return 0;
        }
    }
    return -1;
}


/**
 ***********************************************************************
 * @brief   Get assignment in the register group.
 * @param   AssignmentName  Name of the assignment.
 * @return  Pointer to the assignment.
 */
Architecture::RegisterAssign* Architecture::RegisterGroup::GetAssignment(std::string AssignmentName)
{
    for (std::list<RegisterAssign>::iterator assign = this->Assignments.begin(); assign != this->Assignments.end(); assign++)
    {
        if (!AssignmentName.compare(assign->GetName()))
        {
            return &*assign;
        }
    }
    return NULL;
}


/**
 ***********************************************************************
 * @brief   Get assignment in the register group.
 * @param   Index   Index of the assignment in the group.
 * @return  Pointer to the assignment.
 */
Architecture::RegisterAssign* Architecture::RegisterGroup::GetAssignment(std::size_t Index)
{
    if (Index >= this->GetAssignmentCount()) return NULL;
    std::list<Architecture::RegisterAssign>::iterator it = this->Assignments.begin();
    std::advance(it, Index);
    return &*it;
}


/**
 ***********************************************************************
 * @brief   Get number of register assignments in the group.
 * @return  Number of the assignments.
 */
std::size_t Architecture::RegisterGroup::GetAssignmentCount()
{
    return this->Assignments.size();
}
