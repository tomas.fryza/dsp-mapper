/**
 ***********************************************************************
 * @file    register.cpp
 * @author  Roman Mego
 * @date    28.4.2015
 * @brief   Methods for Register class.
 */


#include "architecture.h"


/**
 ***********************************************************************
 * @brief   Constructor
 * @param   Parent  Pointer to the parent architecture structure.
 * @param   Name    Name of the register.
 */
Architecture::Register::Register(Architecture::DataPath *Parent, std::string Name)
{
    this->Parent = Parent;
    this->Name.assign(Name);
}


/**
 ***********************************************************************
 * @brief   Destructor.
 */
Architecture::Register::~Register()
{
    for (std::size_t n = 0; n < this->Parent->GetRegisterGroupCount(); n++)
    {
        Architecture::RegisterGroup *pGroup = this->Parent->GetRegisterGroup(n);
        for (std::size_t m = 0; m < pGroup->GetAssignmentCount(); m++)
        {
            pGroup->GetAssignment(m)->RemoveRegister(this);
        }
    }
}


/**
 ***********************************************************************
 * @brief   Get name of the register.
 * @return  Name of the register.
 */
std::string Architecture::Register::GetName()
{
    return this->Name;
}
