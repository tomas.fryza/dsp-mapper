/**
 ***********************************************************************
 * @file    functionalunit.cpp
 * @author  Roman Mego
 * @date    24.10.2016
 * @brief   Methods for FunctionalUnit class.
 */


#include "architecture.h"


/**
 ***********************************************************************
 * @brief Constructor.
 * @param Parent    Pointer to the  parent data path.
 * @param Name      Name of the functional unit.
 */
Architecture::FunctionalUnit::FunctionalUnit(Architecture::DataPath* Parent, std::string Name)
{
    this->Parent = Parent;
    this->Name.assign(Name);
}


/**
 ***********************************************************************
 * @brief Destructor.
 */
Architecture::FunctionalUnit::~FunctionalUnit()
{
    Architecture *pArch = this->Parent->GetParentArchitecture();

    for (std::size_t n = 0; n < pArch->GetInstructionCount(); n++)
    {
        pArch->GetInstruction(n)->RemoveFunctionalUnit(this);
    }
}


/**
 ***********************************************************************
 * @brief  Get name of the functional unit.
 * @return Name of the functional unit.
 */
std::string Architecture::FunctionalUnit::GetName()
{
    return this->Name;
}


/**
 ***********************************************************************
 * @brief  Get parent data path.
 * @return Pointer to the parent data path.
 */
Architecture::DataPath* Architecture::FunctionalUnit::GetParentDataPath()
{
    return this->Parent;
}


/**
 ***********************************************************************
 * @brief   Get number of cross path inputs.
 * @return  Number of cross path inputs.
 */
std::size_t Architecture::FunctionalUnit::GetCrossPathInputCount()
{
    return this->CrossPathInput.size();
}


/**
 ***********************************************************************
 * @brief   Get cross path input.
 * @param   Index   Index of input.
 * @return  Cross path on selected input.
 */
Architecture::CrossPath* Architecture::FunctionalUnit::GetCrossPathInput(size_t Index)
{
    if (Index >= this->GetCrossPathInputCount()) return NULL;
    std::list<Architecture::CrossPath*>::iterator it = this->CrossPathInput.begin();
    std::advance(it, Index);
    return *it;
}


/**
 ***********************************************************************
 * @brief   Sets cross path connection on functional unit input.
 * @param   CrossPaths  List of cross paths to set on input.
 * @return  0 on success.
 */
int Architecture::FunctionalUnit::SetCrossPathInput(std::list<CrossPath*> CrossPaths)
{
    if (this->CleanCrossPath())
    {
        return -1;
    }

    for (std::list<CrossPath*>::iterator it = CrossPaths.begin(); it != CrossPaths.end(); it++)
    {
        this->CrossPathInput.push_back(*it);
    }

    return 0;
}


/**
 ***********************************************************************
 * @brief   Clean all cross path connections on functional unit.
 * @return  0 on success.
 */
int Architecture::FunctionalUnit::CleanCrossPath()
{
    this->CrossPathInput.clear();
    return 0;
}


/**
 ***********************************************************************
 * @brief   Clean cross path input.
 * @param   Index   Cross path input to clean.
 * @return  0 on success.
 */
int Architecture::FunctionalUnit::CleanCrossPath(size_t Index)
{
    if (Index >= this->GetCrossPathInputCount()) return -1;
    std::list<Architecture::CrossPath*>::iterator it = this->CrossPathInput.begin();
    std::advance(it, Index);
    *it = NULL;
    return 0;
}
