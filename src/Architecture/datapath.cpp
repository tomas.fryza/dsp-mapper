/**
 * @file    datapath.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Methods for DataPath class.
 */


#include "architecture.h"


/**
 ***********************************************************************
 * @brief Constructor.
 * @param Parent    Pointer to the parent class.
 * @param Name      Name of the data path.
 */
Architecture::DataPath::DataPath(Architecture *Parent, std::string Name)
{
    this->Parent = Parent;
    this->Name.assign(Name);
}


/**
 ***********************************************************************
 * @brief Destructor.
 */
Architecture::DataPath::~DataPath()
{
    while (this->GetRegisterGroupCount())
    {
        this->RemoveRegisterGroup(this->GetRegisterGroup(0)->GetName());
    }

    while (this->GetFunctionalUnitCount() > 0)
    {
        this->RemoveFunctionalUnit(this->GetFunctionalUnit(0)->GetName());
    }

    while (this->GetRegisterCount())
    {
        this->RemoveRegister(this->GetRegister(0)->GetName());
    }
}


/**
 ***********************************************************************
 * @brief  Get parent.
 * @return Pointer to the parent architecture.
 */
Architecture* Architecture::DataPath::GetParentArchitecture()
{
    return this->Parent;
}


/**
 ***********************************************************************
 * @brief  Get name of the data path.
 * @return Name of the data path.
 */
std::string Architecture::DataPath::GetName()
{
    return this->Name;
}


/**
 ***********************************************************************
 * @brief  Add functional unit to data path.
 * @param  FunctionalUnitName   Name of the new functional unit.
 * @return Pointer to the new functional unit or NULL.
 */
Architecture::FunctionalUnit* Architecture::DataPath::AddFunctionalUnit(std::string FunctionalUnitName)
{
    /* Functional unit with this name exists in the architecture */
    if (this->Parent->GetFunctionalUnit(FunctionalUnitName) != NULL) return NULL;

    this->FunctionalUnits.push_back(FunctionalUnit(this, FunctionalUnitName));
    FunctionalUnit* pUnit = &*(--this->FunctionalUnits.end());

    return pUnit;
}


/**
 ***********************************************************************
 * @brief  Remove functional unit from the data path.
 * @param  FunctionalUnitName   Name of the functional unit to remove.
 * @return 0 on success.
 */
int Architecture::DataPath::RemoveFunctionalUnit(std::string FunctionalUnitName)
{
    for (std::list<Architecture::FunctionalUnit>::iterator it = this->FunctionalUnits.begin(); it != this->FunctionalUnits.end(); it++)
    {
        if (!FunctionalUnitName.compare(it->GetName()))
        {
            this->FunctionalUnits.erase(it);
            return 0;
        }
    }
    return -1;
}


/**
 ***********************************************************************
 * @brief  Get functional unit.
 * @param  Index    Index of the functional unit in the list.
 * @return Pointer to the functional unit or NULL.
 */
Architecture::FunctionalUnit* Architecture::DataPath::GetFunctionalUnit(std::size_t Index)
{
    if (Index >= this->GetFunctionalUnitCount()) return NULL;
    std::list<Architecture::FunctionalUnit>::iterator it = this->FunctionalUnits.begin();
    std::advance(it, Index);
    return &*it;
}


/**
 ***********************************************************************
 * @brief  Get number of functional units in the data path.
 * @return Number of functional units.
 */
std::size_t Architecture::DataPath::GetFunctionalUnitCount()
{
    return this->FunctionalUnits.size();
}


/**
 ***********************************************************************
 * @brief  Add register to the data path.
 * @param  RegisterName Name of the new register.
 * @return Pointer to the new register or NULL.
 */
Architecture::Register* Architecture::DataPath::AddRegister(std::string RegisterName)
{
    if (this->Parent->GetRegister(RegisterName) != NULL) return NULL;

    this->Registers.push_back(Register(this, RegisterName));
    Register* pReg = &*(--this->Registers.end());

    return pReg;
}


/**
 ***********************************************************************
 * @brief  Remove register from the data path.
 * @param  RegisterName Name of the register to remove.
 * @return 0 on success.
 */
int Architecture::DataPath::RemoveRegister(std::string RegisterName)
{
    for (std::list<Architecture::Register>::iterator it = this->Registers.begin(); it != this->Registers.end(); it++)
    {
        if (!RegisterName.compare(it->GetName()))
        {
            this->Registers.erase(it);
            return 0;
        }
    }
    return -1;
}


/**
 ***********************************************************************
 * @brief  Get register.
 * @param  Index    Index of the register in the list.
 * @return Pointer to the register or NULL.
 */
Architecture::Register* Architecture::DataPath::GetRegister(std::size_t Index)
{
    if (Index >= this->GetRegisterCount()) return NULL;
    std::list<Architecture::Register>::iterator it = this->Registers.begin();
    std::advance(it, Index);
    return &*it;
}


/**
 ***********************************************************************
 * @brief  Get number of registers in the data path.
 * @return Number of registers.
 */
std::size_t Architecture::DataPath::GetRegisterCount()
{
    return this->Registers.size();
}


/**
 ***********************************************************************
 * @brief  Add register group to the data path.
 * @param  GroupName    Name of the new group.
 * @return Pointer to the new group or NULL.
 */
Architecture::RegisterGroup* Architecture::DataPath::AddRegisterGroup(std::string GroupName)
{
    if (this->GetRegisterGroup(GroupName) != NULL) return NULL;

    this->RegisterGroups.push_back(RegisterGroup(this, GroupName));
    RegisterGroup* pGroup = &*(--this->RegisterGroups.end());

    return pGroup;
}


/**
 ***********************************************************************
 * @brief  Remove register group from the data path.
 * @param  GroupName    Name of the group to remove.
 * @return 0 on success.
 */
int Architecture::DataPath::RemoveRegisterGroup(std::string GroupName)
{
    for (std::list<Architecture::RegisterGroup>::iterator it = this->RegisterGroups.begin(); it != this->RegisterGroups.end(); it++)
    {
        if (!GroupName.compare(it->GetName()))
        {
            this->RegisterGroups.erase(it);
            return 0;
        }
    }
    return -1;
}


/**
 ***********************************************************************
 * @brief  Get number of register groups in the data path.
 * @return Number of register groups.
 */
std::size_t Architecture::DataPath::GetRegisterGroupCount()
{
    return this->RegisterGroups.size();
}


/**
 ***********************************************************************
 * @brief  Get register group.
 * @param  GroupName    Name of the register group.
 * @return Pointer to the register group or NULL.
 */
Architecture::RegisterGroup* Architecture::DataPath::GetRegisterGroup(std::string GroupName)
{
    for (std::list<RegisterGroup>::iterator group = this->RegisterGroups.begin(); group != this->RegisterGroups.end(); group++)
    {
        if (!GroupName.compare(group->GetName()))
        {
            return &*group;
        }
    }
    return NULL;
}


/**
 ***********************************************************************
 * @brief  Get register group.
 * @param  Index    Index of the register group in the list.
 * @return Pointer to the register group or NULL.
 */
Architecture::RegisterGroup* Architecture::DataPath::GetRegisterGroup(std::size_t Index)
{
    if (Index >= this->GetRegisterGroupCount()) return NULL;
    std::list<Architecture::RegisterGroup>::iterator it = this->RegisterGroups.begin();
    std::advance(it, Index);
    return &*it;
}


/**
 ***********************************************************************
 * @brief  Set register group to be used as data type.
 * @param  DataType     Data type.
 * @param  GroupName    Name of the register group.
 * @return 0 on success.
 */
int Architecture::DataPath::ConnectDataTypeToGroup(Types::DataType DataType, std::string GroupName)
{
    std::string typeStr = Types::GetDataTypeStr(DataType);
    RegisterGroup *pGroup = this->GetRegisterGroup(GroupName);
    if (typeStr.empty() || pGroup == NULL) return -1;
    this->DataType[typeStr] = pGroup;
    return 0;
}


/**
 ***********************************************************************
 * @brief  Set register group to be used as data type.
 * @param  DataType     Data type.
 * @param  GroupName    Name of the register group.
 * @return 0 on success.
 */
int Architecture::DataPath::ConnectDataTypeToGroup(Types::DataType DataType, Architecture::RegisterGroup* RegisterGroup)
{
    std::string typeStr = Types::GetDataTypeStr(DataType);
    this->DataType[typeStr] = RegisterGroup;
    return 0;
}


/**
 ***********************************************************************
 * @brief  Remove data type connection.
 * @param  DataType Data type.
 * @return 0 on success.
 */
int Architecture::DataPath::RemoveDataType(Types::DataType DataType)
{
    if (this->GetRegisterGroup(DataType) == NULL) return -1;
    this->DataType[Types::GetDataTypeStr(DataType)] = NULL;
    return 0;
}


/**
 ***********************************************************************
 * @brief  Get register group.
 * @param  DataType Data type.
 * @return Pointer to the register group.
 */
Architecture::RegisterGroup* Architecture::DataPath::GetRegisterGroup(Types::DataType DataType)
{
    return this->DataType[Types::GetDataTypeStr(DataType)];
}
