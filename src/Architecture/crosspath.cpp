/**
 ***********************************************************************
 * @file    crosspath.cpp
 * @author  Roman Mego
 * @date    24.10.2016
 * @brief   Cross path methods
 */


#include "architecture.h"


/**
 ***********************************************************************
 * @brief Constructor
 * @param Parent    Reference to the parent architecture.
 * @param Name      Name of the cross path.
 */
Architecture::CrossPath::CrossPath(Architecture* Parent, std::string Name)
{
    this->Parent = Parent;
    this->Name.assign(Name);
    this->Source = NULL;
    this->MaxWidth = 0;
    this->NumOfOperands = 0;
}


/**
 ***********************************************************************
 * @brief   Destructor
 */
Architecture::CrossPath::~CrossPath()
{
    for (std::list<DataPath>::iterator itPath = this->Parent->DataPaths.begin(); itPath != this->Parent->DataPaths.end(); itPath++)
    {
        for (std::size_t n = 0; n < itPath->GetFunctionalUnitCount(); n++)
        {
            FunctionalUnit* unit = itPath->GetFunctionalUnit(n);
            for (std::size_t i = 0; i < unit->GetCrossPathInputCount(); i++)
            {
                if (unit->GetCrossPathInput(i) == this)
                {
                    unit->CleanCrossPath(i);
                }
            }
        }
    }
}


/**
 ***********************************************************************
 * @brief   Get the name of the cross path.
 * @return  Name of the cross path.
 */
std::string Architecture::CrossPath::GetName()
{
    return this->Name;
}


/**
 ***********************************************************************
 * @brief   Set source data path.
 * @param   Source  Reference to the source data path.
 * @return  0 on success.
 */
int Architecture::CrossPath::SetSource(DataPath* Source)
{
    this->Source = Source;
    return 0;
}


/**
 ***********************************************************************
 * @brief   Set source data path.
 * @param   DataPathName    Name of the source data path.
 * @return  0 on success.
 */
int Architecture::CrossPath::SetSource(std::string DataPathName)
{
    DataPath* pDataPath = this->Parent->GetDataPath(DataPathName);

    if (pDataPath == NULL)
    {
        return -1;
    }

    this->Source = pDataPath;

    return 0;
}


/**
 ***********************************************************************
 * @brief   Get source data path.
 * @return  Source data path.
 */
Architecture::DataPath* Architecture::CrossPath::GetSource()
{
    return this->Source;
}


/**
 ***********************************************************************
 * @brief   Set maximum width of transfered data through the cross path.
 * @param   MaxWidth    Width in number of registers.
 * @return  0 on success.
 */
int Architecture::CrossPath::SetMaxWidth(std::size_t MaxWidth)
{
    this->MaxWidth = MaxWidth;
    return 0;
}


/**
 ***********************************************************************
 * @brief   Get maximum width of transfered data through the cross path.
 * @return  Width on cross path in number of registers.
 */
std::size_t Architecture::CrossPath::GetMaxWidth()
{
    return this->MaxWidth;
}


/**
 ***********************************************************************
 * @brief   Set number of operands where can be cross path used at once.
 * @param   NumOfOperands   Number of target operands.
 * @return  0 on success.
 */
int Architecture::CrossPath::SetNumOfOperands(std::size_t NumOfOperands)
{
    this->NumOfOperands = NumOfOperands;
    return 0;
}


/**
 ***********************************************************************
 * @brief   Get the number of operand where the cross path can be used.
 * @return  Number of operands.
 */
std::size_t Architecture::CrossPath::GetNumOfOperands()
{
    return this->NumOfOperands;
}
