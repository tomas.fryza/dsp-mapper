/**
 * @file    mapping.h
 * @author  Roman Mego
 * @date    31.12.2016
 * @brief   Definition of Mapping class.
 */

#ifndef MAPPING_H_
#define MAPPING_H_

#include <vector>
#include <list>
#include "../Architecture/architecture.h"
#include "../Algorithm/algorithm.h"

#include "NodeMap/nodemap.h"
#include "SignalMap/signalmap.h"

namespace Mapping
{
    class Mapping {

    public:
        /**
         * Sorting methods for node placing order.
         */
        class Sorting {
        public:
            enum sorting_method_t {
                NONE,
                UNIT_COUNT,
                TOTAL_CYCLES
            };

            struct sorting_table_t {
                sorting_method_t method;
                bool (*function)(::Mapping::NodeMap&, ::Mapping::NodeMap&);
            };

            static const sorting_table_t sortingTable[];
            static size_t GetMethodsCount();

        private:
            static bool UnitCount(::Mapping::NodeMap &first, ::Mapping::NodeMap &second);
            static bool TotalCycles(::Mapping::NodeMap &first, ::Mapping::NodeMap &second);
        };

        /**
         * Class for controlling time domain of mapped algorithm.
         */
        class Usage {
        public:
            class UnitUsage {
            public:
                Architecture::FunctionalUnit *UnitPtr;          ///< Reference to the unit which represents its usage.
                std::vector<NodeMap*> Usage;                    ///< Vector with references to the mapped nodes for each cycle.
            };

            class RegisterUsage {
            public:
                Architecture::Register *RegisterPtr;            ///< Reference to the register which represent its usage.
                std::vector<SignalMap*> Usage;                  ///< Vector with references to the mapped nodes for each cycle.
            };

            Mapping *Owner;                                     ///< Reference to the parent mapping which usage class belongs to.
            std::list<UnitUsage> UnitsUsage;                    ///< Functional unit usage list (one item for one functional unit).
            std::list<RegisterUsage> RegistersUsage;            ///< Register usage list (one item for one register).

            Usage();
            Usage(Mapping *Owner);
            void AddCycle(void);
            void AddCycles(int n);
            void AddCyclesTotal(int n);
            int GetCycleCount(void);
            UnitUsage* GetUnitUsage(Architecture::FunctionalUnit *pUnit);
            RegisterUsage* GetRegisterUsage(Architecture::Register *pRegister);
            bool IsUnitFree(Architecture::FunctionalUnit *pUnit, int cycle);
            bool IsRegisterFree(Architecture::Register *pRegister, int cycle);
        };

        Mapping(Architecture* pArchitecture, ::Algorithm::Algorithm* pAlgorithm, Sorting::sorting_method_t Method);
        int DoMap();
        SignalMap *FindSignal(::Algorithm::Algorithm::Signal *pSignal);
        bool SignalEqual(::Algorithm::Algorithm::Signal* pSignal1, ::Algorithm::Algorithm::Signal* pSignal2);
        int SaveAsm(std::string FileName, bool ForceComments);
        int SaveDot(std::string FileName);
        int SaveUnitMap(std::string FileName);
        int SaveRegisterMap(std::string FileName);
        Architecture* architecture;                             ///< Architecture description.
        std::list<NodeMap> Nodes;                               ///< Mapped nodes from the algorithm.
        std::list<SignalMap> Signals;                           ///< Mapped signals from the algorithm.
        Usage UsageMap;                                         ///< Map of the resources in the time domain.

    private:
        ::Algorithm::Algorithm* algorithm;                      ///< Algorithm description.
        Sorting::sorting_method_t sortingMethod;
        int AddNode(::Algorithm::Algorithm::Node* pNode);
        int AddNodeConstant(::Algorithm::Algorithm::Node* pNode);
        int AddNodeLoadMem(::Algorithm::Algorithm::Node* pNode);
        int AddNodeStoreMem(::Algorithm::Algorithm::Node* pNode);
        static bool CompareNodes(::Mapping::NodeMap &first, ::Mapping::NodeMap &second);
        static bool FinalCompare(::Mapping::NodeMap &first, ::Mapping::NodeMap &second);
        int GetMaxLevel();
    };
}

#endif /* MAPPING_H_ */
