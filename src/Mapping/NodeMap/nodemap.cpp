/**
 * @file    nodemap.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Methods for NodeMap class.
 */

#include <sstream>
#include <iomanip>
#include <cassert>
#include "../../Common/String/string.h"
#include "../../Common/Messages/message.h"

#include "../mapping.h"

/**
 * @brief Constructor for the node in algorithm map
 */
Mapping::NodeMap::NodeMap()
{
    this->Owner = NULL;
    this->NodePtr = NULL;
    this->MultiInstIndex = -1;
    this->DataPathPtr = NULL;
    this->InstructionPtr = NULL;
    this->FunctionalUnitPtr = NULL;
    this->ProcessLevel = -1;
    this->StartCycle = -1;
    this->EndCycle = -1;
}

/**
 * @brief Constructor for the node in algorithm map
 * @param Owner     Pointer to owner structure of the node map member.
 * @param pNode     Pointer to node for mapping.
 */
Mapping::NodeMap::NodeMap(Mapping *Owner, ::Algorithm::Algorithm::Node* pNode)
{
    assert(Owner != NULL);
    assert(pNode != NULL);

    std::vector<Architecture::Instruction*> instructions;

    this->Owner = Owner;
    this->NodePtr = pNode;
    this->MultiInstIndex = -1;
    /* TODO Temporary place to first data path */
    this->DataPathPtr = this->Owner->architecture->GetDataPath(0);
    this->InstructionPtr = NULL;
    this->FunctionalUnitPtr = NULL;
    this->ProcessLevel = -1;
    this->StartCycle = -1;
    this->EndCycle = -1;

    /* Find the instruction for node */
    if (pNode->Operation == Types::Operation::STOREMEM || pNode->Operation == Types::Operation::LOADMEM)
    {
        instructions = Owner->architecture->GetInstruction(pNode->Operation);
    }
    else
    {
        instructions = Owner->architecture->GetInstruction(pNode->Operation, pNode->OutputSignal->DataType);
    }

    for (std::size_t i = 0; i < instructions.size(); i++)
    {
        /* If instruction is not assigned or the assigned instruction takes more cycles */
        if (this->InstructionPtr == NULL ||
            this->InstructionPtr->GetTotalCycles() > instructions[i]->GetTotalCycles())
        {
            this->InstructionPtr = instructions[i];
        }
    }
}

Mapping::NodeMap::NodeMap(Mapping *Owner, ::Algorithm::Algorithm::Node* pNode, ::Architecture::Instruction* pInstruction, int Index)
{
    assert(Owner != NULL);
    assert(pNode != NULL);
    assert(pInstruction != NULL);

    assert(pNode->Operation == Types::Operation::CONST);
    assert(pInstruction->GetOperation() == Types::Operation::CONST || pInstruction->GetOperation() == Types::Operation::CONSTH || pInstruction->GetOperation() == Types::Operation::CONSTL);

    this->Owner = Owner;
    this->NodePtr = pNode;
    this->MultiInstIndex = Index;
    /* TODO Temporary place to first data path */
    this->DataPathPtr = this->Owner->architecture->GetDataPath(0);
    this->InstructionPtr = pInstruction;
    this->FunctionalUnitPtr = NULL;
    this->ProcessLevel = -1;
    this->StartCycle = -1;
    this->EndCycle = -1;
}

std::string Mapping::NodeMap::DefinitionFile()
{
    assert(this != NULL);
    return this->NodePtr->Owner->DefinitionFile;
}

int Mapping::NodeMap::DefinitionLine()
{
    assert(this != NULL);
    return this->NodePtr->DefinitionLine;
}

/**
 * @brief   Get operation of the node.
 * @return  Operation of the node.
 */
Types::Operation Mapping::NodeMap::Operation()
{
    assert(this != NULL);
    return this->NodePtr->Operation;
}

/**
 * @brief   Check if node generates output signal of the algorithm.
 * @return  TRUE if node generates output signal, otherwise FALSE.
 */
bool Mapping::NodeMap::IsOutput()
{
    assert(this != NULL);
    return (this->NodePtr->OutputSignal->Type == Types::SignalType::OUTPUT);
}

/**
 * @brief   Gets input signals of the node.
 * @return  Vector with input signals.
 */
std::vector<Mapping::SignalMap*> Mapping::NodeMap::GetInputSignals()
{
    assert(this != NULL);

    std::vector<SignalMap*> retVal;

    for (std::vector<::Algorithm::Algorithm::NodeInput>::iterator it = this->NodePtr->InputSignal.begin(); it != this->NodePtr->InputSignal.end(); it++)
    {
        if (it->Type != Types::NodeInputType::SIGNAL && it->Type != Types::NodeInputType::POINTER)
        {
            continue;
        }

        SignalMap* tmpSignal = this->Owner->FindSignal(it->Value.signal);
        assert(tmpSignal != NULL);
        retVal.push_back(tmpSignal);
    }

    return retVal;
}

/**
 * @brief   Gets nodes creating input signals of current node.
 * @return  Vector with nodes creating input signals.
 */
std::vector<Mapping::NodeMap*> Mapping::NodeMap::GetInputNodes()
{
    assert(this != NULL);

    std::vector<NodeMap*> retVal;
    std::vector<SignalMap*> signals = this->GetInputSignals();

    for (std::vector<SignalMap*>::iterator it = signals.begin(); it != signals.end(); it++)
    {
        retVal.push_back((*it)->Creator);
    }

    return retVal;
}

Mapping::SignalMap* Mapping::NodeMap::GetOutputSignal()
{
    assert(this != NULL);

    return this->Owner->FindSignal(this->NodePtr->OutputSignal);
}

bool Mapping::NodeMap::OutputEquals(::Algorithm::Algorithm::Signal* pSignal)
{
    assert(this != NULL);
    assert(pSignal != NULL);

    return this->Owner->SignalEqual(pSignal, this->NodePtr->OutputSignal);
}

/**
 * @brief Set start instruction cycle for the node.
 * @param Cycle Start cycle of the mapped node.
 */
void Mapping::NodeMap::SetStartCycle(int Cycle)
{
    assert(this != NULL);
    assert(Cycle >= 0);

    this->StartCycle = Cycle;
    this->EndCycle = this->StartCycle + this->InstructionPtr->GetTotalCycles() - 1;
}

/**
 * @brief  Get minimum instruction cycle when the instruction can be executed.
 * @return Cycle number.
 */
int Mapping::NodeMap::GetMinCycle(void)
{
    assert(this != NULL);

    int minCycle = -1;

    if (this->InstructionPtr->GetOperation() == Types::Operation::CONST ||
        this->InstructionPtr->GetOperation() == Types::Operation::CONSTL ||
        this->InstructionPtr->GetOperation() == Types::Operation::CONSTH)
    {
        minCycle = 0;
    }
    else
    {
        for (std::vector<::Algorithm::Algorithm::NodeInput>::iterator it = this->NodePtr->InputSignal.begin(); it != this->NodePtr->InputSignal.end(); it++)
        {
            ::Mapping::SignalMap* pInputSignal = this->Owner->FindSignal(it->Value.signal);

            if (pInputSignal->Creator != NULL)
            {
                /* If signal creator is constant loading in 2 instructions */
                if (pInputSignal->Creator->InstructionPtr->GetOperation() == Types::Operation::CONSTL ||
                    pInputSignal->Creator->InstructionPtr->GetOperation() == Types::Operation::CONSTH)
                {
                    /* Search for constant loading instructions */
                    for (std::list<::Mapping::NodeMap>::iterator tmpIt = this->Owner->Nodes.begin(); tmpIt != this->Owner->Nodes.end(); tmpIt++)
                    {
                        if (tmpIt->NodePtr->OutputSignal == it->Value.signal &&
                            minCycle <= tmpIt->EndCycle)
                        {
                            minCycle = tmpIt->EndCycle + 1;
                        }
                    }
                }
                /* If signal creator is arithmetic/logic operation */
                else
                {
                    if (minCycle <= pInputSignal->Creator->EndCycle)
                    {
                        minCycle = pInputSignal->Creator->EndCycle + 1;
                    }
                }
            }
            else
            {
                if (minCycle < 0)
                {
                    minCycle = 0;
                }
            }
        }
    }

    return minCycle;
}

/**
 * @brief   Place node into the map.
 * @return  0 on success.
 */
int Mapping::NodeMap::Place(void)
{
    assert(this != NULL);

    /* Fill to min. cycle count */
    if (this->GetMinCycle() >= this->Owner->UsageMap.GetCycleCount())
    {
        this->Owner->UsageMap.AddCycles(this->GetMinCycle() - this->Owner->UsageMap.GetCycleCount() + 1);
    }

    /* Search from min. instruction cycle */
    for (int n = this->GetMinCycle(); n <= this->Owner->UsageMap.GetCycleCount(); n++)
    {
        if (n == this->Owner->UsageMap.GetCycleCount())
        {
            this->Owner->UsageMap.AddCycle();
        }

        /* Search in every supported functional unit */
        for (std::size_t m = 0; m < this->InstructionPtr->GetFunctionalUnitCount(); m++)
        {
            Architecture::FunctionalUnit* pUnit = this->InstructionPtr->GetFunctionalUnit(m);

            /* If it is in selected data path */
            if (pUnit->GetParentDataPath() == this->DataPathPtr)
            {
                /* If functional unit is free in the cycle */
                if (this->Owner->UsageMap.IsUnitFree(pUnit, n))
                {
                    this->Owner->UsageMap.GetUnitUsage(pUnit)->Usage[n] = this;
                    this->SetStartCycle(n);
                    this->FunctionalUnitPtr = pUnit;

                    this->Owner->UsageMap.AddCyclesTotal(this->EndCycle + 1);
                    return 0;
                }
            }
        }
    }

    return -1;
}

std::string Mapping::NodeMap::GetNumberHex(double Value, Types::DataType DataType)
{
    std::ostringstream str;

    union {
        float float_val;
        double double_val;
        int64_t int64_val;
    };

    switch (DataType)
    {
        case Types::DataType::FLOAT:
            float_val = Value;
            break;
        case Types::DataType::DOUBLE:
            double_val = Value;
            break;
        default:
            int64_val = Value;
    }

    str << std::hex << std::uppercase << std::setw(16) << std::setfill('0') << int64_val;

    return str.str();
}

/**
 * @brief   Get string of the final instruction.
 * @return  Instruction for the node.
 */
std::string Mapping::NodeMap::GetAsmInstruction(void)
{
    assert(this != NULL);

    int n = 1;
    std::string instr = this->InstructionPtr->GetFormat();

    instr = StringReplace(instr, "unit", this->FunctionalUnitPtr->GetName());
    if (this->NodePtr->OutputSignal != NULL)
    {
        instr = StringReplace(instr, "dst", this->Owner->FindSignal(this->NodePtr->OutputSignal)->RegAssignPtr->GetName());
    }

    for (std::vector<::Algorithm::Algorithm::NodeInput>::iterator it = this->NodePtr->InputSignal.begin(); it != this->NodePtr->InputSignal.end(); it++)
    {
        std::ostringstream tmpStr;
        tmpStr << "src" << n++;

        if (this->InstructionPtr->GetOperation() == Types::Operation::CONST ||
            this->InstructionPtr->GetOperation() == Types::Operation::CONSTH ||
            this->InstructionPtr->GetOperation() == Types::Operation::CONSTL)
        {
            std::stringstream strStream;
            std::string numStr = NodeMap::GetNumberHex(it->Value.constant, this->NodePtr->OutputSignal->DataType);
            size_t instTargetSize = 2 * Types::GetDataTypeBytes(this->InstructionPtr->GetDataType(0));

            int strStartIndex = numStr.size() - (instTargetSize * (this->MultiInstIndex + 1));

            if (this->InstructionPtr->GetOperation() == Types::Operation::CONSTL)
            {
                strStartIndex += instTargetSize / 2;
            }

            if (this->InstructionPtr->GetOperation() != Types::Operation::CONST)
            {
                instTargetSize /= 2;
            }

            strStream << numStr.substr(strStartIndex, instTargetSize) << 'h';
            instr = StringReplace(instr, tmpStr.str(), strStream.str());
        }
        else
        {
            instr = StringReplace(instr, tmpStr.str(), this->Owner->FindSignal(it->Value.signal)->RegAssignPtr->GetName());
        }
    }

    return instr;
}

/**
 * @brief   Get the notation of the node.
 * @return  Notation of the node.
 */
std::string Mapping::NodeMap::GetNotation()
{
    assert(this != NULL);
    return this->NodePtr->GetNotation();
}
