/**
 * @file    nodemap.h
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   NodeMap class definition.
 */

#ifndef NODEMAP_H_
#define NODEMAP_H_

#include <vector>
#include "../../Architecture/architecture.h"
#include "../../Algorithm/algorithm.h"
#include "../../Common/Types/types.h"

namespace Mapping
{
    class Mapping;
    class SignalMap;

    /**
     * Class with attributes of the mapped node.
     */
    class NodeMap {
    private:
        ::Algorithm::Algorithm::Node* NodePtr;              //< Reference to the mapped node.
    public:
        ::Mapping::Mapping *Owner;                          ///< Owner mapping where node map belongs to.
        int MultiInstIndex;                                 ///< Index of the node required multiple instructions.
        int ProcessLevel;                                   ///< Process level, order of the node in the algorithm from the input.
        Architecture::DataPath *DataPathPtr;                ///< Mapped data path.
        Architecture::Instruction *InstructionPtr;          ///< Mapped instruction.
        Architecture::FunctionalUnit *FunctionalUnitPtr;    ///< Mapped functional unit.
        int StartCycle;                                     ///< First cycle order of the operation in the mapped algorithm.
        int EndCycle;                                       ///< Last cycle order of the operation in the mapped algorithm.
        NodeMap();
        NodeMap(::Mapping::Mapping *Owner, ::Algorithm::Algorithm::Node* pNode);
        NodeMap(::Mapping::Mapping *Owner, ::Algorithm::Algorithm::Node* pNode, ::Architecture::Instruction* pInstruction, int Index);
        std::string DefinitionFile();
        int DefinitionLine();
        Types::Operation Operation();
        bool IsOutput();
        std::vector<::Mapping::SignalMap*> GetInputSignals();
        std::vector<NodeMap*> GetInputNodes();
        ::Mapping::SignalMap* GetOutputSignal();
        bool OutputEquals(::Algorithm::Algorithm::Signal* pSignal);
        int GetMinCycle(void);
        void SetStartCycle(int Cycle);
        int Place(void);
        static std::string GetNumberHex(double Value, Types::DataType DataType);
        std::string GetAsmInstruction(void);
        std::string GetNotation();
    };
}

#endif /* NODEMAP_H_ */
