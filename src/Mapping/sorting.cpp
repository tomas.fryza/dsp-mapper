/**
 * @file    sorting.cpp
 * @author  Roman Mego
 * @date    31.12.2016
 * @brief   Map sorting methods for nodes.
 */

#include "mapping.h"

const ::Mapping::Mapping::Sorting::sorting_table_t Mapping::Mapping::Sorting::sortingTable[] = {
    { NONE,         NULL },
    { UNIT_COUNT,   UnitCount },
    { TOTAL_CYCLES, TotalCycles }
};

size_t Mapping::Mapping::Sorting::GetMethodsCount()
{
    return sizeof(Mapping::Sorting::sortingTable) / sizeof(Mapping::Sorting::sorting_table_t);
}

bool ::Mapping::Mapping::Sorting::UnitCount(::Mapping::NodeMap &first, ::Mapping::NodeMap &second)
{
    return (first.InstructionPtr->GetFunctionalUnitCount() < second.InstructionPtr->GetFunctionalUnitCount());
}

bool ::Mapping::Mapping::Sorting::TotalCycles(::Mapping::NodeMap &first, ::Mapping::NodeMap &second)
{
    return (first.InstructionPtr->GetTotalCycles() > second.InstructionPtr->GetTotalCycles());
}
