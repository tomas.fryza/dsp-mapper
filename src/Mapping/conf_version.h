/**
 * @file    conf_version.h
 * @author  Roman Mego
 * @date    5.11.2015
 * @brief   Version configuration
 */

#ifndef CONF_VERSION_H_
#define CONF_VERSION_H_

#define VERSION_MAJOR   0   ///< Major version
#define VERSION_MINOR   1   ///< Minor version

#endif /* CONF_VERSION_H_ */
