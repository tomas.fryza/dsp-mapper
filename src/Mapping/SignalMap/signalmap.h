/**
 * @file    signalmap.h
 * @author  Roman Mego
 * @date    31.12.2016
 * @brief   SignalMap class definition.
 */

#ifndef SIGNALMAP_H_
#define SIGNALMAP_H_

#include "../../Architecture/architecture.h"
#include "../../Algorithm/algorithm.h"
#include "../../Common/Types/types.h"

namespace Mapping
{
    class NodeMap;
    class Mapping;

    /**
     * Class with attributes of the mapped signal.
     */
    class SignalMap {
    private:
        ::Mapping::Mapping *Owner;                          ///< Owner mapping where signal map belongs to.
        ::Algorithm::Algorithm::Signal *SignalPtr;          ///< Reference to the  mapped signal.
    public:
        SignalMap(::Mapping::Mapping *Owner, ::Algorithm::Algorithm::Signal* pSignal);
        std::string Name();
        bool Equals(::Algorithm::Algorithm::Signal* pSignal);
        void RefreshLifeTime(void);
        int Place(void);
        ::Mapping::NodeMap *Creator;                        ///< Reference to the node which created the signal.
        Architecture::RegisterGroup *RegGroupPtr;           ///< Reference to the register group according to the data type.
        Architecture::RegisterAssign *RegAssignPtr;         ///< Register assignment for the signal.
        int StartCycle;                                     ///< First cycle of the signal lifetime.
        int EndCycle;                                       ///< Last cycle of the signal lifetime.
    };
}

#endif /* SOURCE_MAPPING_SIGNALMAP_H_ */
