/**
 * @file    signalmap.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Methods for SignalMap class.
 */

#include <cassert>

#include "../../Common/Messages/message.h"
#include "../mapping.h"

/**
 * @brief Constructor for the signal in algorithm map
 * @param Owner     Pointer to parent structure.
 * @param pSignal   Pointer to signal from algorithm.
 */
Mapping::SignalMap::SignalMap(::Mapping::Mapping *Owner, ::Algorithm::Algorithm::Signal* pSignal)
{
    assert(Owner != NULL);
    assert(pSignal != NULL);

    this->Owner = Owner;
    this->SignalPtr = pSignal;
    this->Creator = NULL;
    this->RegAssignPtr = NULL;
    this->StartCycle = -1;
    this->EndCycle = -1;

    /* Get pointer to the creator of the signal */
    for (std::list<::Mapping::NodeMap>::iterator itNode = this->Owner->Nodes.begin(); itNode != this->Owner->Nodes.end(); itNode++)
    {
        if (itNode->InstructionPtr->GetOperation() == Types::Operation::CONSTH ||
            itNode->InstructionPtr->GetOperation() == Types::Operation::STOREMEM)
        {
            continue;
        }

        if (itNode->OutputEquals(pSignal))
        {
            if (this->Creator == NULL)
            {
                this->Creator = &*itNode;
            }
            else
            {
                Error("Multiple creators of signal " + this->Name(), itNode->DefinitionFile(), itNode->DefinitionLine());
            }
        }
    }

    /* Check the definition of the signal */
    if (this->SignalPtr->Type != Types::SignalType::INPUT && this->SignalPtr->DataType != Types::DataType::POINTER && this->Creator == NULL)
    {
        Error("Signal " + this->SignalPtr->Name + " has not defined its creator.", this->SignalPtr->Owner->DefinitionFile, this->SignalPtr->DefinitionLine);
    }

    /* Get pointer to the register group representing signal data type */
    /* TODO Temporary only data path 0 */
    this->RegGroupPtr = this->Owner->architecture->GetDataPath(0)->GetRegisterGroup(this->SignalPtr->DataType);
}

/**
 * @brief   Get name of the signal.
 * @return  Signal name.
 */
std::string Mapping::SignalMap::Name()
{
    assert(this != NULL);
    return this->SignalPtr->Name;
}

/**
 * @brief   Check if signal map element equals to signal from algorithm.
 * @param   pSignal Pointer to signal to check.
 * @return  TRUE if signal equals, otherwise FALSE.
 */
bool Mapping::SignalMap::Equals(::Algorithm::Algorithm::Signal* pSignal)
{
    assert(this != NULL);
    assert(pSignal != NULL);
    return (this->SignalPtr == pSignal);
}

/**
 * @brief Refresh lifetime of the signal
 */
void Mapping::SignalMap::RefreshLifeTime(void)
{
    assert(this != NULL);

    this->EndCycle = -1;

    /* Start */
    if (this->SignalPtr->Type == Types::SignalType::INPUT)
    {
        this->StartCycle = 0;
    }
    else
    {
        this->StartCycle = this->Creator->EndCycle - this->Creator->InstructionPtr->GetWriteCycles() + 1;
    }

    /* End */
    if (this->SignalPtr->Type == Types::SignalType::OUTPUT)
    {
        this->EndCycle = this->Owner->UsageMap.GetCycleCount();
    }
    else
    {
        /* Search in every node */
        for (std::list<NodeMap>::iterator itNode = this->Owner->Nodes.begin(); itNode != this->Owner->Nodes.end(); itNode++)
        {
            /* Test every input signal of the node */
            std::vector<SignalMap*> signals = itNode->GetInputSignals();
            for (std::vector<SignalMap*>::iterator itSignal = signals.begin(); itSignal != signals.end(); itSignal++)
            {
                if (*itSignal == this)
                {
                    if (this->EndCycle < itNode->StartCycle + itNode->InstructionPtr->GetReadCycles() - 1)
                    {
                        this->EndCycle = itNode->StartCycle + itNode->InstructionPtr->GetReadCycles() - 1;
                    }
                }
            }
        }
    }
}

/**
 * @brief   Place signal into the map.
 * @return  0 on success.
 */
int Mapping::SignalMap::Place(void)
{
    assert(this != NULL);

    /* Repeat for every register assignment (single registers, pairs, quads) in register group according to data type */
    for (std::size_t i = 0; i < this->RegGroupPtr->GetAssignmentCount(); i++)
    {
        bool found = true;

        /* Get register assignment (register, pair, quad...) */
        Architecture::RegisterAssign *pAssign = this->RegGroupPtr->GetAssignment(i);

        /* For every register in assignment */
        for (std::size_t n = 0; n < pAssign->GetRegisterCount(); n++)
        {
            /* For every instruction cycle of signal lifetime */
            for (int m = this->StartCycle; m <= this->EndCycle; m++)
            {
                /* Check if register is free */
                if (!this->Owner->UsageMap.IsRegisterFree(pAssign->GetRegister(n), m) &&
                    (this->Owner->UsageMap.GetRegisterUsage(pAssign->GetRegister(n))->Usage[m]->EndCycle != m) &&
                    !(this->Owner->UsageMap.GetRegisterUsage(pAssign->GetRegister(n))->Usage[m]->StartCycle == m && this->EndCycle == m))
                {
                    found = false;
                }
            }
        }

        if (found)
        {
            this->RegAssignPtr = pAssign;

            for (std::size_t n = 0; n < RegAssignPtr->GetRegisterCount(); n++)
            {
                for (int m = this->StartCycle; m <= this->EndCycle; m++)
                {
                    if (this->Owner->UsageMap.GetRegisterUsage(RegAssignPtr->GetRegister(n))->Usage[m] == NULL ||
                        this->EndCycle != m)
                    {
                        this->Owner->UsageMap.GetRegisterUsage(RegAssignPtr->GetRegister(n))->Usage[m] = this;
                    }
                }
            }
            return 0;
        }
    }

    return -1;
}
