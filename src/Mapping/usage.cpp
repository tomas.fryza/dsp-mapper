/**
 * @file    usage.cpp
 * @author  Roman Mego
 * @date    31.12.2016
 * @brief   Methods for Usage class.
 */

#include "mapping.h"

/**
 * @brief Usage constructor.
 */
::Mapping::Mapping::Usage::Usage()
{
    this->Owner = NULL;
}

/**
 * @brief Usage constructor.
 * @param pParent   Pointer to parent structure.
 */
::Mapping::Mapping::Usage::Usage(Mapping *pParent)
{
    this->Owner = pParent;

    for (std::size_t n = 0; n < this->Owner->architecture->GetDataPathCount(); n++)
    {
        Architecture::DataPath *pDataPath = this->Owner->architecture->GetDataPath(n);
        for (std::size_t m = 0; m < pDataPath->GetFunctionalUnitCount(); m++)
        {
            UnitUsage tmpUnitUsage;
            tmpUnitUsage.UnitPtr = pDataPath->GetFunctionalUnit(m);
            this->UnitsUsage.push_back(tmpUnitUsage);
        }
        for (std::size_t m = 0; m < pDataPath->GetRegisterCount(); m++)
        {
            RegisterUsage tmpRegUsage;
            tmpRegUsage.RegisterPtr = pDataPath->GetRegister(m);
            this->RegistersUsage.push_back(tmpRegUsage);
        }
    }
}

/**
 * @brief Add one cycle to usage map.
 */
void ::Mapping::Mapping::Usage::AddCycle(void)
{
    for (std::list<Usage::UnitUsage>::iterator it = this->UnitsUsage.begin(); it != this->UnitsUsage.end(); it++)
    {
        it->Usage.push_back(NULL);
    }
    for (std::list<Usage::RegisterUsage>::iterator it = this->RegistersUsage.begin(); it != this->RegistersUsage.end(); it++)
    {
        it->Usage.push_back(NULL);
    }
}

/**
 * @brief Add cycles to usage map.
 * @param n Number of cycles to add.
 */
void ::Mapping::Mapping::Usage::AddCycles(int n)
{
    while (n--)
    {
        this->AddCycle();
    }
}

/**
 * @brief Add cycles to usage map for total number of cycles.
 * @param n Number of cycles contained in the usage map at the end.
 */
void ::Mapping::Mapping::Usage::AddCyclesTotal(int n)
{
    int size = this->GetCycleCount();
    if (n > size)
    {
        this->AddCycles(n - size);
    }
}

/**
 * @brief  Get number of cycles in usage map.
 * @return Number of cycles in usage map.
 */
int ::Mapping::Mapping::Usage::GetCycleCount(void)
{
    if (!this->UnitsUsage.empty())
    {
        return this->UnitsUsage.begin()->Usage.size();
    }
    return -1;
}

/**
 * @brief  Get the usage map of the functional unit.
 * @param  pUnit    Pointer to functional unit to find.
 * @return Pointer to usage map of the functional unit.
 */
::Mapping::Mapping::Usage::UnitUsage* ::Mapping::Mapping::Usage::GetUnitUsage(Architecture::FunctionalUnit *pUnit)
{
    for (std::list<UnitUsage>::iterator it = this->UnitsUsage.begin(); it != this->UnitsUsage.end(); it++)
    {
        if (it->UnitPtr == pUnit)
        {
            return &*it;
        }
    }
    return NULL;
}

/**
 * @brief  Get the usage of the register.
 * @param  pRegister    Pointer to register to find.
 * @return Pointer to usage map of the register.
 */
::Mapping::Mapping::Usage::RegisterUsage* ::Mapping::Mapping::Usage::GetRegisterUsage(Architecture::Register *pRegister)
{
    for (std::list<RegisterUsage>::iterator it = this->RegistersUsage.begin(); it != this->RegistersUsage.end(); it++)
    {
        if (it->RegisterPtr == pRegister)
        {
            return &*it;
        }
    }
    return NULL;
}

/**
 * @brief  Get state of the functional unit usage in one instruction cycle.
 * @param  pUnit    Pointer to functional unit.
 * @param  cycle    Instruction cycle.
 * @return TRUE if the functional unit is not used.
 */
bool ::Mapping::Mapping::Usage::IsUnitFree(Architecture::FunctionalUnit *pUnit, int cycle)
{
    UnitUsage* unitUsage = this->GetUnitUsage(pUnit);
    if (unitUsage == NULL) return false;
    if (cycle >= this->GetCycleCount()) return true;
    return (unitUsage->Usage[cycle] == NULL);
}

/**
 * @brief  Get state of the register usage in one instruction cycle.
 * @param  pUnit    Pointer to register.
 * @param  cycle    Instruction cycle.
 * @return TRUE if the register is not used.
 */
bool ::Mapping::Mapping::Usage::IsRegisterFree(Architecture::Register *pRegister, int cycle)
{
    RegisterUsage* registerUsage = this->GetRegisterUsage(pRegister);
    if (registerUsage == NULL) return false;
    if (cycle >= this->GetCycleCount()) return true;
    return (!registerUsage->Usage[cycle]);
}
