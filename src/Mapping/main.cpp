/**
 * @file    main.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Main file.
 */

#include <iostream>
#include "mapping.h"
#include "../Common/Messages/debug.h"
#include "../Common/Messages/message.h"
#include "../Common/Arguments/commandlineparser.h"
#include "../Common/FileInfo/fileinfo.h"
#include "../Common/Version/version.h"

/**
 * @brief   Main function called at the start of the application.
 * @param   argc    Number of arguments from the console.
 * @param   argv    Array of arguments from the console.
 * @return  0 on success.
 */
int main (int argc, char *argv[])
{
    Architecture targetArchitecture;
    ::Algorithm::Algorithm algorithm;

    Mapping::Mapping* dspMap;
    std::string asmPath;
    std::string archPath;
    std::string algPath;
    std::string dotPath;
    Types::DataType dataType = Types::DataType::INT32;

    CommandLineParser parser;

    CommandLineParser::CommandLineOption architecture('a', "architecture", "Architecture file");
    CommandLineParser::CommandLineOption source('s', "source", "Source code file");
    CommandLineParser::CommandLineOption datatype('d', "data", "Data type (int32, int64, int128, float, double)");
    CommandLineParser::CommandLineOption out('o', "output", "Assembler output file");
    CommandLineParser::CommandLineOption graph('g', "graph", "Graphwiz output file");
    CommandLineParser::CommandLineOption map('m', "map", "Resource map output file");
    CommandLineParser::CommandLineOption comments('c', "comments", "Turn on the comments output in the ASM file");

    parser.AddOption(architecture);
    parser.AddOption(source);
    parser.AddOption(datatype);
    parser.AddOption(out);
    parser.AddOption(graph);
    parser.AddOption(map);
    parser.AddOption(comments);

    std::cout << "DSP Mapper" << std::endl;
    std::cout << "Version: " << (int)Version().major << "." << (int)Version().minor << "." << Version().build << "." << Version().revision << std::endl;

    parser.Parse(argc, argv);

    if (!parser.IsSet(architecture) || parser.Value(architecture).empty())
    {
        std::cout << "Architecture is not defined." << std::endl;
        parser.ShowHelp();
        std::exit(-1);
    }

    archPath = parser.Value(architecture);

    if (!parser.IsSet(source) || parser.Value(source).empty())
    {
        std::cout << "Source code is not defined." << std::endl;
        parser.ShowHelp();
        std::exit(-1);
    }

    algPath = parser.Value(source);

    if (!parser.IsSet(out) || parser.Value(out).empty())
    {
        asmPath = FileInfo::GetBaseFileName(algPath) + ".asm";
    }
    else
    {
        asmPath = parser.Value(out);
    }

    if (!parser.IsSet(datatype) || parser.Value((int)dataType).empty())
    {
        Warning("Default data type is not defined (set to " + Types::GetDataTypeStr(dataType) + ").");
    }
    else
    {
        dataType = Types::GetDataType(parser.Value(datatype));
        if (dataType == Types::DataType::NONE) Error("Unsupported data type \"" + parser.Value(datatype) + "\".");
    }

    targetArchitecture.Load(archPath);
    algorithm.Load(algPath, dataType);
    dspMap = new Mapping::Mapping(&targetArchitecture, &algorithm, Mapping::Mapping::Sorting::NONE);

    if (dspMap->DoMap())
    {
        Error("Cannot map algorithm to the target architecture.");
    }

    dspMap->SaveAsm(asmPath, parser.IsSet(comments));

    if (parser.IsSet(graph))
    {
        if (parser.Value(graph).empty())
        {
            dotPath = FileInfo::GetDirectory(asmPath) + FileInfo::GetBaseFileName(asmPath) + ".gv";
        }
        else
        {
            dotPath = parser.Value(graph);
        }

        dspMap->SaveDot(dotPath);
    }

    if (parser.IsSet(map))
    {
        std::string unitPath = FileInfo::GetDirectory(asmPath) + FileInfo::GetBaseFileName(asmPath) + ".unt.csv";
        std::string registerPath = FileInfo::GetDirectory(asmPath) + FileInfo::GetBaseFileName(asmPath) + ".reg.csv";

        dspMap->SaveUnitMap(unitPath);
        dspMap->SaveRegisterMap(registerPath);
    }

    return 0;
}
