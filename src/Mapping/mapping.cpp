/**
 * @file    mapping.cpp
 * @author  Roman Mego
 * @date    29.4.2017
 * @brief   Methods for Mapping class.
 */

#include <fstream>
#include <functional>
#include <cassert>
#include "mapping.h"
#include "SignalMap/signalmap.h"
#include "NodeMap/nodemap.h"
#include "../Common/Messages/debug.h"

/**
 * @brief   Constructor of the mapping class.
 * @param   pArchitecture   Pointer to the architecture.
 * @param   pAlgorithm      Pointer to the Algorithm.
 */
Mapping::Mapping::Mapping(Architecture* pArchitecture, ::Algorithm::Algorithm* pAlgorithm, Sorting::sorting_method_t Method)
{
    assert(pArchitecture != NULL);
    assert(pAlgorithm != NULL);

    this->architecture = pArchitecture;
    this->algorithm = pAlgorithm;
    this->sortingMethod = Method;

    /* Create usage map from architecture */
    this->UsageMap = Usage(this);

    DBG_PRINT("Mapping algorithm...");

    /* Add nodes to the map */
    for (std::list<::Algorithm::Algorithm::Node>::iterator it = this->algorithm->Nodes.begin(); it != this->algorithm->Nodes.end(); ++it)
    {
        DBG_PRINT("Adding node \"" << it->GetNotation() << "\"...");
        this->AddNode(&*it);
    }

    /* Add signals to the map */
    for (std::list<::Algorithm::Algorithm::Signal>::iterator it = this->algorithm->Signals.begin(); it != this->algorithm->Signals.end(); it++)
    {
        if (it->Type == Types::SignalType::ALIAS)
        {
            continue;
        }

        ::Mapping::SignalMap tmpSignalMap = SignalMap(this, &*it);
        this->Signals.push_back(tmpSignalMap);
    }
}

/**
 * @brief   Add node to map according to node from algorithm.
 * @param   pNode   Pointer to node from algorithm to add.
 * @return  0 on success.
 */
int Mapping::Mapping::AddNode(::Algorithm::Algorithm::Node* pNode)
{
    assert(this != NULL);
    assert(pNode != NULL);

    ::Mapping::NodeMap tmpNodeMap;

    if (pNode->Operation == Types::Operation::CONST)
    {
        this->AddNodeConstant(pNode);
    }
    else if (pNode->Operation == Types::Operation::LOADMEM)
    {
        this->AddNodeLoadMem(pNode);
    }
    else if (pNode->Operation == Types::Operation::STOREMEM)
    {
        this->AddNodeStoreMem(pNode);
    }
    else
    {
        tmpNodeMap = NodeMap(this, pNode);

        assert(tmpNodeMap.InstructionPtr != NULL);
        if (tmpNodeMap.InstructionPtr == NULL)
        {
            return -1;
        }

        this->Nodes.push_back(tmpNodeMap);
    }

    return 0;
}

int Mapping::Mapping::AddNodeConstant(::Algorithm::Algorithm::Node* pNode)
{
    ::Mapping::NodeMap tmpNodeMap;
    Types::DataType tmpType = pNode->OutputSignal->DataType;
    tmpType = Types::GetDataType(Types::GetDataTypeBytes(tmpType), Types::representation_t::REPR_INTEGER);

    do
    {
        std::vector<::Architecture::Instruction*> ins_const = this->architecture->GetInstruction(Types::Operation::CONST, tmpType);
        if (ins_const.size() > 0)
        {
            for (size_t i = 0; i < Types::GetDataTypeBytes(pNode->OutputSignal->DataType) / Types::GetDataTypeBytes(tmpType); i++)
            {
                tmpNodeMap = NodeMap(this, pNode, ins_const[0], i);
                this->Nodes.push_back(tmpNodeMap);
            }
        }
        else
        {
            std::vector<::Architecture::Instruction*> ins_consth = this->architecture->GetInstruction(Types::Operation::CONSTH, tmpType);
            std::vector<::Architecture::Instruction*> ins_constl = this->architecture->GetInstruction(Types::Operation::CONSTL, tmpType);
            if (ins_consth.size() > 0 && ins_constl.size() > 0)
            {
                for (size_t i = 0; i < Types::GetDataTypeBytes(pNode->OutputSignal->DataType) / Types::GetDataTypeBytes(tmpType); i++)
                {
                    tmpNodeMap = NodeMap(this, pNode, ins_constl[0], i);
                    this->Nodes.push_back(tmpNodeMap);

                    tmpNodeMap = NodeMap(this, pNode, ins_consth[0], i);
                    this->Nodes.push_back(tmpNodeMap);
                }
            }
        }

        tmpType = Types::GetDataType(Types::GetDataTypeBytes(tmpType) / 2, Types::representation_t::REPR_INTEGER);
    } while(tmpType != Types::DataType::NONE);

    return 0;
}

// TODO LOADMEM AddNode
int Mapping::Mapping::AddNodeLoadMem(::Algorithm::Algorithm::Node* pNode)
{
    assert(pNode != NULL);

    // Create algorithm signal which will store modified pointer
    ::Algorithm::Algorithm::Signal* modifiedPointer = new ::Algorithm::Algorithm::Signal();
    modifiedPointer->Owner = pNode->Owner;
    modifiedPointer->DefinitionLine = pNode->DefinitionLine;
    modifiedPointer->DataType = Types::DataType::POINTER;
    modifiedPointer->Type = Types::SignalType::SIGNAL;
    modifiedPointer->Name.assign(pNode->InputSignal[0].Value.signal->Name + "[" + std::to_string(pNode->InputSignal[1].Value.index) + "]");

    // Create constant to modify pointer
    ::Algorithm::Algorithm::Signal* constantSignal = new ::Algorithm::Algorithm::Signal();
    constantSignal->Owner = pNode->Owner;
    constantSignal->DefinitionLine = pNode->DefinitionLine;
    constantSignal->DataType = Types::DataType::POINTER;
    constantSignal->Type = Types::SignalType::SIGNAL;
    constantSignal->Name.assign(pNode->InputSignal[0].Value.signal->Name + "_index_" + std::to_string(pNode->InputSignal[1].Value.index));

    // Create algorithm node to store constant to index
    ::Algorithm::Algorithm::Node* constantNode = new ::Algorithm::Algorithm::Node();
    constantNode->Owner = pNode->Owner;
    constantNode->DefinitionLine = pNode->DefinitionLine;
    constantNode->Operation = Types::Operation::CONST;
    constantNode->OutputSignal = constantSignal;
    constantNode->InputSignal.push_back((float)(pNode->InputSignal[1].Value.index));

    // Create algorithm node which modifies pointer with constant value
    ::Algorithm::Algorithm::Node* modifyPointer = new ::Algorithm::Algorithm::Node();
    modifyPointer->Owner = pNode->Owner;
    modifyPointer->DefinitionLine = pNode->DefinitionLine;
    modifyPointer->Operation = Types::Operation::ADD;
    modifyPointer->OutputSignal = modifiedPointer;
    modifyPointer->InputSignal.push_back(::Algorithm::Algorithm::NodeInput(pNode->InputSignal[0].Value.signal));
    modifyPointer->InputSignal.push_back(constantSignal);

    // Load from memory via modified pointer
    ::Algorithm::Algorithm::Node* loadNode = new ::Algorithm::Algorithm::Node();
    loadNode->Owner = pNode->Owner;
    loadNode->DefinitionLine = pNode->DefinitionLine;
    loadNode->Operation = Types::Operation::LOADMEM;
    loadNode->OutputSignal = pNode->OutputSignal;
    loadNode->InputSignal.push_back(modifiedPointer);

    // Add nodes and signals to map
    this->AddNodeConstant(constantNode);
    this->AddNode(modifyPointer);
    this->Signals.push_back(SignalMap(this, modifiedPointer));
    this->Signals.push_back(SignalMap(this, constantSignal));

    this->Nodes.push_back(NodeMap(this, loadNode));

    return 0;
}

// TODO STOREMEM AddNode
int Mapping::Mapping::AddNodeStoreMem(::Algorithm::Algorithm::Node* pNode)
{
    assert(pNode != NULL);

    // Create algorithm signal which will store modified pointer
    ::Algorithm::Algorithm::Signal* modifiedPointer = new ::Algorithm::Algorithm::Signal();
    modifiedPointer->Owner = pNode->Owner;
    modifiedPointer->DefinitionLine = pNode->DefinitionLine;
    modifiedPointer->DataType = Types::DataType::POINTER;
    modifiedPointer->Type = Types::SignalType::SIGNAL;
    modifiedPointer->Name.assign(pNode->InputSignal[1].Value.signal->Name + "[" + std::to_string(pNode->InputSignal[2].Value.index) + "]");

    // Create algorithm signal which will store index to modify pointer
    ::Algorithm::Algorithm::Signal* constantSignal = new ::Algorithm::Algorithm::Signal();
    constantSignal->Owner = pNode->Owner;
    constantSignal->DefinitionLine = pNode->DefinitionLine;
    constantSignal->DataType = Types::DataType::POINTER;
    constantSignal->Type = Types::SignalType::SIGNAL;
    constantSignal->Name.assign(pNode->InputSignal[1].Value.signal->Name + "_index_" + std::to_string(pNode->InputSignal[2].Value.index));

    // Create algorithm node to store constant to index
    ::Algorithm::Algorithm::Node* constantNode = new ::Algorithm::Algorithm::Node();
    constantNode->Owner = pNode->Owner;
    constantNode->DefinitionLine = pNode->DefinitionLine;
    constantNode->Operation = Types::Operation::CONST;
    constantNode->OutputSignal = constantSignal;
    constantNode->InputSignal.push_back((float)(pNode->InputSignal[2].Value.index));

    // Create algorithm node which modifies pointer with constant value
    ::Algorithm::Algorithm::Node* modifyPointer = new ::Algorithm::Algorithm::Node();
    modifyPointer->Owner = pNode->Owner;
    modifyPointer->DefinitionLine = pNode->DefinitionLine;
    modifyPointer->Operation = Types::Operation::ADD;
    modifyPointer->OutputSignal = modifiedPointer;
    modifyPointer->InputSignal.push_back(::Algorithm::Algorithm::NodeInput(pNode->InputSignal[1].Value.signal));
    modifyPointer->InputSignal.push_back(constantSignal);

    // Store to memory via modified pointer
    ::Algorithm::Algorithm::Node* storeNode = new ::Algorithm::Algorithm::Node();
    storeNode->Owner = pNode->Owner;
    storeNode->DefinitionLine = pNode->DefinitionLine;
    storeNode->Operation = Types::Operation::STOREMEM;
    storeNode->InputSignal.push_back(pNode->InputSignal[0].Value.signal);
    storeNode->InputSignal.push_back(modifiedPointer);

    // Add nodes and signals to map
    this->AddNodeConstant(constantNode);
    this->AddNode(modifyPointer);
    this->Signals.push_back(SignalMap(this, modifiedPointer));
    this->Signals.push_back(SignalMap(this, constantSignal));

    this->Nodes.push_back(NodeMap(this, storeNode));

    return 0;
}

/**
 * @brief   Finds signal map member which points to specific signal from algorithm.
 * @param   pSignal Pointer to signal, which should be contained in the signal map member.
 * @return  Pointer to signal map member or NULL.
 */
Mapping::SignalMap* Mapping::Mapping::FindSignal(::Algorithm::Algorithm::Signal* pSignal)
{
    assert(this != NULL);

    if (pSignal == NULL)
    {
        return NULL;
    }

	if (pSignal->Type == Types::SignalType::ALIAS)
	{
		for (std::list<::Algorithm::Algorithm::SignalAlias>::iterator itAliasGroup = this->algorithm->SignalAliases.begin(); itAliasGroup != this->algorithm->SignalAliases.end(); itAliasGroup++)
		{
			for (std::list<::Algorithm::Algorithm::Signal*>::iterator itMember = itAliasGroup->Members.begin(); itMember != itAliasGroup->Members.end(); itMember++)
			{
				if (*itMember == pSignal)
				{
					for (std::list<::Algorithm::Algorithm::Signal*>::iterator itSource = itAliasGroup->Members.begin(); itSource != itAliasGroup->Members.end(); itSource++)
					{
						if ((*itSource)->Type != Types::SignalType::ALIAS)
						{
							pSignal = *itSource;
							break;
						}
					}
				}
			}
		}
	}

    for (std::list<::Mapping::SignalMap>::iterator it = this->Signals.begin(); it != this->Signals.end(); it++)
    {
        if (it->Equals(pSignal)) return &*it;
    }
    return NULL;
}

bool Mapping::Mapping::SignalEqual(::Algorithm::Algorithm::Signal* pSignal1, ::Algorithm::Algorithm::Signal* pSignal2)
{
    assert(this != NULL);
    assert(pSignal1 != NULL);
    assert(pSignal2 != NULL);

    return (this->algorithm->GetSignal(pSignal1) == this->algorithm->GetSignal(pSignal2));
}

/**
 * @brief   Comparison of two node map members based on process for sorting function.
 * @param   first
 * @param   second
 * @return
 */
bool Mapping::Mapping::CompareNodes(::Mapping::NodeMap &first, ::Mapping::NodeMap &second)
{
    Mapping* mapping = first.Owner;

    if (first.ProcessLevel < second.ProcessLevel)
    {
        return true;
    }
    else if (first.ProcessLevel == second.ProcessLevel)
    {
        for (size_t n = 0; n < Sorting::GetMethodsCount(); n++)
        {
            if (Sorting::sortingTable[n].method == mapping->sortingMethod)
            {
                if (Sorting::sortingTable[n].function != NULL)
                {
                    return Sorting::sortingTable[n].function(first, second);
                }
            }
        }
    }

    return false;
}

/**
 * @brief   Comparison of two node map members based on process for sorting function.
 * @param   first
 * @param   second
 * @return
 */
bool Mapping::Mapping::FinalCompare(::Mapping::NodeMap &first, ::Mapping::NodeMap &second)
{
    return (first.StartCycle < second.StartCycle);
}

/**
 * @brief   Map functional units and signals.
 * @return  0 on success.
 */
int Mapping::Mapping::DoMap()
{
    assert(this != NULL);

    /* Set process level of the nodes */
    for (std::list<::Mapping::NodeMap>::size_type n = 0; n < this->Nodes.size(); n++)
    {
        for (std::list<::Mapping::NodeMap>::iterator it = this->Nodes.begin(); it != this->Nodes.end(); it++)
        {
            /* If node does not have assigned process level */
            if (it->ProcessLevel == -1)
            {
                int processLevel = 0;
                bool inputIsNull = true;
                bool inputHasLevel = true;

                /* Node loads constant */
                if (it->Operation() == Types::Operation::CONST)
                {
                    it->ProcessLevel = processLevel;
                }
                /* Node is general operation */
                else
                {
                    std::vector<NodeMap*> signals = it->GetInputNodes();
                    for (std::vector<NodeMap*>::iterator itInNode = signals.begin(); itInNode != signals.end(); itInNode++)
                    {
                        if (*itInNode != NULL)
                        {
                            inputIsNull = false;

                            if ((*itInNode)->ProcessLevel < 0)
                            {
                                inputHasLevel = false;
                            }
                            else if ((*itInNode)->ProcessLevel >= processLevel)
                            {
                                processLevel = (*itInNode)->ProcessLevel + 1;
                            }
                        }
                    }

                    if (inputIsNull || inputHasLevel)
                    {
                        it->ProcessLevel = processLevel;
                    }
                }
            }
        }
    }

    // TODO Update constant loading process level
    for (std::list<NodeMap>::iterator it = this->Nodes.begin(); it != this->Nodes.end(); it++)
    {
        if (it->Operation() == Types::Operation::CONST)
        {
            it->ProcessLevel = this->GetMaxLevel();

            if (!it->IsOutput())
            {
                for (std::list<NodeMap>::iterator itNode = this->Nodes.begin(); itNode != this->Nodes.end(); itNode++)
                {
                    if (itNode->Operation() != Types::Operation::CONST)
                    {
                        std::vector<SignalMap*> inputSignals = itNode->GetInputSignals();
                        for (std::vector<SignalMap*>::iterator itSignal = inputSignals.begin(); itSignal != inputSignals.end(); itSignal++)
                        {
                            if (*itSignal == it->GetOutputSignal() &&
                                it->ProcessLevel >= itNode->ProcessLevel)
                            {
                                it->ProcessLevel = itNode->ProcessLevel - 1;
                            }
                        }
                    }
                }
            }
        }
    }

    this->Nodes.sort(Mapping::CompareNodes);

    for (std::list<NodeMap>::iterator it = this->Nodes.begin(); it != this->Nodes.end(); it++)
    {
        if (it->Place())
        {
            return -1;
        }
    }

    // TODO Sort signals ???

    for (std::list<SignalMap>::iterator it = this->Signals.begin(); it != this->Signals.end(); it++)
    {
        it->RefreshLifeTime();
    }

    for (std::list<SignalMap>::iterator it = this->Signals.begin(); it != this->Signals.end(); it++)
    {
        if (it->Place())
        {
            return -1;
        }
    }

    /* Final sort based on the start cycle */
    this->Nodes.sort(FinalCompare);

    return 0;
}

/**
 * @brief   Save mapped algorithm in ASM format.
 * @param   FileName        Path to file to be saved.
 * @param   ForceComments   Turn on the comments on the instructions.
 * @return  0 on success.
 */
int Mapping::Mapping::SaveAsm(std::string FileName, bool ForceComments)
{
    assert(this != NULL);
    assert(!FileName.empty());

    std::ofstream file;
    file.open(FileName.c_str(), std::ios::out);
    if (!file.is_open())
    {
        return -1;
    }

    for (int n = 0; n < this->UsageMap.GetCycleCount(); n++)
    {
        bool isFirst = true;
        for (std::list<Mapping::Usage::UnitUsage>::iterator it = this->UsageMap.UnitsUsage.begin(); it != this->UsageMap.UnitsUsage.end(); it++)
        {
            if (it->Usage[n] != NULL)
            {
                if (isFirst)
                {
                    file << "    " << it->Usage[n]->GetAsmInstruction();
                    isFirst = false;
                }
                else
                {
                    file << "||  " << it->Usage[n]->GetAsmInstruction();
                }

                if (ForceComments)
                {
                	file << "    ; " << it->Usage[n]->GetNotation();
                }

                file << std::endl;
            }
        }
        if (isFirst)
        {
            file << "    NOP" << std::endl;
        }
    }

    file.close();
    return 0;
}

/**
 * @brief   Save mapped algorithm in format for Graphviz.
 * @param   FileName    Path to file to be saved.
 * @return  0 on success.
 */
int Mapping::Mapping::SaveDot(std::string FileName)
{
    assert(this != NULL);
    assert(!FileName.empty());

    std::ofstream file;
    file.open(FileName.c_str(), std::ios::out);
    if (!file.is_open())
    {
        return -1;
    }

    file << "digraph G {" << std::endl;

    for (std::list<::Mapping::SignalMap>::iterator it = this->Signals.begin(); it != this->Signals.end(); it++)
    {
        file << "    " << it->Name() << " [shape=box];" << std::endl;
    }

    int n = 0;
    for (std::list<::Mapping::NodeMap>::iterator it = this->Nodes.begin(); it != this->Nodes.end(); it++)
    {
        if (it->Operation() != Types::Operation::CONST)
        {
            file << "    node" << n << " [label=\"" << Types::GetOperationStr(it->Operation()) << "\"];" << std::endl;
            if (it->GetOutputSignal() != NULL)
            {
                file << "    node" << n << " -> " << it->GetOutputSignal()->Name() << ";" << std::endl;
            }

            std::vector<SignalMap*> signals = it->GetInputSignals();
            for (std::vector<SignalMap*>::iterator itSignal = signals.begin(); itSignal != signals.end(); itSignal++)
            {
                file << "    " << (*itSignal)->Name() << " -> node" << n << ";" << std::endl;
            }
            n++;
        }
    }

    file << "}";

    file.close();
    return 0;
}

/**
 * @brief   Save functional unit usage map in CSV table.
 * @param   FileName    Path to file to be saved
 * @return  0 on success
 */
int Mapping::Mapping::SaveUnitMap(std::string FileName)
{
    assert(this != NULL);
    assert(!FileName.empty());

    std::ofstream file;
    file.open(FileName.c_str(), std::ios::out);
    if (!file.is_open())
    {
        return -1;
    }

    for (std::list<Mapping::Usage::UnitUsage>::iterator it = this->UsageMap.UnitsUsage.begin(); it != this->UsageMap.UnitsUsage.end(); it++)
    {
        file << it->UnitPtr->GetName() << ";";
    }
    file << std::endl;

    for (int n = 0; n < this->UsageMap.GetCycleCount(); n++)
    {
        for (std::list<Mapping::Usage::UnitUsage>::iterator it = this->UsageMap.UnitsUsage.begin(); it != this->UsageMap.UnitsUsage.end(); it++)
        {
            if (it->Usage[n] != NULL)
            {
                file << it->Usage[n]->InstructionPtr->GetName();
            }
            file << ";";
        }
        file << std::endl;
    }

    file.close();
    return 0;
}

/**
 * @brief   Save register usage map in CSV table.
 * @param   FileName    Path to file to be saved
 * @return  0 on success
 */
int Mapping::Mapping::SaveRegisterMap(std::string FileName)
{
    assert(this != NULL);
    assert(!FileName.empty());

    std::ofstream file;
    file.open(FileName.c_str(), std::ios::out);
    if (!file.is_open())
    {
        return -1;
    }

    for (std::list<Mapping::Usage::RegisterUsage>::iterator it = this->UsageMap.RegistersUsage.begin(); it != this->UsageMap.RegistersUsage.end(); it++)
    {
        file << it->RegisterPtr->GetName() << ";";
    }
    file << std::endl;

    for (int n = 0; n < this->UsageMap.GetCycleCount(); n++)
    {
        for (std::list<Mapping::Usage::RegisterUsage>::iterator it = this->UsageMap.RegistersUsage.begin(); it != this->UsageMap.RegistersUsage.end(); it++)
        {
            if (it->Usage[n] != NULL)
            {
                file << it->Usage[n]->Name();
            }
            file << ";";
        }
        file << std::endl;
    }

    file.close();
    return 0;
}

/**
 * @brief   Get number of processing levels of the algorithm.
 * @return  Number of processing levels of the algorithm.
 */
int Mapping::Mapping::GetMaxLevel()
{
    assert(this != NULL);

    int tmpLevel = -1;

    for (std::list<::Mapping::NodeMap>::iterator tmpIt = this->Nodes.begin(); tmpIt != this->Nodes.end(); tmpIt++)
    {
        if (tmpIt->ProcessLevel > tmpLevel)
        {
            tmpLevel = tmpIt->ProcessLevel;
        }
    }

    return tmpLevel;
}
